/*
SQLyog Community v9.60 
MySQL - 5.6.21-log : Database - ates_credentials
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ates_credentials` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `ates_credentials`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `adminID` int(11) NOT NULL AUTO_INCREMENT,
  `employeeID` varchar(8) NOT NULL,
  PRIMARY KEY (`adminID`),
  KEY `employeeID` (`employeeID`),
  CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`employeeID`) REFERENCES `employee` (`employeeID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `admin` */

insert  into `admin`(`adminID`,`employeeID`) values (1,'522-0000');

/*Table structure for table `attendance` */

DROP TABLE IF EXISTS `attendance`;

CREATE TABLE `attendance` (
  `participationID` int(11) NOT NULL,
  `date` date NOT NULL,
  `amAttendance` enum('Present','Tardy','Absent','N/A') DEFAULT 'N/A',
  `pmAttendance` enum('Present','Tardy','Absent','N/A') DEFAULT 'N/A',
  PRIMARY KEY (`participationID`,`date`),
  CONSTRAINT `participation_id` FOREIGN KEY (`participationID`) REFERENCES `participation` (`participantID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `attendance` */

/*Table structure for table `employee` */

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `employeeID` varchar(8) NOT NULL,
  `firstName` varchar(16) NOT NULL,
  `middleName` varchar(16) DEFAULT NULL,
  `lastName` varchar(16) NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `address` varchar(128) NOT NULL,
  `email` varchar(32) DEFAULT NULL,
  `contactNumber` varchar(16) DEFAULT NULL,
  `employer` varchar(32) NOT NULL,
  `jobTitle` varchar(32) NOT NULL,
  `businessGroup` varchar(32) NOT NULL,
  `buHead` varchar(8) NOT NULL,
  `supervisor` varchar(8) DEFAULT NULL,
  `password` varchar(32) NOT NULL DEFAULT '123456',
  `loggedIn` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`employeeID`),
  KEY `buHead` (`buHead`),
  KEY `supervisor` (`supervisor`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`buHead`) REFERENCES `employee` (`employeeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `employee_ibfk_2` FOREIGN KEY (`supervisor`) REFERENCES `employee` (`employeeID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `employee` */

insert  into `employee`(`employeeID`,`firstName`,`middleName`,`lastName`,`gender`,`address`,`email`,`contactNumber`,`employer`,`jobTitle`,`businessGroup`,`buHead`,`supervisor`,`password`,`loggedIn`) values ('522-0000','Araceli',NULL,'Adlawon','Female','Cebu','email.com','09999999999','Alliance Software Inc.','Product Owner','Human Resource','522-0000',NULL,'test',1),('522-0001','Jhon Christian',NULL,'Ambrad','Male','Cebu','email.com','09888888888','Alliance Software Inc.','Project Manager','Human Resource','522-0000','522-0000','test',1),('522-0002','Justin',NULL,'Enerio','Male','Cebu','email.com','09777777777','Alliance Software Inc.','Developer','Human Resource','522-0000','522-0000','1234',1),('522-0003','Joshua',NULL,'Dy','Male','Cebu','email.com','09666666666','Alliance Software Inc.','Developer','Human Resource','522-0000','522-0000','test',1),('522-0004','Razil','Ogario','Cansancio','Male','Cebu','chichiri.ryota@gmail.com','09567120870','Alliance Software Inc.','Developer','Human Resource','522-0000','522-0000','password',1);

/*Table structure for table `form` */

DROP TABLE IF EXISTS `form`;

CREATE TABLE `form` (
  `formID` int(11) NOT NULL AUTO_INCREMENT,
  `formType` int(11) NOT NULL,
  `trainingID` int(11) NOT NULL,
  `formFor` enum('N/A','Pre','Post') NOT NULL,
  PRIMARY KEY (`formID`),
  KEY `formType` (`formType`),
  KEY `trainingID` (`trainingID`),
  CONSTRAINT `form_ibfk_1` FOREIGN KEY (`formType`) REFERENCES `form_type` (`typeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `form_ibfk_2` FOREIGN KEY (`trainingID`) REFERENCES `training` (`trainingID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=408 DEFAULT CHARSET=utf8;

/*Data for the table `form` */

insert  into `form`(`formID`,`formType`,`trainingID`,`formFor`) values (403,2,1033,'Post'),(404,2,1035,'Pre'),(405,4,1033,'Post'),(406,3,1033,'Pre'),(407,1,1033,'Post');

/*Table structure for table `form_details` */

DROP TABLE IF EXISTS `form_details`;

CREATE TABLE `form_details` (
  `detailID` int(11) NOT NULL AUTO_INCREMENT,
  `formType` int(11) NOT NULL,
  `category` varchar(32) DEFAULT NULL,
  `questionNumber` float DEFAULT NULL,
  `questionString` varchar(256) NOT NULL,
  `questionType` enum('Level','Check','Year','Essay','none','Comprehension','Well','Suitable','Objectives','Useful','Good','Clear','Agree','Expectations') NOT NULL,
  PRIMARY KEY (`detailID`),
  KEY `formType` (`formType`),
  CONSTRAINT `form_details_ibfk_1` FOREIGN KEY (`formType`) REFERENCES `form_type` (`typeID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

/*Data for the table `form_details` */

insert  into `form_details`(`detailID`,`formType`,`category`,`questionNumber`,`questionString`,`questionType`) values (1,1,'Management',1,'Scope Management','Level'),(2,1,'Management',2,'Schedule management		','Level'),(3,1,'Management',3,'Quality management		','Level'),(4,1,'Management',4,'Manpower management 		','Level'),(5,1,'Management',5,'Cost management		','Level'),(6,1,'Management',6,'Communication management','Level'),(7,1,'Management',7,'Problem management','Level'),(8,1,'Management',8,'Risk management','Level'),(9,1,'Human Skill',9,'Leadership','Level'),(10,1,'Human Skill',10,'Globally competitive human resources','Level'),(11,1,'Human Skill',11,'Communication skill','Level'),(12,1,'Language',12,'Japanese (if applicable)','Level'),(13,1,'Language',13,'English','Level'),(14,1,'Alliance Process',14,'Development Process','Level'),(15,1,'Alliance Process',15,'Devise development plan','Level'),(16,1,'Alliance Process',16,'Bug analysis','Level'),(17,1,'Alliance Process',17,'TEST/SIT/UAT Issue management during TST/SIT/UAT','Level'),(18,1,'Systems Engineer',18,'Grasp of technology trends','Level'),(19,1,'Systems Engineer',19,'Communication skills as expert on the field','Level'),(20,1,'Technical',20,'Understand specificaiton of the development','Level'),(21,1,'Technical',21,'Estimation of the development','Level'),(22,1,'Technical',22,'Review specification','Level'),(23,1,'Technical',23,'Review source code','Level'),(24,1,'Technical',24,'Making of test cases','Level'),(25,1,'Technical',25,'Review test cases','Level'),(26,1,'Technical',26,'Log analysis','Level'),(27,1,'Development Experience',27,'C, C++','Year'),(28,1,'Development Experience',28,'C#','Year'),(29,1,'Development Experience',29,'.NET','Year'),(30,1,'Development Experience',30,'MySQL','Year'),(31,1,'Development Experience',31,'HTML','Year'),(32,1,'Development Experience',32,'VBS','Year'),(33,1,'Development Experience',33,'JAVA, J2EE','Year'),(34,1,'OS',34,'Windows - Spec','Year'),(35,1,'OS',35,'Windows - Development','Year'),(36,1,'OS',36,'Linux - Spec','Year'),(37,1,'OS',37,'Linux - Development','Year'),(38,1,'Technical',38,'NDC','Year'),(39,1,'Technical',39,'DDC','Year'),(40,1,'Technical',40,'ISO','Year'),(41,1,'Technical',41,'IFX','Year'),(42,1,'Technical',42,'EMV','Year'),(43,1,'Technical',43,'PCI','Year'),(44,1,'Technical',44,'CEN/XFS','Year'),(45,1,'Technical',45,'BIO/API','Year'),(46,1,'Testing',46,'Making of the recovery CD','Year'),(47,1,'Testing',47,'Setup','Year'),(48,1,'Testing',48,'Operations','Year'),(49,1,'Testing',49,'Setup of simulator','Year'),(50,1,'Testing',50,'Simulator operations','Year'),(51,1,'Testing',51,'HOST Simulator operations','Year'),(52,4,'Questions',1,'The training met my expectations and desired outcome.','Agree'),(53,4,'Questions',2,'Participant was able to apply the knowledge learned.','Agree'),(54,4,'Questions',3,'The materials distribuated were pertinent and useful to the participant (if any).','Agree'),(55,4,'Questions',4,'The trainer was able to deliver the desired knowledge and output as evident by the participant\'s development. ','Agree'),(56,4,'Questions',5,'How do you rate the participant\'s development relating to the training objectives?','Expectations'),(57,4,'Questions',6,'How do you rate the training overall?','Expectations'),(58,4,'Questions',7,'What aspects of the training could be improved?','Essay'),(59,4,'Questions',8,'Other comments?','Essay'),(60,3,'Self-Evaluation',1,'Comprehension of the Subject.','Comprehension'),(61,3,'Self-Evaluation',2,'If your answer to #1 is either \"No improvement\" or \"Regressed\", give your reason(s) by choosing one or several of the items below:','Check'),(62,3,'option',2,'The textbooks/manuals were too difficult.','none'),(63,3,'option',2,'The textbooks/manuals were inadequate.','none'),(64,3,'option',2,'The explanation of the instructor was insufficient.','none'),(65,3,'option',2,'The content of the lectures was too difficult.','none'),(66,3,'option',2,'The size of the class was too big.','none'),(67,3,'option',2,'I  did not have enough time to finish the exercise.','none'),(68,3,'option',2,'I did not have pre-knowledge of this subject.','none'),(69,3,'option',2,'I was absent during critical period of the subject.','none'),(70,3,'option',2,'My computer experience was insufficient.','none'),(71,3,'option',2,'Other','none'),(72,3,'Self-Evaluation',3,'How well did you understand each objective given by the ','Well'),(73,3,'Subject / Teacher Evaluation',4,'Evaluate the subject\'s content','Suitable'),(74,3,'Subject / Teacher Evaluation',5,'Evaluate the subject\'s coverage','Suitable'),(75,3,'Subject / Teacher Evaluation',6,'Evaluate the subject\'s depth','Suitable'),(76,3,'Subject / Teacher Evaluation',7,'Evaluate the exercises\' content','Suitable'),(77,3,'Subject / Teacher Evaluation',8,'Evaluate the exercises\' coverage','Suitable'),(78,3,'Subject / Teacher Evaluation',9,'Evaluate the exercises\' depth','Suitable'),(79,3,'Subject / Teacher Evaluation',10,'How well did the subject meet your objectives?','Objectives'),(80,3,'Subject / Teacher Evaluation',11,'Select a number that best suits your opinion regarding its Usefulness.','Useful'),(81,3,'Subject / Teacher Evaluation',12,'Select a number that best suits your opinion regarding the Instruction Method','Useful'),(82,3,'Subject / Teacher Evaluation',13,'Select a number that best suits your opinion regarding the Clarity of Explanation.','Useful'),(83,3,'Subject / Teacher Evaluation',14,'If your answer is (4) or (5), give your reason(s).','Essay'),(84,3,'Subject / Teacher Evaluation',15,'Adequate time was allocated for Lecture','Agree'),(85,3,'Subject / Teacher Evaluation',16,'Adequate time was allocated for Q&A','Agree'),(86,3,'Subject / Teacher Evaluation',17,'Adequate time was allocated for Exercises ','Agree'),(87,3,'Subject / Teacher Evaluation',18,'Adequate time was allocated for Workshop','Agree'),(88,3,'Subject / Teacher Evaluation',19,'The slide presentations and learning materials used were supportive of the topic ','Agree'),(89,3,'Subject / Teacher Evaluation',20,'The slide presentations and learning materials used were properly sequenced ','Agree'),(90,3,'Subject / Teacher Evaluation',21,'The speaker is able to explain concepts clearly','Agree'),(91,3,'Subject / Teacher Evaluation',22,'The speaker is well-organized ','Agree'),(92,3,'Subject / Teacher Evaluation',23,'The speaker provides a stimulating atmosphere that encourages discussion','Agree'),(93,3,'Subject / Teacher Evaluation',24,'The speaker projects an image of authority','Agree'),(94,3,'Subject / Teacher Evaluation',25,'Additional comments on the Speaker ','Essay'),(95,3,'Subject / Teacher Evaluation',26,'The facilities for the training exceeded expectation','Agree'),(96,3,'Subject / Teacher Evaluation',27,'The venue is conducive for learning','Agree'),(97,3,'Subject / Teacher Evaluation',28,'The sound system was audible','Agree'),(98,3,'Subject / Teacher Evaluation',29,'The air-conditioning system works just fine','Agree'),(99,3,'Subject / Teacher Evaluation',30,'Additional comments on the Facilities','Essay'),(100,3,'Subject / Teacher Evaluation',31,'Overall comments','Essay');

/*Table structure for table `form_result` */

DROP TABLE IF EXISTS `form_result`;

CREATE TABLE `form_result` (
  `resultID` int(11) NOT NULL AUTO_INCREMENT,
  `formID` int(11) NOT NULL,
  `employeeID` varchar(8) NOT NULL,
  `detailID` int(11) NOT NULL,
  `answerString` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`resultID`),
  KEY `detailID` (`detailID`),
  KEY `employeeID` (`employeeID`),
  KEY `form_result_ibfk_3` (`formID`),
  CONSTRAINT `form_result_ibfk_1` FOREIGN KEY (`detailID`) REFERENCES `form_details` (`detailID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `form_result_ibfk_2` FOREIGN KEY (`employeeID`) REFERENCES `employee` (`employeeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `form_result_ibfk_3` FOREIGN KEY (`formID`) REFERENCES `form` (`formID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=utf8;

/*Data for the table `form_result` */

insert  into `form_result`(`resultID`,`formID`,`employeeID`,`detailID`,`answerString`) values (107,406,'522-0003',60,'0'),(108,406,'522-0003',71,'null'),(109,406,'522-0003',70,'1'),(110,406,'522-0003',69,'0'),(111,406,'522-0003',68,'1'),(112,406,'522-0003',67,'0'),(113,406,'522-0003',66,'1'),(114,406,'522-0003',65,'0'),(115,406,'522-0003',64,'1'),(116,406,'522-0003',63,'0'),(117,406,'522-0003',62,'1'),(118,406,'522-0003',61,'0'),(119,406,'522-0003',72,'0'),(120,406,'522-0003',73,'0'),(121,406,'522-0003',74,'0'),(122,406,'522-0003',75,'0'),(123,406,'522-0003',76,'0'),(124,406,'522-0003',77,'0'),(125,406,'522-0003',78,'0'),(126,406,'522-0003',79,'0'),(127,406,'522-0003',80,'0'),(128,406,'522-0003',81,'0'),(129,406,'522-0003',82,'0'),(130,406,'522-0003',83,'hehe'),(131,406,'522-0003',84,'0'),(132,406,'522-0003',85,'0'),(133,406,'522-0003',86,'0'),(134,406,'522-0003',87,'0'),(135,406,'522-0003',88,'0'),(136,406,'522-0003',89,'0'),(137,406,'522-0003',90,'0'),(138,406,'522-0003',91,'0'),(139,406,'522-0003',92,'0'),(140,406,'522-0003',93,'0'),(141,406,'522-0003',94,'hehe'),(142,406,'522-0003',95,'0'),(143,406,'522-0003',96,'0'),(144,406,'522-0003',97,'0'),(145,406,'522-0003',98,'0'),(146,406,'522-0003',99,'hehe'),(147,406,'522-0003',100,'hehe'),(148,406,'522-0002',60,'0'),(149,406,'522-0002',71,'null'),(150,406,'522-0002',70,'1'),(151,406,'522-0002',69,'0'),(152,406,'522-0002',68,'1'),(153,406,'522-0002',67,'0'),(154,406,'522-0002',66,'0'),(155,406,'522-0002',65,'0'),(156,406,'522-0002',64,'0'),(157,406,'522-0002',63,'0'),(158,406,'522-0002',62,'0'),(159,406,'522-0002',61,'0'),(160,406,'522-0002',72,'3'),(161,406,'522-0002',73,'3'),(162,406,'522-0002',74,'1'),(163,406,'522-0002',75,'0'),(164,406,'522-0002',76,'1'),(165,406,'522-0002',77,'0'),(166,406,'522-0002',78,'0'),(167,406,'522-0002',79,'0'),(168,406,'522-0002',80,'0'),(169,406,'522-0002',81,'0'),(170,406,'522-0002',82,'0'),(171,406,'522-0002',83,'asdf'),(172,406,'522-0002',84,'0'),(173,406,'522-0002',85,'0'),(174,406,'522-0002',86,'0'),(175,406,'522-0002',87,'0'),(176,406,'522-0002',88,'0'),(177,406,'522-0002',89,'0'),(178,406,'522-0002',90,'0'),(179,406,'522-0002',91,'0'),(180,406,'522-0002',92,'0'),(181,406,'522-0002',93,'0'),(182,406,'522-0002',94,'asdf'),(183,406,'522-0002',95,'0'),(184,406,'522-0002',96,'0'),(185,406,'522-0002',97,'4'),(186,406,'522-0002',98,'4'),(187,406,'522-0002',99,'asdf'),(188,406,'522-0002',100,'asdf');

/*Table structure for table `form_type` */

DROP TABLE IF EXISTS `form_type`;

CREATE TABLE `form_type` (
  `typeID` int(11) NOT NULL AUTO_INCREMENT,
  `formType` varchar(64) NOT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `form_type` */

insert  into `form_type`(`typeID`,`formType`) values (1,'Skills Assessment'),(2,'Training Needs Analysis'),(3,'Course Feedback'),(4,'Training Effectiveness Assessment');

/*Table structure for table `participation` */

DROP TABLE IF EXISTS `participation`;

CREATE TABLE `participation` (
  `participantID` int(11) NOT NULL AUTO_INCREMENT,
  `employeeID` varchar(8) NOT NULL,
  `trainingID` int(11) NOT NULL,
  PRIMARY KEY (`participantID`),
  KEY `employeeID` (`employeeID`),
  KEY `trainingID` (`trainingID`),
  CONSTRAINT `participation_ibfk_1` FOREIGN KEY (`employeeID`) REFERENCES `employee` (`employeeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `participation_ibfk_2` FOREIGN KEY (`trainingID`) REFERENCES `training` (`trainingID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8;

/*Data for the table `participation` */

insert  into `participation`(`participantID`,`employeeID`,`trainingID`) values (142,'522-0001',1033),(143,'522-0003',1033),(144,'522-0000',1034),(145,'522-0004',1034),(146,'522-0000',1035),(147,'522-0004',1035),(148,'522-0003',1035),(149,'522-0000',1035),(150,'522-0003',1035),(151,'522-0000',1036),(152,'522-0004',1036),(153,'522-0003',1036),(154,'522-0000',1036),(155,'522-0003',1036),(156,'522-0002',1036),(157,'522-0000',1036),(158,'522-0004',1036);

/*Table structure for table `pending` */

DROP TABLE IF EXISTS `pending`;

CREATE TABLE `pending` (
  `pendingID` int(11) NOT NULL AUTO_INCREMENT,
  `employeeID` varchar(8) NOT NULL,
  `formID` int(11) NOT NULL,
  `participantID` int(11) DEFAULT NULL,
  `isDone` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`pendingID`),
  KEY `employeeID` (`employeeID`),
  KEY `formID` (`formID`),
  KEY `participantID` (`participantID`),
  CONSTRAINT `pending_ibfk_1` FOREIGN KEY (`employeeID`) REFERENCES `employee` (`employeeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pending_ibfk_2` FOREIGN KEY (`formID`) REFERENCES `form` (`formID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pending_ibfk_3` FOREIGN KEY (`participantID`) REFERENCES `participation` (`participantID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `pending` */

insert  into `pending`(`pendingID`,`employeeID`,`formID`,`participantID`,`isDone`) values (3,'522-0000',407,NULL,0),(4,'522-0000',406,NULL,0),(5,'522-0000',405,NULL,0),(6,'522-0000',403,NULL,0),(7,'522-0000',404,NULL,0);

/*Table structure for table `training` */

DROP TABLE IF EXISTS `training`;

CREATE TABLE `training` (
  `trainingID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `trainingPlanID` int(11) NOT NULL,
  `facilitatorID` varchar(8) NOT NULL,
  `objective` longtext NOT NULL,
  `type` enum('Int','Ext','Cert') NOT NULL,
  `remarks` longtext,
  `businessUnit` varchar(32) DEFAULT NULL,
  `noOfPax` int(3) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `startTime` time DEFAULT NULL,
  `endTime` time DEFAULT NULL,
  PRIMARY KEY (`trainingID`),
  KEY `trainingPlanID` (`trainingPlanID`),
  KEY `facilitatorID` (`facilitatorID`),
  CONSTRAINT `training_ibfk_1` FOREIGN KEY (`trainingPlanID`) REFERENCES `training_plan` (`trainingPlanID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `training_ibfk_2` FOREIGN KEY (`facilitatorID`) REFERENCES `employee` (`employeeID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1037 DEFAULT CHARSET=utf8;

/*Data for the table `training` */

insert  into `training`(`trainingID`,`title`,`trainingPlanID`,`facilitatorID`,`objective`,`type`,`remarks`,`businessUnit`,`noOfPax`,`startDate`,`endDate`,`startTime`,`endTime`) values (1033,'J2EE Summer Bridge',105,'522-0000','<p><blockquote>Test<b>setset</b>Est tsetset</blockquote><b></b></p>','Int','ASI Summer Bridge 2017','ASJ',45,'2017-06-04','2017-06-30',NULL,NULL),(1034,'Spam',105,'522-0001','<p>asdasd</p>','Int','spam','ASJ',5,'2017-06-06','2017-06-08',NULL,NULL),(1035,'spam2',105,'522-0002','<p>asdfasf</p>','Int','sdf','ASJ',5,'2017-06-08','2017-06-09',NULL,NULL),(1036,'test',105,'522-0002','<p>asdf</p>','Int','test','ASJ',4,'2017-06-09','2017-06-10',NULL,NULL);

/*Table structure for table `training_plan` */

DROP TABLE IF EXISTS `training_plan`;

CREATE TABLE `training_plan` (
  `trainingPlanID` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(4) NOT NULL,
  PRIMARY KEY (`trainingPlanID`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;

/*Data for the table `training_plan` */

insert  into `training_plan`(`trainingPlanID`,`year`) values (105,2017),(106,2018),(107,2019),(108,2020);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
