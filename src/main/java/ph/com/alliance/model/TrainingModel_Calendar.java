package ph.com.alliance.model;

import javax.persistence.*;
import java.util.Date;


public class TrainingModel_Calendar {
	
	@Id
	private int id;

	@Temporal(TemporalType.DATE)
	private Date end;
	
	@Temporal(TemporalType.DATE)
	private Date start;

	private String title;
	
	

	public int getId() {
		return this.id;
	}

	public void setId(int trainingID) {
		this.id = trainingID;
	}

	public Date getEnd() {
		return this.end;
	}

	public void setEnd(Date endDate) {
		this.end = endDate;
	}

	public Date getStart() {
		return this.start;
	}

	public void setStart(Date startDate) {
		this.start = startDate;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


}