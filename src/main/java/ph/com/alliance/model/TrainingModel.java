package ph.com.alliance.model;

import javax.persistence.*;

import java.sql.Time;
import java.util.Date;


public class TrainingModel {
	@Id
	private int trainingID;

	private String businessUnit;

	@Temporal(TemporalType.DATE)
	private Date endDate;

	private Time endTime;

	private int noOfPax;
	
	private String type;

	@Lob
	private String objective;

	@Temporal(TemporalType.DATE)
	private Date startDate;

	private Time startTime;

	private String title;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getTrainingID() {
		return this.trainingID;
	}

	public void setTrainingID(int trainingID) {
		this.trainingID = trainingID;
	}

	public String getBusinessUnit() {
		return this.businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date string) {
		this.endDate = string;
	}

	public Time getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}

	public int getNoOfPax() {
		return this.noOfPax;
	}

	public void setNoOfPax(int noOfPax) {
		this.noOfPax = noOfPax;
	}

	public String getObjective() {
		return this.objective;
	}

	public void setObjective(String objective) {
		this.objective = objective;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date string) {
		this.startDate = string;
	}

	public Time getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}