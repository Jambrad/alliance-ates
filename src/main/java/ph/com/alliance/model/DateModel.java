package ph.com.alliance.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class DateModel {
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",timezone="Singapore")
	private Date date;
	
	public DateModel(){}
	
	public DateModel(Date date){
		this.date = date;
	}
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "DateModel [date=" + date + "]";
	}
	
	

}
