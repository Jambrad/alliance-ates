package ph.com.alliance.model;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


/**
 * The persistent class for the participation database table.
 * 
 */
public class ParticipationModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int participantID;

	//bi-directional many-to-one association to Employee
	@ManyToOne
	@JoinColumn(name="employeeID")
	private EmployeeModel employee;

	public ParticipationModel() {
	}

	public int getParticipantID() {
		return this.participantID;
	}

	public void setParticipantID(int participantID) {
		this.participantID = participantID;
	}

	public EmployeeModel getEmployee() {
		return this.employee;
	}

	public void setEmployee(EmployeeModel employee) {
		this.employee = employee;
	}


}