package ph.com.alliance.model;

import java.io.Serializable;


/**
 * The persistent class for the training_plan database table.
 * 
 */

public class TrainingPlanModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private int trainingPlanID;

	private int year;

	public TrainingPlanModel() {
	}

	public int getTrainingPlanID() {
		return this.trainingPlanID;
	}

	public void setTrainingPlanID(int trainingPlanID) {
		this.trainingPlanID = trainingPlanID;
	}

	public int getYear() {
		return this.year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return "TrainingPlan [trainingPlanID=" + trainingPlanID + ", year="
				+ year + "]";
	}
	
	

}