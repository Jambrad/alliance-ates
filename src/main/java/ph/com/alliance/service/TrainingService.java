package ph.com.alliance.service;


import java.util.List;



import ph.com.alliance.entity.Employee;
import ph.com.alliance.entity.Training;
import ph.com.alliance.entity.TrainingPlan;

public interface TrainingService {

	/**
	 * This function creates a new training plan.
	 * @param p
	 * @return true, if created
	 * 			false, if otherwise
	 */
	public boolean createTrainingPlan(TrainingPlan	p);

	/**
	 * This function updates an existing training plan.
	 * @param p
	 * @return true, if created.
	 * 			false, if otherwise
	 */
	public boolean updateTrainingPlan(TrainingPlan p);

	/**
	 * This function deletes an existing training plan by a specific year.
	 * @param year
	 * @return true, if deleted
	 * 			false, if otherwise
	 */
	public boolean deleteTrainingPlan(int year);

	/**
	 * This function retrieves all training plans.
	 * @return list of training plans
	 */
	public List<TrainingPlan> getAllTrainingPlan();
	
	/**
	 * This function retrieves a specific training plan by year.
	 * @param year
	 * @return trainingplan object
	 */
	public TrainingPlan getByYear(int year);
	
	/**
	 * This function retrieves a specific training plan by its ID.
	 * @param id
	 * @return trainingplan object
	 */
	public TrainingPlan getByID(int id);
	
	/**
	 * This function retrieves the latest year in the training plan.
	 * @return int year
	 */
	public int getLatestYear();
	
	/**
	 * This function creates a new training.
	 * @param t
	 * @return true, if training is created
	 * 			false, if otherwise
	 */
	public boolean createTraining(Training t);

	/**
	 * This function updates an existing training.
	 * @param t
	 * @return true, if training updated
	 * 			false, if otherwise
	 */
	public boolean updateTraining(Training t);

	/**
	 * This function deletes an existing training.
	 * @param id
	 * @return true, if training is deleted
	 * 			false, if otherwise
	 */
	public boolean deleteTrainingByID(int id);
	
	/**
	 * This function deletes a training by batch.
	 * @param id
	 * @return true, if all trainings deleted
	 * 			false, if otherwise
	 */
	public boolean deleteTrainingBatch(List<Integer> id);
	
	/**
	 * This function retrieves a list of all trainings.
	 * @return list of training
	 */
	public List<Training> getAllTraining();
	
	/**
	 * This function retrieves a list of trainings by a specfic training plan.
	 * @param id
	 * @return list of trainings
	 */
	public List<Training> getByTrainingPlanID(int id);
	
	/**
	 * This function retrieves a specific training by its ID.
	 * @param id
	 * @return training object
	 */
	public Training getByTrainingID(int id);
	
	/**
	 * This function retrieves a list of trainings by its title.
	 * @param title
	 * @return list of trainings
	 */
	public List<Training> getByTitle(String title);

	/**
	 * This function validates if an Employee is a facilitator.
	 * @param emp
	 * @return true, if employee is faciliator
	 * 			false, if otherwise
	 */
	public boolean isFacilitator(Employee emp);
	
	/**
	 * This function gets a list of training by a specific facilitator.
	 * @param faci
	 * @return list of training.
	 */
	public List<Training> getTrainingByFacilitator(Employee faci);
}
