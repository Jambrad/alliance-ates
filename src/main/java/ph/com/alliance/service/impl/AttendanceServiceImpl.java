package ph.com.alliance.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.AttendanceDao;
import ph.com.alliance.dao.ParticipantDao;
import ph.com.alliance.entity.Attendance;
import ph.com.alliance.entity.AttendancePK;
import ph.com.alliance.entity.Employee;
import ph.com.alliance.entity.Participation;
import ph.com.alliance.entity.Training;
import ph.com.alliance.service.AttendanceService;

@Service("attendanceService")
public class AttendanceServiceImpl implements AttendanceService {

	@Autowired
	private JpaTransactionManager transactionManager;
	
	@Autowired
	private AttendanceDao attendanceDao;
	
	@Autowired
	private ParticipantDao participantDao;
	
	@Override
	public boolean createAttendance(Attendance a) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try{
			result = attendanceDao.createAttendance(em, a);
			em.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			result = false;
			
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean updatettendance(Attendance a) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = attendanceDao.updateAttendance(em, a);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean deleteAttendance(Attendance a) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = attendanceDao.deleteAttendance(em, a);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}
	
	@Override
	public boolean createAttendanceByBatch(List<Participation> p, Date date) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		List<Attendance> a = new ArrayList<Attendance>();
    	
		//create attendance object with participant and date
    	for(Participation part : p){
			Attendance at = new Attendance(new AttendancePK(part.getParticipantID(),date));
			a.add(at);
    	}
    	
		em.getTransaction().begin();
		
		try{
			result = attendanceDao.createAttendanceBatch(em, a);
			em.getTransaction().commit();
			
		}catch(Exception e){
			e.printStackTrace();
			result = false;
			
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}
	

	@Override
	public boolean deleteAttendanceByBatch(List<Attendance> a) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = attendanceDao.deleteAttendanceBatch(em, a);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}
	
	@Override
	public boolean updateAttendanceByBatch(List<Attendance> a) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = attendanceDao.updateAttendanceBatch(em, a);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	
	
	@Override
	public List<Attendance> getAllAttendance() {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Attendance> result = null;
		
		try{
			result = attendanceDao.getAllAttendance(em);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public List<Attendance> getAttendance(List<Participation> p, Date date) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Attendance> result = null;
		
		try{
			result = attendanceDao.getAttendance(em, p, date);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}
	
	@Override
	public List<Attendance> getAttendanceByTrainingID(Training t) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Attendance> result = null;
		
		List<Participation> p = getParticipants(t); 
		
		try{
			result = attendanceDao.getAttendanceByTrainingID(em, p);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}
	
	
	
	@Override
	public List<Date> generateDate(Date startDate, Date endDate) {
		List<Date> dates = new ArrayList<Date>();
    	
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(startDate);
    	
    	while (cal.getTime().before(endDate)) {
    	    dates.add(cal.getTime());
    	    cal.add(Calendar.DATE, 1);
    	}
 
    	dates.add(cal.getTime());
    	
    	return dates;
	}
	
	@Override
	public List<Date> getDates(Training t) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Date> result = null;
		
		List<Participation> p = getParticipants(t);
		
		try{
			result = attendanceDao.getDates(em, p);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}
	
	//participants TODO

	@Override
	public boolean addParticipant(Participation p) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try{
			result = participantDao.addParticipant(em, p);
			em.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			result = false;
			
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}
	
	@Override
	public boolean addParticipantsBatch(Training t, List<Employee> Employee) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		List<Participation> p = new ArrayList<Participation>();
		
		for(Employee emp : Employee){
	    	Participation pp = new Participation(t,emp);
	    	p.add(pp);
    	}
		
		em.getTransaction().begin();
		
		try{
			result = participantDao.addParticipants(em, p);
			em.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			result = false;
			
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean updateParticipant(Participation p) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = participantDao.updateParticipant(em, p);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean deleteParticipant(Participation p) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = participantDao.deleteParticipant(em, p);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean deleteParticipantsBatch(List<Participation> p) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
	
		em.getTransaction().begin();
		
		try{
			result = participantDao.deleteParticipants(em, p);
			em.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			result = false;
			
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}
	
	@Override
	public List<Participation> getParticipants(Training t) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Participation> result = null;
		
		try{
			result = participantDao.getParticipants(em, t);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public Participation getParticipant(String employeeID, int trainingID) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		Participation result = null;
		
		try{
			result = participantDao.getParticipant(em, employeeID, trainingID);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}





}
