package ph.com.alliance.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.FormDao;
import ph.com.alliance.dao.FormDetailDao;
import ph.com.alliance.dao.FormResultDao;
import ph.com.alliance.dao.FormTypeDao;
import ph.com.alliance.dao.PendingDao;
import ph.com.alliance.entity.Employee;
import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.FormDetail;
import ph.com.alliance.entity.FormResult;
import ph.com.alliance.entity.FormType;
import ph.com.alliance.entity.Participation;
import ph.com.alliance.entity.Pending;
import ph.com.alliance.entity.Training;
import ph.com.alliance.service.FormService;

@Service("formService")
public class FormServiceImpl implements FormService{

	@Autowired
	private JpaTransactionManager transactionManager;
	
	@Autowired
	private FormDao formDao;
	
	@Autowired
	private FormDetailDao formDetailDao;
	
	@Autowired
	private FormResultDao formResultDao;
	
	@Autowired
	private FormTypeDao	 formTypeDao;
	
	@Autowired
	private PendingDao pendingDao;
	
	//form TODO
	
	@Override
	public boolean createForm(Form f) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try{
			result = formDao.createForm(em, f);
			em.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			result = false;
			
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean updateForm(Form f) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = formDao.updateForm(em, f);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean deleteForm(Form f) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {	
			result = formDao.deleteForm(em, f);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}
	
	@Override
	public boolean createForms(List<Form> f) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try{
			result = formDao.createForms(em, f);
			em.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			result = false;
			
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}


	@Override
	public Form getForm(int trainingID, FormType ft) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		Form result = null;
		
		try{
			result = formDao.getForm(em, trainingID, ft);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public List<Form> getFormbyTraining(Training t) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Form> result = null;
		
		try{
			result = formDao.getFormbyTraining(em, t);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public Form getFormID(int id) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		Form result = null;
		
		try{
			result = formDao.getFormByID(em, id);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public int isReleased(Form ff) {
		
		int result = -1;
		
		Form f = getForm(ff.getTraining().getTrainingID(), ff.getFormTypeBean());
		
		if(f.getFormFor().equals("Pre")){
			result = 0;
		}else if(f.getFormFor().equals("Post")){
			result = 1;
		}
		
		return result;
	}
	
	//form detail TODO
	
	@Override
	public List<FormDetail> getFormDetailByFormID(Form f) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<FormDetail> result = null;
		
		try{
			result = formDetailDao.getFormDetailByFormID(em, f);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public List<FormDetail> getQuestionList(int type) {
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		List<FormDetail> myQuestionList = null;
		try{
			myQuestionList = formDetailDao.getQuestions(em,type);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(em.isOpen())
				em.close();
		}
		return myQuestionList;
	}

	@Override
	public List<String> getCategoryList(int id, int type) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		
		List<String> myQuestionList = null;
		
		try{
			myQuestionList = formDetailDao.getCategories(em,id,type);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(em.isOpen())
				em.close();
		}
		return myQuestionList;
	}

	@Override
	public List<FormDetail> getChecks() {
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		List<FormDetail> myChecksList = null;
		try{
			myChecksList = formDetailDao.getChecks(em);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(em.isOpen())
				em.close();
		}
		return myChecksList;
	}
	

	
	@Override
	public FormType getFormType(String type) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		FormType result = null;
		
		try{
			result = formTypeDao.getFormType(em, type);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	//form result TODO
	
	@Override
	public boolean createFormResult(FormResult fr) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try{
			result = formResultDao.createFormResult(em, fr);
			em.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			result = false;
			
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean createFormResults(List<FormResult> fr) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try{
			result = formResultDao.createFormResults(em, fr);
			em.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			result = false;
			
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean updateFormResults(List<FormResult> fr) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = formResultDao.updateFormResults(em, fr);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean deleteFormResults(List<FormResult> fr) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = formResultDao.deleteFormResults(em, fr);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public List<FormResult> getResults(Form f) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<FormResult> result = null;
		
		try{
			result = formResultDao.getResults(em, f);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}
	
	
	

	//pending TODO
	
	@Override
	public boolean addPending(List<Pending> in) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try{
			result = pendingDao.addPending(em, in);
			em.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			result = false;
			
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean updatePending(List<Pending> in) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = pendingDao.updatePending(em, in);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean deletePending(List<Pending> in) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = pendingDao.deletePending(em, in);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean updatePendingObject(Pending p) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = pendingDao.updatePending(em, p);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public Pending getPendingByID(int id) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		Pending result = null;
		
		try{
			result = pendingDao.getPendingByID(em, id);
		}catch(Exception q){
			q.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}
	
	@Override
	public List<Pending> getPending(Employee e) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Pending> result = null;
		
		try{
			result = pendingDao.getPending(em, e);
		}catch(Exception q){
			q.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}
	
	@Override
	public List<Pending> getPendingFaci(Employee e) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Pending> result = null;
		
		try{
			result = pendingDao.getPendingFaci(em, e);
		}catch(Exception q){
			q.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	//results service TODO

	@Override
	public Map<FormDetail, int[]>  getReport(Form f) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		
		Map<FormDetail, int[]> mp = new LinkedHashMap<FormDetail, int[]>();
		
		List<FormDetail> fd = getFormDetailByFormID(f);
		
		if(f.getFormTypeBean().getFormType().equals("Course Feedback")){
			mp.putAll(getCheckBox(f));
		}
		
		try{
			for(FormDetail af : fd){
				
				List<FormResult> result = formResultDao.getResultsByQuestionNum(em, f, af.getQuestionNumber());
				
				if(result.size()>0){
					
					if(isInteger(result.get(0).getAnswerString()) && !(af.getCategory().equals("option")) && !(af.getQuestionType().equals("Check"))){
						
							mp.put(af, accumulator(result,getMaxArray(af.getQuestionType())));
						
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return mp;
	}
	
	@Override
	public Map<FormDetail, int[]> getCheckBox(Form f) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		
		List<FormDetail> fd = getFormDetailByFormID(f);
		FormDetail fdd = null;
		
		int[] accumulated = new int[10];
		int count = 0;
		
		Iterator<FormDetail> itFD = fd.iterator();
		
		while(itFD.hasNext()){
			FormDetail aa = itFD.next();
			if(aa.getQuestionType().equals("Check")){
				fdd = aa;
			}
			
			if(!(aa.getCategory().equals("option"))){
				itFD.remove();
			}
		}
		
		try{
		
			for(FormDetail aa : fd){
				List<FormResult> fr = formResultDao.getResultsForCheckBox(em, f, aa);
				
				for(FormResult result : fr){
					if(result.getAnswerString().equals("1")){
						accumulated[count] = accumulated[count] + 1;
					}
				}
				
				count++;
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		Map<FormDetail, int[]> mp = new LinkedHashMap<FormDetail, int[]>();
		mp.put(fdd, accumulated);
		
		return mp;
	}
	
	
	@Override
	public List<FormDetail> getReportQuestions(Form f) {

		List<FormDetail> fd = getFormDetailByFormID(f);
		
		Iterator<FormDetail> itFD = fd.iterator();
		
		while(itFD.hasNext()){
			FormDetail ff = itFD.next();
			
			if(ff.getQuestionType().equals("check") || ff.getQuestionType().equals("none") || ff.getQuestionType().equals("Essay")){
					itFD.remove();
			}
		}
		
		return fd;
	}

	public int[] accumulator(List<FormResult> fr, int max) {
		int[] test = new int[max];

		for(FormResult ar : fr){
			switch(ar.getAnswerString()){
			
			case "0":
				test[0] = test[0] + 1;
				break;
			case "1":
				test[1] = test[1] + 1;
				break;
			case "2":
				test[2] = test[2] + 1;
				break;
			case "3":
				test[3] = test[3] + 1;
				break;
			case "4":
				test[4] = test[4] + 1;
				break;
			case "5":
				test[5] = test[5] + 1;
				break;
			case "6":
				test[6] = test[6] + 1;
				break;
			}
		}
		return test;
	}
	
	public int getMaxArray(String type){
		int num = 0;

		if(type.equals("Suitable")|| type.equals("Objectives")){
			num=4;
		}else if(type.equals("Well")){
			num=6;
		}else{
			num=5;
		}

		return num;
	}

	public boolean isInteger(String str) {
	    if (str == null) {
	        return false;
	    }
	    int length = str.length();
	    if (length == 0) {
	        return false;
	    }
	    int i = 0;
	    if (str.charAt(0) == '-') {
	        if (length == 1) {
	            return false;
	        }
	        i = 1;
	    }
	    for (; i < length; i++) {
	        char c = str.charAt(i);
	        if (c < '0' || c > '9') {
	            return false;
	        }
	    }
	    return true;
	}

	//release
	
	@Override
	public boolean releaseToParticipants(List<Participation> p, Form f) {
		boolean success = false;
		List<Pending> in = new ArrayList<Pending>();
		
		for(Participation pp : p){
			Pending newPending = new Pending(pp.getEmployee(),f);
			
			in.add(newPending);
		}
		
		success = addPending(in);
		
		return success;
	}

	@Override
	public boolean releaseToSupervisor(List<Participation> p, Form f) {
		boolean success = false;
		List<Pending> in = new ArrayList<Pending>();
		
		for(Participation pp : p){
			
			Pending newPending = new Pending(pp.getEmployee().getEmployee2(),f);
			newPending.setParticipation(pp);
			
			in.add(newPending);
		}
		
		success = addPending(in);
		
		return success;
	}

}
