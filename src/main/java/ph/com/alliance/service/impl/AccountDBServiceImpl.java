package ph.com.alliance.service.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.EmployeeDao;
import ph.com.alliance.entity.Employee;
import ph.com.alliance.service.AccountDBService;

@Service("accountDBSerivce")
public class AccountDBServiceImpl implements AccountDBService{

	@Autowired
	private EmployeeDao employeeDao;
	
	@Autowired
	private JpaTransactionManager transactionManager;

	@Override
	public Employee getLogin(Employee acc) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		Employee a = null;
		
		try{
			a = employeeDao.getLogin(em, acc);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return a;
	}

	@Override
	public Employee getUserByID(String id) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		Employee a = null;
		
		try{
			a = employeeDao.getUserByID(em, id);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return a;
	}
	
	@Override
	public List<Employee> getAll() {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Employee> a = null;
		
		try{
			a = employeeDao.getAllEmployee(em);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return a;
	}

	
	@Override
	public List<Employee> getUserByName(String name) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Employee> a = null;
		
		try{
			a = employeeDao.getUserByName(em, name);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return a;
	}

	@Override
	public boolean changePass(Employee acc) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = employeeDao.updateEmployee(em, acc);
			em.getTransaction().commit();
			
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean isAdmin(Employee acc) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;

		try{
			result = employeeDao.isAdmin(em, acc);	
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}




}
