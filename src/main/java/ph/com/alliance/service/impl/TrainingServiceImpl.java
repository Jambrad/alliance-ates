package ph.com.alliance.service.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.TrainingDao;
import ph.com.alliance.dao.TrainingPlanDao;
import ph.com.alliance.entity.Employee;
import ph.com.alliance.entity.Training;
import ph.com.alliance.entity.TrainingPlan;
import ph.com.alliance.service.TrainingService;

@Service("trainingService")
public class TrainingServiceImpl implements TrainingService{
	
	@Autowired
	private JpaTransactionManager transactionManager;
	
	@Autowired
	private TrainingDao trainingDao;
	
	@Autowired
	private TrainingPlanDao trainingPlanDao;
	
	@Override
	public boolean createTrainingPlan(TrainingPlan p) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try{
			result = trainingPlanDao.createTrainingPlan(em, p);
			em.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			result = false;
			
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean updateTrainingPlan(TrainingPlan p) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = trainingPlanDao.updateTrainingPlan(em, p);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean deleteTrainingPlan(int year) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = trainingPlanDao.deleteTrainingPlan(em, year);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public List<TrainingPlan> getAllTrainingPlan() {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<TrainingPlan> result = null;
		
		try{
			result = trainingPlanDao.getAllTrainingPlan(em);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public TrainingPlan getByYear(int year) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		TrainingPlan result = null;
		
		try{
			result = trainingPlanDao.getByYear(em, year);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public TrainingPlan getByID(int id) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		TrainingPlan result = null;
		
		try{
			result = trainingPlanDao.getByID(em, id);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}
	
	@Override
	public int getLatestYear() {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		int result = 0;
		
		try{
			result = trainingPlanDao.getMaxYear(em);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	
///training TODO
	
	@Override
	public boolean createTraining(Training t) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try{
			result = trainingDao.createTraining(em, t);
			em.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			result = false;
			
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean updateTraining(Training t) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = trainingDao.updateTraining(em, t);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public boolean deleteTrainingByID(int id) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = trainingDao.deleteTrainingByID(em, id);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}
	
	@Override
	public boolean deleteTrainingBatch(List<Integer> id) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		
		try {
			result = trainingDao.deleteTrainingBatch(em, id);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public List<Training> getAllTraining() {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Training> result = null;
		
		try{
			result = trainingDao.getAllTraining(em);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public List<Training> getByTrainingPlanID(int id) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Training> result = null;
		
		try{
			result = trainingDao.getByTPID(em, id);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public Training getByTrainingID(int id) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		Training result = null;
		
		try{
			result = trainingDao.getByTrainingID(em, id);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public List<Training> getByTitle(String title) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Training> result = null;
		
		try{
			result = trainingDao.getByTitle(em, title);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}


	@Override
	public boolean isFacilitator(Employee emp){
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = true;

		try{
			result = trainingDao.isFacilitator(em, emp);	
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		if(result){
			System.out.println("FACILITATOR "+emp.getEmployeeID());
		}
		else{
			System.out.println("nope");
		}
		
		return result;
	}
	
	public List<Training> getTrainingByFacilitator(Employee faci){
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Training> result = null;

		try{
			result = trainingDao.getTrainingByFacilitator(em, faci);	
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

}
