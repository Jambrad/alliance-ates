package ph.com.alliance.service;

import java.util.Date;
import java.util.List;




import ph.com.alliance.entity.Attendance;
import ph.com.alliance.entity.Employee;
import ph.com.alliance.entity.Participation;
import ph.com.alliance.entity.Training;

public interface AttendanceService {
	
	/**
	 * This function creates a new Attendance in the DB.
	 * 
	 * @param a
	 * @return true, if new attendance is added
	 * 			false, if otherwise
	 */
	public boolean createAttendance(Attendance a);
	
	/**
	 * This function updates an existing Attendance.
	 * 
	 * @param a
	 * @return true, if update success
	 * 			false, if otherwise
	 */
	public boolean updatettendance(Attendance a);
	
	/**
	 * This function deletes an existing Attendance.
	 * 
	 * @param a
	 * @return	true, if attendance is deleted
	 * 			false, if otherwise
	 */
	public boolean deleteAttendance(Attendance a);
	
	/**
	 * This function creates Attendance by batch from a specific date.
	 * 
	 * @param p
	 * @param date
	 * @return true, if all is added
	 * 			false, if otherwise
	 */
	public boolean createAttendanceByBatch(List<Participation> p, Date date);
	
	/**
	 * This function deletes attendance by batch.
	 * 
	 * @param a
	 * @return true, if all list of attendance is deleted
	 * 			false, if otherwise
	 */
	public boolean deleteAttendanceByBatch(List<Attendance> a);
	
	/**
	 * This function updates attendance by batch.
	 * 
	 * @param a
	 * @return true, if all list of attendance is updated
	 * 			false, if otherwise
	 */
	public boolean updateAttendanceByBatch(List<Attendance> a);
	
	/** 
	 * This function retrieves all attendance.
	 * @return list of attendance
	 */
	public List<Attendance> getAllAttendance();
	
	/**
	 * This function retrieves all attendance by a specific date and list of participants.
	 * @param p
	 * @param date
	 * @return list of attendance
	 */
	public List<Attendance> getAttendance(List<Participation> p, Date date);
	
	/**
	 * This function retrieves all attendance by a specific training.
	 * @param t
	 * @return list of attendance
	 */
	public List<Attendance> getAttendanceByTrainingID(Training t);
	
	/**
	 * This function generates a list of dates from a starting to and end point.
	 * @param startDate
	 * @param endDate
	 * @return list of dates.
	 */
	public List<Date> generateDate(Date startDate, Date endDate);
	
	/**
	 * This function retrieves a list of distinct dates of a specific training.
	 * @param t
	 * @return list of dates
	 */
	public List<Date> getDates(Training t);
	
	/**
	 * This function adds a new participant.
	 * @param p
	 * @return true, if added
	 * 			false, if otherwise
	 */
	public boolean addParticipant(Participation p);
	
	/**
	 * This function updates an existing participant
	 * @param p
	 * @return true, if updated
	 * 			false, if otherwise
	 */
	public boolean updateParticipant(Participation p);
	
	/**
	 * This function deletes an existing participant
	 * @param p
	 * @return true, if deleted
	 * 			false, if otherwise
	 */
	public boolean deleteParticipant(Participation p);
	
	/**
	 * This function adds participants by batch.
	 * @param t
	 * @param e
	 * @return true, if all employees added
	 * 			false, if otherwise
	 */
	public boolean addParticipantsBatch(Training t, List<Employee> e);
	
	/**
	 * This function deletes participants by batch.
	 * @param p
	 * @return true, if all deleted
	 * 			false, if otherwise
	 */
	public boolean deleteParticipantsBatch(List<Participation> p);
	
	
	/**
	 * This function retrieves a list of participants by a specific training
	 * @param t
	 * @return list of participants
	 */
	public List<Participation> getParticipants(Training t);
	
	/**
	 * This function retrieves a specific participant.
	 * @param employeeID
	 * @param trainingID
	 * @return participation object
	 */
	public Participation getParticipant(String employeeID, int trainingID);
	
}
