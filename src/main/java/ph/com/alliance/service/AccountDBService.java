package ph.com.alliance.service;

import java.util.List;

import ph.com.alliance.entity.Employee;

public interface AccountDBService {
	
	/**
	 * A function that returns an Employee object after verifying Username and password.
	 * 
	 * @param acc
	 * @return Employee
	 */
	public Employee getLogin(Employee acc);
	
	/**
	 * A function that returns an Employee by its ID.
	 * 
	 * @param id
	 * @return Employee
	 */
	public Employee getUserByID(String id);
	
	/**
	 * A function that gets all employees.
	 * 
	 * @return list of all Employees
	 */
	public List<Employee> getAll();
	
	/**
	 * A function that searches an Employee by its name.
	 * 
	 * @param name
	 * @return list of Employees
	 */
	public List<Employee> getUserByName(String name);
	
	/**
	 * A function that changes the password on an Employee
	 * 
	 * @param acc
	 * @return true, if Employee object updated successfully
	 * 			false, otherwise
	 */
	public boolean changePass(Employee acc);
	
	/**
	 * A function that checks if an Employee is an admin
	 * 
	 * @param acc
	 * @return true, if admin
	 * 			false, otherwise
	 */
	public boolean isAdmin(Employee acc);
	
	
}
