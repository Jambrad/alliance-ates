package ph.com.alliance.service;

import java.util.List;
import java.util.Map;

import ph.com.alliance.entity.Employee;
import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.FormDetail;
import ph.com.alliance.entity.FormResult;
import ph.com.alliance.entity.FormType;
import ph.com.alliance.entity.Participation;
import ph.com.alliance.entity.Pending;
import ph.com.alliance.entity.Training;

public interface FormService {

	/**
	 * A function that creates a new form.
	 * 
	 * @param f
	 * @return true, if form is successfully created
	 * 		   false, otherwise
	 */
	public boolean createForm(Form f);
	
	/**
	 * A function that updates an existing form.
	 * 
	 * @param f
	 * @return true, if form is successfully updated
	 * 		   false, otherwise
	 */
	public boolean updateForm(Form f);
	
	/**
	 * A function that deletes an existing form.
	 * 
	 * @param f
	 * @return true, if form is successfully deleted
	 * 		   false, otherwise
	 */		  
	public boolean deleteForm(Form f);
	
	/**
	 * A function that creates multiple forms.
	 * 
	 * @param f
	 * @return true, if forms are successfully created
	 * 		   false, otherwise
	 */
	public boolean createForms(List<Form> f);
	
	/**
	 * A function that retrieves a form from a specific training and of a specific type.
	 * 
	 * @param trainingID
	 * @param ft
	 * @return Form Entity
	 */
	public Form getForm(int trainingID, FormType ft);
	
	/**
	 * A function that retrieves all forms for a specific training.
	 * 
	 * @param t
	 * @return Form Entities
	 */
	public List<Form> getFormbyTraining(Training t);
	
	/**
	 * A function that retrieves a form.
	 * 
	 * @param id
	 * @return Form Entity
	 */
	public Form getFormID(int id);
	
	/**
	 * A function that determines if a form is released or not.
	 * 
	 * @param f
	 * @return Integer flag
	 */
	public int isReleased(Form f);
	
	/**
	 * A function that retrieves form details for a specific form.
	 * 
	 * @param f
	 * @return FormDetail Entities
	 */
	public List<FormDetail> getFormDetailByFormID(Form f);
	
	/**
	 * A function that gets all questions for a specific type of form.
	 * 
	 * @param type
	 * @return FormDetail Entities
	 */
	public List<FormDetail> getQuestionList(int type);
	
	/**
	 * A function that retrieves categories of qustions for a specific form and type.
	 * 
	 * @param id
	 * @param type
	 * @return String list
	 */
	public List<String> getCategoryList(int id,int type);
	
	/**
	 * A function that retrieves all the checkboxes options.
	 * 
	 * @return FormDetail Entities
	 */
	public List<FormDetail> getChecks();
	
	/**
	 * A function that retrieves form type.
	 * 
	 * @param type
	 * @return FormType Entities
	 */ 
	public FormType getFormType(String type);
	

	/**
	 * This function creates a new form result.
	 * @param fr
	 * @return true, if created
	 * 			false, if otherwise
	 */
	public boolean createFormResult(FormResult fr);
	
	/**
	 * This function creates a new form result by batch.
	 * @param fr
	 * @return true, if created
	 * 			false, if otherwise
	 */
	public boolean createFormResults(List<FormResult> fr);
	
	/**
	 * This function updates a list of existing form results.
	 * @param fr
	 * @return true, if updated
	 * 			false, if otherwise
	 */
	public boolean updateFormResults(List<FormResult> fr);
	
	/**
	 * This function deletes a list of existing form results.
	 * @param fr
	 * @return true, if deleted
	 * 			false, if otherwise
	 */
	public boolean deleteFormResults(List<FormResult> fr);
	
	/**
	 * This function retrieves a list of form results by a specific form.
	 * @param f
	 * @return list of form results
	 */
	public List<FormResult> getResults(Form f);

	/**
	 * This function creates a new pending by batch.
	 * @param in
	 * @return true, if created
	 * 			false, if otherwise
	 */
	public boolean addPending(List<Pending> in);
	
	/**
	 * This functions updates an existing pending by batch.
	 * @param in
	 * @return true, if updated
	 * 			false, if otherwise
	 */
	public boolean updatePending(List<Pending> in);
	
	/**
	 * This function deletes an existing pending by batch.
	 * @param in
	 * @return true, if deleted
	 * 			false, if otherwise
	 */
	public boolean deletePending(List<Pending> in);
	
	/**
	 * This function updates an existing pending.
	 * @param p
	 * @return true, if updated
	 * 			false, if otherwise
	 */
	public boolean updatePendingObject(Pending p);
	
	/**
	 * This function retrieves a pending by its ID.
	 * @param id
	 * @return pending object
	 */
	public Pending getPendingByID(int id);

	/**
	 * This function retrieves a list of pending by employee.
	 * @param e
	 * @return list of pending
	 */
	public List<Pending> getPending(Employee e);
	
	/**
	 * This function retrieves a list of pending for facilitator.
	 * @param emp
	 * @return
	 */
	public List<Pending> getPendingFaci(Employee emp);
	
	/**
	 * This function generates a map reports with formdetail and array 
	 * of accumulated answers by number.
	 * @param f
	 * @return map of formdetails and array of answers by number
	 */
	public Map<FormDetail, int[]>  getReport(Form f);
	
	/**
	 * This functions retrieves a list of all quantifiable questions per form.
	 * @param f
	 * @return list of form details
	 */
	public List<FormDetail> getReportQuestions(Form f);
	
	/**
	 * This function generates a report for a specific type of question(checkbox).
	 * @param fd
	 * @return map of formdetails and array of answers
	 */
	public Map<FormDetail, int[]> getCheckBox(Form fd);

	/**
	 * This function creates new pendings to specific participants and 
	 * a specific form.
	 * @param p
	 * @param f
	 * @return true, if released
	 * 			false, if otherwise
	 */
	public boolean releaseToParticipants(List<Participation> p, Form f);
	
	/**
	 * This function creates new pendings to specific supervisors and
	 * a specific form.
	 * @param p
	 * @param f
	 * @return true, if released
	 * 			false, if otherwise
	 */
	public boolean releaseToSupervisor(List<Participation> p, Form f);

	
	
	
}
