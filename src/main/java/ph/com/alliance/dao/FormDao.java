package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.FormType;
import ph.com.alliance.entity.Training;

public interface FormDao {
	
	/**
	 * A function that creates form
	 * 
	 * @param em
	 * @param f
	 * @return true, if form is created successfully
	 * 		   false, otherwise
	 */
	public boolean createForm(EntityManager em, Form f);
	
	/**
	 * A function that updates a form
	 * 
	 * @param em
	 * @param f
	 * @return true, if form updated successfully
	 * 		   false, otherwise
	 */
	public boolean updateForm(EntityManager em, Form f);
	
	/**
	 * A function that deletes a form
	 * 
	 * @param em
	 * @param f
	 * @return true, if form is successfully deleted
	 * 		   false, otherwise
	 */
	public boolean deleteForm(EntityManager em, Form f);
	
	/**
	 * A function that creates form by batch
	 * 
	 * @param em
	 * @param f
	 * @return true, if forms are created successfully
	 * 		   false, otherwise
	 */
	public boolean createForms(EntityManager em, List<Form> f);
	
	/**
	 * A function that retrieves all forms for a specific training
	 * 
	 * @param em
	 * @param t
	 * @return Form Entities
	 */
	public List<Form> getFormbyTraining(EntityManager em, Training t);
	
	/**
	 * A function that retrieves form by its form ID
	 * 
	 * @param em
	 * @param id
	 * @return Form Entity
	 */
	public Form getFormByID(EntityManager em, int id);
			
	/**
	 * A function that a specific form type for a specific training
	 * 
	 * @param em
	 * @param trainingID
	 * @param ft
	 * @return Form Entity
	 */
	public Form getForm(EntityManager em, int trainingID, FormType ft);
}
