package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Employee;

public interface EmployeeDao {
	
	/**
	 * A function that checks employee's username and password and returns the employee Entity if existing
	 * 
	 * @param em
	 * @param acc
	 * @return Employee Entity
	 */
	public Employee getLogin(EntityManager em, Employee acc);
	
	/**
	 * A function that retrieves an employee by its ID
	 * 
	 * @param em
	 * @param id
	 * @return Employee Entity
	 */
	public Employee getUserByID(EntityManager em, String id);
	
	/**
	 * A function that retrieves all employees in the system
	 * 
	 * @param em
	 * @return Employee Entities
	 */
	public List<Employee> getAllEmployee(EntityManager em);	
	
	/**
	 * A function that retrieve an employee using its name
	 * 
	 * @param em
	 * @param name
	 * @return Employee Entities
	 */
	public List<Employee> getUserByName(EntityManager em, String name);		
	
	/**
	 * A function that updates an employee
	 * 
	 * @param em
	 * @param acc
	 * @return true, if employee is updated successfully
	 */
	public boolean updateEmployee(EntityManager em, Employee acc);
	
	/**
	 * A function that determines if an employee has an administrator privilege
	 * 
	 * @param em
	 * @param acc
	 * @return true, if an employee is an administrator
	 * 		   false, otherwise
	 */
	public boolean isAdmin(EntityManager em, Employee acc);
}
