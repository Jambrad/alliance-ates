package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Employee;
import ph.com.alliance.entity.Pending;

public interface PendingDao {
	
	/**
	 * A function that adds released forms to pending table
	 * 
	 * @param em
	 * @param in
	 * @return true, if forms are added successfully
	 * 		   false, otherwise
	 */
	public boolean addPending(EntityManager em, List<Pending> in);
	
	/**
	 * A function that updates pending forms
	 * 
	 * @param em
	 * @param in
	 * @return true, if forms are updated successfully
	 * 		   false, otherwise
	 */
	public boolean updatePending(EntityManager em, List<Pending> in);
	
	/**
	 * A function that deletes pending forms
	 * 
	 * @param em
	 * @param in
	 * @return true, if forms are deleted successfully
	 * 		   false, otherwise
	 */
	public boolean deletePending(EntityManager em, List<Pending> in);
	
	/**
	 * A function that updates a specific pending form
	 * 
	 * @param em
	 * @param p
	 * @return true, if form is updated successfully
	 * 		   false, otherwise
	 */
	public boolean updatePending(EntityManager em, Pending p);
	
	/**
	 * A function that retrieves Pending Entity using Pending ID
	 * 
	 * @param em
	 * @param id
	 * @return Pending Entity
	 */
	public Pending getPendingByID(EntityManager em, int id);
	
	/**
	 * A function that retrieves pending forms for a specific employee
	 * 
	 * @param em
	 * @param e
	 * @return Pending Entities
	 */
	public List<Pending> getPending(EntityManager em, Employee e);
	
	/**
	 * A function that retrieves pending TNA forms
	 * 
	 * @param em
	 * @param e
	 * @return Pending Entities
	 */
	public List<Pending> getPendingFaci(EntityManager em, Employee e);
}
