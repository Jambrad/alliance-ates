package ph.com.alliance.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Attendance;
import ph.com.alliance.entity.Participation;


public interface AttendanceDao {

	/**
	 * A function that creates attendance
	 * 
	 * @param em
	 * @param a
	 * @return true, if attendance is created successfully
	 * 		   false, otherwise
	 */
	public boolean createAttendance(EntityManager em, Attendance a);

	/**
	 * A function that updates an attendance
	 * 
	 * @param em
	 * @param a
	 * @return true, if attendance is updated successfully
	 * 		   false, otherwise
	 */
	public boolean updateAttendance(EntityManager em, Attendance a);

	/**
	 * A function that deletes an attendance
	 * 
	 * @param em
	 * @param a
	 * @return true, if attendance is deleted successfully
	 * 		   false, otherwise
	 */
	public boolean deleteAttendance(EntityManager em, Attendance a);
	
	/**
	 * A function that creates attendance by batch
	 * 
	 * @param em
	 * @param a
	 * @return true, if attendance are created successfully
	 * 		   false, otherwise
	 */
	public boolean createAttendanceBatch(EntityManager em, List<Attendance> a);
	
	/**
	 * A function that deletes attendance by batch
	 * 
	 * @param em
	 * @param a
	 * @return true, if attendance are deleted successfully
	 */
	public boolean deleteAttendanceBatch(EntityManager em, List<Attendance> a);
	
	/**
	 * A function that updates attendance by batch
	 * 
	 * @param em
	 * @param a
	 * @return true, if attendance are updated successfully
	 * 		   false, otherwise
	 */
	public boolean updateAttendanceBatch(EntityManager em, List<Attendance> a);

	/**
	 * A function that retrieves all attendance
	 * 
	 * @param em
	 * @return Attendance Entities
	 */
	public List<Attendance> getAllAttendance(EntityManager em);
	
	/**
	 * A function that retrieves dates of attendance
	 * 
	 * @param em
	 * @param p
	 * @return Date 
	 */
	public List<Date> getDates(EntityManager em, List<Participation> p);
	
	/**
	 * A function that retrieves attendance for a specific training
	 * 
	 * @param em
	 * @param p
	 * @return Attendance Entities
	 */
	public List<Attendance> getAttendanceByTrainingID(EntityManager em, List<Participation> p);
	
	/**
	 * A function that retrieves attendance of a specific participant in a specific date
	 * 
	 * @param em
	 * @param p
	 * @param date
	 * @return
	 */
	public List<Attendance> getAttendance(EntityManager em, List<Participation> p, Date date);
}
