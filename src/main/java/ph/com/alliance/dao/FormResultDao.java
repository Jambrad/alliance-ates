package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.FormDetail;
import ph.com.alliance.entity.FormResult;

public interface FormResultDao {
	
	/**
	 * A function that adds employee's response to a form
	 * 
	 * @param em
	 * @param fr
	 * @return true, if result is added successfully
	 */
	public boolean createFormResult(EntityManager em, FormResult fr);
	
	/**
	 * A function that adds employee's response by batch
	 * 
	 * @param em
	 * @param fr
	 * @return true, if responses are added successfully
	 * 		   false, otherwise
	 */
	public boolean createFormResults(EntityManager em, List<FormResult> fr);
	
	/**
	 * A function that updates responses by batch
	 * 
	 * @param em
	 * @param fr
	 * @return true, if responses are updated successfully
	 */
	public boolean updateFormResults(EntityManager em, List<FormResult> fr);
	
	/**
	 * A function that deletes responses by batch
	 * 
	 * @param em
	 * @param fr
	 * @return true, if responses are deleted successfully
	 */
	public boolean deleteFormResults(EntityManager em, List<FormResult> fr);
	
	/**
	 * A function that retrieves responses for a specific form
	 * 
	 * @param em
	 * @param f
	 * @return FormResult Entities
	 */
	public List<FormResult> getResults(EntityManager em, Form f);
	
	/**
	 * A function that retrieves responses for a specific number from a specific form
	 * 
	 * @param em
	 * @param f
	 * @param questionNumber
	 * @return FromResult Entities
	 */
	public List<FormResult> getResultsByQuestionNum(EntityManager em, Form f, int questionNumber);
	
	/**
	 * 
	 * 
	 * @param em
	 * @param f
	 * @param fd
	 * @return FormResult Entities
	 */
	public List<FormResult> getResultsForCheckBox(EntityManager em, Form f, FormDetail fd);
}
