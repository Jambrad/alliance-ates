package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Employee;
import ph.com.alliance.entity.Training;

public interface TrainingDao {
	
	/**
	 * A function that creates training
	 * 
	 * @param em
	 * @param t
	 * @return true, if training is created successfully
	 * 		   false, otherwise
	 */
	public boolean createTraining(EntityManager em, Training t);
	
	/**
	 * A function that updates a training
	 * 
	 * @param em
	 * @param p
	 * @return true, if training is updated successfully
	 * 		   false, otherwise
	 */
	public boolean updateTraining(EntityManager em, Training p);
	
	/**
	 * A function that deletes a training
	 * 
	 * @param em
	 * @param id
	 * @return true, if training is deleted successfully
	 * 		   false, otherwise
	 */
	public boolean deleteTrainingByID(EntityManager em, int id);
	
	/**
	 * A function that deletes training by batch
	 * 
	 * @param em
	 * @param id
	 * @return true, if trainings are deleted successfully
	 * 		   false, otherwise
	 */
	public boolean deleteTrainingBatch(EntityManager em, List<Integer> id);
	
	/**
	 * A function that retrieves all trainings
	 * 
	 * @param em
	 * @return Training Entities
	 */
	public List<Training> getAllTraining(EntityManager em);
	
	/**
	 * A function that retrieves trainings under a specific training plan
	 * 
	 * @param em
	 * @param id
	 * @return Training Entities
	 */
	public List<Training> getByTPID(EntityManager em, int id);
	
	/**
	 * A function that retrieves a training using training ID
	 * 
	 * @param em
	 * @param id
	 * @return Training Entity
	 */
	public Training getByTrainingID(EntityManager em, int id);
	
	/**
	 * A function that retrieves trainings with similarities in training title
	 * 
	 * @param em
	 * @param title
	 * @return Training Entities
	 */
	public List<Training> getByTitle(EntityManager em, String title);
	
	/**
	 * A function that determines if an employee is a handling a training
	 * 
	 * @param em
	 * @param emp
	 * @return true, if employee is a facilitator
	 * 		   false, otherwise
	 */
	public boolean isFacilitator(EntityManager em, Employee emp);
	
	/**
	 * A function that retrieves all trainings handled by a specific facilitator
	 * 
	 * @param em
	 * @param faci
	 * @return Training Entities
	 */
	public List<Training> getTrainingByFacilitator(EntityManager em, Employee faci);
	
}
