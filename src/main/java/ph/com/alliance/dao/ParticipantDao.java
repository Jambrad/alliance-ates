package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Participation;
import ph.com.alliance.entity.Training;

public interface ParticipantDao {
	
	/**
	 * A function that adds a participant to a training
	 * 
	 * @param em
	 * @param p
	 * @return true, if participant is added successfully
	 * 		   false, otherwise
	 */
	public boolean addParticipant(EntityManager em, Participation p);
	
	/**
	 * A function that updates participation
	 * 
	 * @param em
	 * @param p
	 * @return true, if participation is updated successfully
	 * 		   false, otherwise
	 */
	public boolean updateParticipant(EntityManager em, Participation p);
	
	/**
	 * A function that deletes a participation
	 * 
	 * @param em
	 * @param p
	 * @return true, if participation is deleted successfully
	 * 		   false, otherwise
	 */
	public boolean deleteParticipant(EntityManager em, Participation p);
	
	/**
	 * A function that adds participations by batch
	 * 
	 * @param em
	 * @param p
	 * @return true, if participations are added successfully
	 * 		   false, otherwise
	 */
	public boolean addParticipants(EntityManager em, List<Participation> p);
	
	/**
	 * A function that deletes participations by batch
	 * 
	 * @param em
	 * @param p
	 * @return true, if participations are deleted successfully
	 * 		   false, otherwise
	 */
	public boolean deleteParticipants(EntityManager em, List<Participation> p);
	
	/**
	 * A function that retrieves all participants of a specific training
	 * 
	 * @param em
	 * @param t
	 * @return Participation Entities
	 */
	public List<Participation> getParticipants(EntityManager em, Training t);
	
	/**
	 * A function that retrieves a participation of a specific employee for a specific training
	 * 
	 * @param em
	 * @param employeeID
	 * @param trainingID
	 * @return Participation Entities
	 */
	public Participation getParticipant(EntityManager em, String employeeID, int trainingID);
	
}
