package ph.com.alliance.dao;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.FormType;

public interface FormTypeDao {
	
	/**
	 * A function that retrieves the form type of a type
	 * 
	 * @param em
	 * @param type
	 * @return FormType Entity
	 */
	public FormType getFormType(EntityManager em, String type);
	
}
