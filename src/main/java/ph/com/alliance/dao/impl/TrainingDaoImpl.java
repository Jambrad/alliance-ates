package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.TrainingDao;
import ph.com.alliance.entity.Employee;
import ph.com.alliance.entity.Training;

@Repository("trainingDao")
public class TrainingDaoImpl implements TrainingDao{

	@Override
	public boolean createTraining(EntityManager em, Training t) {
		boolean success = true;
		
		try{
			em.persist(t);
		}catch(EntityExistsException ee){
			ee.getMessage();
			success = false;
		}catch(IllegalArgumentException iae){
			iae.getMessage();
			success = false;
		}catch(TransactionRequiredException trxe){
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}

	@Override
	public boolean updateTraining(EntityManager em, Training p) {
		boolean success = true;
		
		try{
			em.merge(p);
		}catch (IllegalArgumentException iae) {
			success = false;
			iae.getMessage();
		} catch (TransactionRequiredException trxe) {
			success = false;
			trxe.getMessage();
		}
		
		return success;
	}

	@Override
	public boolean deleteTrainingByID(EntityManager em, int id) {
		boolean result = true;
		
		try{
			Query query = em.createQuery("DELETE From Training t WHERE t.trainingID = :id");
			query.setParameter("id", id);
			query.executeUpdate();
		}catch(TransactionRequiredException tre){
			result = false;
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			result = false;
			qte.getMessage();
		}
		
		return result;
	}
	
	@Override
	public boolean deleteTrainingBatch(EntityManager em, List<Integer> id) {
		boolean result = true;
		
		try{
			for(int a : id){
				Query query = em.createQuery("DELETE From Training t WHERE t.trainingID = :id");
				query.setParameter("id", a);
				query.executeUpdate();
			}
		
		}catch(TransactionRequiredException tre){
			tre.getMessage();
			result = false;
		}catch(QueryTimeoutException qte){
			qte.getMessage();
			result = false;
		}
		
		return result;
	}
	

	@Override
	public List<Training> getAllTraining(EntityManager em) {
		List<Training> t = null;
		
		try{
			Query query = em.createQuery("From Training");
			t = query.getResultList();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return t;
	}

	@Override
	public List<Training> getByTPID(EntityManager em, int id) {
		List<Training> t = null;
		
		try{
			Query query = em.createQuery("From Training WHERE trainingPlanID = :id");
			query.setParameter("id", id);
			
			t = query.getResultList();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return t;
	}

	@Override
	public Training getByTrainingID(EntityManager em, int id) {
		Training t = null;
		
		try{
			Query query = em.createQuery("From Training  t WHERE t.trainingID = :id");
			query.setParameter("id", id);
			
			t = (Training) query.getSingleResult();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return t;
	}

	@Override
	public List<Training> getByTitle(EntityManager em, String title) {
		List<Training> t = null;
		
		try{
			Query query = em.createQuery("From Training  t WHERE t.title LIKE :title");
			query.setParameter("title", "%"+title+"%");
			
			t = query.getResultList();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return t;
	}

	@Override
	public boolean isFacilitator(EntityManager em, Employee emp){
		List<Training> Trainings = null;
		System.out.println("DAODAODAODAO");
		try{
			Query query = em.createQuery("From Training Where facilitatorID = :id");
			query.setParameter("id",emp);
			
			Trainings = query.getResultList();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		
		if(Trainings.size() > 0){
			
			System.out.println("trainings "+Trainings.size());
			
			System.out.println("Employee is a facilitator..");
			return true;
		}
		else{

			return false;
		}
	}
	
	public List<Training> getTrainingByFacilitator(EntityManager em, Employee faci){
		List<Training> trainings = null;
		System.out.println("Getting Trainings of Faci: " + faci.getFirstName());
		try{
			Query query = em.createQuery("From Training Where facilitatorID = :id");
			query.setParameter("id",faci);
			
			trainings = query.getResultList();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		
		return trainings;
	}

}
