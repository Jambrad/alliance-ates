package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.TrainingPlanDao;
import ph.com.alliance.entity.TrainingPlan;

@Repository("trainingPlanDao")
public class TrainingPlanDaoImpl implements TrainingPlanDao{

	@Override
	public boolean createTrainingPlan(EntityManager em, TrainingPlan p) {
		boolean success = true;
		
		try{
			em.persist(p);
		}catch(EntityExistsException ee){
			ee.getMessage();
			success = false;
		}catch(IllegalArgumentException iae){
			iae.getMessage();
			success = false;
		}catch(TransactionRequiredException trxe){
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}

	@Override
	public boolean updateTrainingPlan(EntityManager em, TrainingPlan p) {
		boolean success = true;
		
		try{
			em.merge(p);
		}catch (IllegalArgumentException iae) {
			success = false;
			iae.getMessage();
		} catch (TransactionRequiredException trxe) {
			success = false;
			trxe.getMessage();
		}
		
		return success;
	}

	@Override
	public boolean deleteTrainingPlan(EntityManager em, int year) {
		boolean result = true;
		
		try{
		Query query = em.createQuery("DELETE From TrainingPlan  tp WHERE tp.year = :year");
		query.setParameter("year", year);
		query.executeUpdate();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
			result = false;
		}catch(QueryTimeoutException qte){
			qte.getMessage();
			result = false;
		}
		
		return result;
	}

	@Override
	public List<TrainingPlan> getAllTrainingPlan(EntityManager em) {
		List<TrainingPlan> tp = null;
		
		try{
			Query query = em.createQuery("From TrainingPlan");
			tp = query.getResultList();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return tp;
	}

	@Override
	public TrainingPlan getByYear(EntityManager em, int year) {
		TrainingPlan result = null;
		
		try{
			Query query = em.createQuery("From TrainingPlan tp Where tp.year = :year");
			query.setParameter("year", year);
		
			result = (TrainingPlan) query.getSingleResult();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		
		
		return result;
	}

	@Override
	public TrainingPlan getByID(EntityManager em, int id) {
		TrainingPlan result = null;
		
		try{
			Query query = em.createQuery("From TrainingPlan tp Where tp.trainingPlanID = :id");
			query.setParameter("id", id);
		
			result = (TrainingPlan) query.getSingleResult();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		
		
		return result;
	}
	
	@Override
	public int getMaxYear(EntityManager em) {
		int max = 0;
		
		try{
			Query query = em.createQuery("Select max(t.year) FROM TrainingPlan t");
			
			max = (int) query.getSingleResult();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return max;
	}
}
