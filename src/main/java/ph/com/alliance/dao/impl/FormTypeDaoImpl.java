package ph.com.alliance.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.FormTypeDao;
import ph.com.alliance.entity.FormType;

@Repository("formTypeDao")
public class FormTypeDaoImpl implements FormTypeDao{

	@Override
	public FormType getFormType(EntityManager em, String type) {
		FormType form = null;
		
		try{
	
			Query query = em.createQuery("From FormType Where formType = :type");
			query.setParameter("type", type);
	
			form = (FormType) query.getSingleResult();
			
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return form;
	}

}
