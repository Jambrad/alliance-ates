package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.FormDetailDao;
import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.FormDetail;
import ph.com.alliance.entity.FormType;

@Repository("formDetailDao")
public class FormDetailDaoImpl implements FormDetailDao{

	@Override
	public List<FormDetail> getFormDetailByFormID(EntityManager em, Form f) {
		List<FormDetail> form = null;
		
		try{
			Query query = em.createQuery("From FormDetail Where formType = :type ORDER BY questionNumber ASC");
			query.setParameter("type", f.getFormTypeBean().getTypeID());
			
			form = query.getResultList();
			
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return form;
	}

	@Override
	public List<FormDetail> getQuestions(EntityManager em, int type) {
		List<FormDetail> myQuestionList = null;
		Query myQuestionQuery = em.createQuery("FROM FormDetail f WHERE f.category <> :opt AND f.formTypeBean.typeID = :type");
		myQuestionQuery.setParameter("opt","option");
		myQuestionQuery.setParameter("type",type);
		try{
			myQuestionList = myQuestionQuery.getResultList();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return myQuestionList;
	}
	
	@Override
	public List<String> getCategories(EntityManager em, int id, int type) {
		List<String> myCategoryList = null;
		Query myCategoryQuery = em.createQuery("SELECT DISTINCT category FROM FormDetail WHERE formTypeBean.typeID = :type AND category <> :opt");
		myCategoryQuery.setParameter("type", type);
		myCategoryQuery.setParameter("opt", "option");
		try{
			myCategoryList = myCategoryQuery.getResultList();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return myCategoryList;
	}

	@Override
	public List<FormDetail> getChecks(EntityManager em) {
		List<FormDetail> myChecksList = null;
		Query myChecksQuery = em.createQuery("FROM FormDetail f WHERE f.category = :opt and f.formTypeBean.typeID = 3 ORDER BY f.detailID DESC");
		myChecksQuery.setParameter("opt", "option");
		try{
			myChecksList = myChecksQuery.getResultList();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return myChecksList;
	}

}
