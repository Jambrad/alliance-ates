package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.FormResultDao;
import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.FormDetail;
import ph.com.alliance.entity.FormResult;

@Repository("formResultDao")
public class FormResultDaoImpl implements FormResultDao{

	@Override
	public boolean createFormResult(EntityManager em, FormResult fr) {
		boolean success = true;
		
		try{
				em.persist(fr);
		}catch(EntityExistsException ee){
			ee.getMessage();
			success = false;
		}catch(IllegalArgumentException iae){
			iae.getMessage();
			success = false;
		}catch(TransactionRequiredException trxe){
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}

	@Override
	public boolean createFormResults(EntityManager em, List<FormResult> fr) {
		boolean success = true;
		
		try{
			for(FormResult f : fr){
				em.persist(f);
			}
			
		}catch(EntityExistsException ee){
			ee.getMessage();
			success = false;
		}catch(IllegalArgumentException iae){
			iae.getMessage();
			success = false;
		}catch(TransactionRequiredException trxe){
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}

	@Override
	public boolean updateFormResults(EntityManager em, List<FormResult> fr) {
		boolean success = true;
		
		try{
			
			for(FormResult f : fr){
				em.merge(f);
			}
			
		}catch (IllegalArgumentException iae) {
			iae.getMessage();
			success = false;
		} catch (TransactionRequiredException trxe) {
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}

	@Override
	public boolean deleteFormResults(EntityManager em, List<FormResult> fr) {
		boolean success = true;
		
		FormResult ff = null;
			
		try{
			for(FormResult f : fr){
				ff = em.find(FormResult.class, f.getResultID());
				em.remove(ff);
			}
			
		}catch (IllegalArgumentException iae) {
			success = false;
			iae.getMessage();
		} catch (TransactionRequiredException trxe) {
			success = false;
			trxe.getMessage();
		}
		
		return success;
	}

	@Override
	public List<FormResult> getResults(EntityManager em, Form f) {
		List<FormResult> form = null;
		
		try{
			Query query = em.createQuery("From FormResult Where formID = :id ");
			query.setParameter("id", f.getFormID());
		
			form = query.getResultList();
			
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return form;
	}

	@Override
	public List<FormResult> getResultsByQuestionNum(EntityManager em, Form f,
			int questionNumber) {
		List<FormResult> form = null;
		
		try{
			Query query = em.createQuery("From FormResult fr Where fr.form = :id AND fr.formDetail.questionNumber = :num");
			query.setParameter("id", f);
			query.setParameter("num", questionNumber);
		
			form = query.getResultList();
			
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return form;
	}

	@Override
	public List<FormResult> getResultsForCheckBox(EntityManager em, Form f,
			FormDetail fd) {
		List<FormResult> form = null;
		
		try{
			Query query = em.createQuery("From FormResult fr Where fr.form = :id AND fr.formDetail IN :fd");
			query.setParameter("id", f);
			query.setParameter("fd", fd);
		
			form = query.getResultList();
			
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return form;
	}

}
