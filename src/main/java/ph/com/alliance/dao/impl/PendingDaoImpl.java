package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.PendingDao;
import ph.com.alliance.entity.Employee;
import ph.com.alliance.entity.Pending;

@Repository("pendingDao")
public class PendingDaoImpl implements PendingDao{

	@Override
	public boolean addPending(EntityManager em, List<Pending> in) {
		boolean success = true;
		
		try{
			for(Pending p : in){
				em.persist(p);
			}
			
		}catch(EntityExistsException ee){
			ee.getMessage();
			success = false;
		}catch(IllegalArgumentException iae){
			iae.getMessage();
			success = false;
		}catch(TransactionRequiredException trxe){
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}

	@Override
	public boolean updatePending(EntityManager em, List<Pending> in) {
		boolean success = true;
		
		try{
			
			for(Pending p : in){
				em.merge(p);
			}
			
		}catch (IllegalArgumentException iae) {
			iae.getMessage();
			success = false;
		} catch (TransactionRequiredException trxe) {
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}

	@Override
	public boolean deletePending(EntityManager em, List<Pending> in) {
		boolean success = true;
		
		Pending ff = null;
			
		try{
			for(Pending p : in){
				ff = em.find(Pending.class, p.getPendingID());
				em.remove(ff);
			}
			
		}catch (IllegalArgumentException iae) {
			success = false;
			iae.getMessage();
		} catch (TransactionRequiredException trxe) {
			success = false;
			trxe.getMessage();
		}
		
		return success;
	}

	@Override
	public List<Pending> getPending(EntityManager em, Employee e) {
		List<Pending> form = null;
		
		try{
			Query query = em.createQuery("From Pending Where employeeID = :id AND isDone = 0");
			query.setParameter("id", e);
			
			form = query.getResultList();
			
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return form;
	}
	
	public List<Pending> getPendingFaci(EntityManager em, Employee e) {
		List<Pending> form = null;
		
		try{
			Query query = em.createQuery("From Pending p Where p.employee = :id AND p.isDone = 0 AND p.form.formTypeBean.typeID = :formType");
			query.setParameter("id", e);
			query.setParameter("formType", 2);
			
			form = query.getResultList();
			
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return form;
	}

	@Override
	public boolean updatePending(EntityManager em, Pending p) {
	boolean success = true;
		
		try{
			em.merge(p);
		}catch (IllegalArgumentException iae) {
			iae.getMessage();
			success = false;
		} catch (TransactionRequiredException trxe) {
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}

	@Override
	public Pending getPendingByID(EntityManager em, int id) {
		Pending p = null;
			
		try{
			p = em.find(Pending.class, id);
		}catch (IllegalArgumentException iae) {
			iae.getMessage();
		} catch (TransactionRequiredException trxe) {
			trxe.getMessage();
		}
		
		
		return p;
	}

}
