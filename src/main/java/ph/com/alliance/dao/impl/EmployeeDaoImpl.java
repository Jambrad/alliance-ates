package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.EmployeeDao;
import ph.com.alliance.entity.Admin;
import ph.com.alliance.entity.Employee;

@Repository("employeeDao")
public class EmployeeDaoImpl implements EmployeeDao{
	@Override
	public Employee getLogin(EntityManager em, Employee acc) {
		Employee a = null;
		
		try{
		Query query = em.createQuery("From Employee e WHERE e.employeeID = :username AND e.password = :pass");
		query.setParameter("username", acc.getEmployeeID());
		query.setParameter("pass", acc.getPassword());
		
		a = (Employee) query.getSingleResult();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		
		return a;
	}

	@Override
	public Employee getUserByID(EntityManager em, String id) {
		Employee a = null;
		
		try{
			Query query = em.createQuery("From Employee e Where e.employeeID = :id");
			query.setParameter("id", id);
		
			a = (Employee) query.getSingleResult();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		
		
		return a;
	}

	@Override
	public boolean updateEmployee(EntityManager em, Employee acc) {
		boolean success = true;
		
		try{
			em.merge(acc);
		}catch (IllegalArgumentException iae) {
			success = false;
			iae.getMessage();
		} catch (TransactionRequiredException trxe) {
			success = false;
			trxe.getMessage();
		}
		
		return success;
	}
	
	@Override
	public List<Employee> getUserByName(EntityManager em, String name) {
		List<Employee> a = null;
		
		try{
			Query query = em.createQuery("From Employee e Where e.firstName LIKE :name OR e.middleName LIKE :name OR e.lastName LIKE :name");
			query.setParameter("name", "%"+name+"%");
		
			a = query.getResultList();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		
		
		return a;
	}
	

	@Override
	public boolean isAdmin(EntityManager em, Employee acc) {
		boolean result = false;
		
		try{
		Query query = em.createQuery("From Admin Where employeeID = :id");
		query.setParameter("id", acc.getEmployeeID());
		
		
		result = ((Admin) query.getSingleResult() !=null);
		
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
	
		
		
		return result;
	}

	@Override
	public List<Employee> getAllEmployee(EntityManager em) {
		List<Employee> users = null;
		
		try{
			Query query = em.createQuery("From Employee");
			users = query.getResultList();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return users;
	}



}
