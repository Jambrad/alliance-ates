package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.ParticipantDao;
import ph.com.alliance.entity.Participation;
import ph.com.alliance.entity.Training;

@Repository("participantDao")
public class ParticipantDaoImpl implements ParticipantDao{

	@Override
	public boolean addParticipant(EntityManager em, Participation p) {
		boolean success = true;
		
		try{
			em.persist(p);
		}catch(EntityExistsException ee){
			ee.getMessage();
			success = false;
		}catch(IllegalArgumentException iae){
			iae.getMessage();
			success = false;
		}catch(TransactionRequiredException trxe){
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}
	
	@Override
	public boolean addParticipants(EntityManager em, List<Participation> p) {
		boolean success = true;
		
		try{
			for(Participation pp : p){
				em.persist(pp);
			}
		}catch(EntityExistsException ee){
			ee.getMessage();
			success = false;
		}catch(IllegalArgumentException iae){
			iae.getMessage();
			success = false;
		}catch(TransactionRequiredException trxe){
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}

	@Override
	public boolean updateParticipant(EntityManager em, Participation p) {
		boolean success = true;
		
		try{
			em.merge(p);
		}catch (IllegalArgumentException iae) {
			iae.getMessage();
			success = false;
		} catch (TransactionRequiredException trxe) {
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}

	@Override
	public boolean deleteParticipant(EntityManager em, Participation p) {
		boolean success = true;
		
		Participation pp = null;
			
		try{
			pp = em.find(Participation.class, p.getParticipantID());
			em.remove(pp);
		}catch (IllegalArgumentException iae) {
			success = false;
			iae.getMessage();
		} catch (TransactionRequiredException trxe) {
			success = false;
			trxe.getMessage();
		}
		
		
		return success;
	}

	@Override
	public boolean deleteParticipants(EntityManager em, List<Participation> p) {
		boolean success = true;
		
		Participation pp = null;
			
		try{
			
			for(Participation part : p){
			
			pp = em.find(Participation.class, part.getParticipantID());
			em.remove(pp);

			}
			
		}catch (IllegalArgumentException iae) {
			success = false;
			iae.getMessage();
		} catch (TransactionRequiredException trxe) {
			success = false;
			trxe.getMessage();
		}
		
		
		return success;
	}

	
	@Override
	public List<Participation> getParticipants(EntityManager em, Training t) {
		List<Participation> result = null;
		
		try{
			Query query = em.createQuery("From Participation WHERE trainingID = :id");
			query.setParameter("id", t.getTrainingID());
			
			result = query.getResultList();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return result;
	}

	@Override
	public Participation getParticipant(EntityManager em, String employeeID, int trainingID) {
		Participation result = null;
		
		try{
			Query query = em.createQuery("From Participation WHERE trainingID = :tid AND employeeID = :eid");
			query.setParameter("tid", trainingID);
			query.setParameter("eid", employeeID);
			
			result = (Participation) query.getSingleResult();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return result;
	}
	
}
