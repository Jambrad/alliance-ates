package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.FormDao;
import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.FormType;
import ph.com.alliance.entity.Training;

@Repository("formDao")
public class FormDaoImpl implements FormDao{

	@Override
	public boolean createForm(EntityManager em, Form f) {
		boolean success = true;
		
		try{
			em.persist(f);
		}catch(EntityExistsException ee){
			ee.getMessage();
			success = false;
		}catch(IllegalArgumentException iae){
			iae.getMessage();
			success = false;
		}catch(TransactionRequiredException trxe){
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}

	@Override
	public boolean updateForm(EntityManager em, Form f) {
		boolean success = true;
		
		try{
			em.merge(f);
		}catch (IllegalArgumentException iae) {
			iae.getMessage();
			success = false;
		} catch (TransactionRequiredException trxe) {
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}

	@Override
	public boolean deleteForm(EntityManager em, Form f) {
		boolean success = true;
		
		Form form = null;
			
		try{
			form = em.find(Form.class, f.getFormID());
			em.remove(form);
		}catch (IllegalArgumentException iae) {
			success = false;
			iae.getMessage();
		} catch (TransactionRequiredException trxe) {
			success = false;
			trxe.getMessage();
		}
		
		
		return success;
	}

	
	@Override
	public boolean createForms(EntityManager em, List<Form> f) {
		boolean success = true;
		
		try{
			for(Form ff : f){
				em.persist(ff);
			}
		}catch(EntityExistsException ee){
			ee.getMessage();
			success = false;
		}catch(IllegalArgumentException iae){
			iae.getMessage();
			success = false;
		}catch(TransactionRequiredException trxe){
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}
	


	@Override
	public List<Form> getFormbyTraining(EntityManager em, Training t) {
		List<Form> form = null;
		
		try{
			Query query = em.createQuery("From Form Where trainingID = :id");
			query.setParameter("id", t.getTrainingID());

	
			form = query.getResultList();
			
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return form;
	}

	@Override
	public Form getFormByID(EntityManager em, int id) {
		Form form = null;
		
		try{
	
			Query query = em.createQuery("From Form Where formID = :id");
			query.setParameter("id", id);
	
			form = (Form) query.getSingleResult();
			
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return form;
	}

	@Override
	public Form getForm(EntityManager em, int trainingID, FormType ft) {
		Form form = null;
		
		try{
	
			Query query = em.createQuery("From Form Where trainingID = :id AND formType = :ft");
			query.setParameter("id", trainingID);
			query.setParameter("ft", ft);
	
			form = (Form) query.getSingleResult();
			
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return form;
	}



}
