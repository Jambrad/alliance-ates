package ph.com.alliance.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.AttendanceDao;
import ph.com.alliance.entity.Attendance;
import ph.com.alliance.entity.Participation;


@Repository("attendanceDao")
public class AttendanceDaoImpl implements AttendanceDao {

	@Override
	public boolean createAttendance(EntityManager em, Attendance a) {
		boolean success = true;
		
		try{
			em.persist(a);
		}catch(EntityExistsException ee){
			ee.getMessage();
			success = false;
		}catch(IllegalArgumentException iae){
			iae.getMessage();
			success = false;
		}catch(TransactionRequiredException trxe){
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}

	@Override
	public boolean updateAttendance(EntityManager em, Attendance a) {
		boolean success = true;
		
		try{
			em.merge(a);
		}catch (IllegalArgumentException iae) {
			iae.getMessage();
			success = false;
		} catch (TransactionRequiredException trxe) {
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}

	@Override
	public boolean deleteAttendance(EntityManager em, Attendance a) {
		boolean success = true;
		
		Attendance attendance = null;
			
		try{
			attendance = em.find(Attendance.class, a.getId());
			em.remove(attendance);
		}catch (IllegalArgumentException iae) {
			success = false;
			iae.getMessage();
		} catch (TransactionRequiredException trxe) {
			success = false;
			trxe.getMessage();
		}
		
		
		return success;
	}

	@Override
	public boolean createAttendanceBatch(EntityManager em, List<Attendance> a) {
		boolean success = true;
		
		try{
			for(Attendance at : a){
				em.persist(at);
			}
			
		}catch(EntityExistsException ee){
			success = false;
			ee.getMessage();
		}catch(IllegalArgumentException iae){
			success = false;
			iae.getMessage();
		}catch(TransactionRequiredException trxe){
			success = false;
			trxe.getMessage();
		}
		
		return success;
	}

	
	@Override
	public boolean deleteAttendanceBatch(EntityManager em, List<Attendance> a) {
		boolean success = true;
		
		Attendance attendance = null;
			
		try{
			for(Attendance at : a){
				attendance = em.find(Attendance.class, at.getId());
				em.remove(attendance);
			}
			
		}catch (IllegalArgumentException iae) {
			success = false;
			iae.getMessage();
		} catch (TransactionRequiredException trxe) {
			success = false;
			trxe.getMessage();
		}
		
		
		return success;
	}

	@Override
	public boolean updateAttendanceBatch(EntityManager em, List<Attendance> a) {
		boolean success = true;
		
		try{
			for(Attendance att : a){
				em.merge(att);
			}
		}catch (IllegalArgumentException iae) {
			success = false;
			iae.getMessage();
		} catch (TransactionRequiredException trxe) {
			success = false;
			trxe.getMessage();
		}
		
		return success;
	}
	
	@Override
	public List<Attendance> getAllAttendance(EntityManager em) {
		List<Attendance> a = null;
		
		try{
			Query query = em.createQuery("From Attendance");
			a = query.getResultList();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		return a;
	}

	@Override
	public List<Attendance> getAttendance(EntityManager em, List<Participation> p,
			Date date) {
		
		List<Attendance> attendance = null;
		
		try{
			Query query = em.createQuery("From Attendance WHERE participationID IN :id AND date = :date");
			query.setParameter("id", p);
			query.setParameter("date", date);
			
			attendance = query.getResultList();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		
		return attendance;
	}

	@Override
	public List<Attendance> getAttendanceByTrainingID(EntityManager em,
			List<Participation> p) {
		List<Attendance> attendance = null;
		
		try{
			Query query = em.createQuery("From Attendance WHERE participationID IN :id");
			query.setParameter("id", p);
			
			attendance = query.getResultList();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		
		return attendance;
	}

	@Override
	public List<Date> getDates(EntityManager em, List<Participation> p) {
		List<Date> dates = null;
		
		try{
			Query query = em.createQuery("Select Distinct date From Attendance Where participationID IN :id");
			query.setParameter("id", p);
			
			dates = query.getResultList();
		}catch(TransactionRequiredException tre){
			tre.getMessage();
		}catch(QueryTimeoutException qte){
			qte.getMessage();
		}catch(NoResultException nre){
			nre.getMessage();
		}
		
		return dates;
	}


}
