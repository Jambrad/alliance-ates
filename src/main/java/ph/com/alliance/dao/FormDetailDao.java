package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.FormDetail;

public interface FormDetailDao {
	
	/**
	 * A function that retrieves form details of a specific form
	 * 
	 * @param em
	 * @param f
	 * @return FormDetail Entities
	 */
	public List<FormDetail> getFormDetailByFormID(EntityManager em, Form f);
	
	/**
	 * A function that retrieves all questions for a specific form
	 * 
	 * @param em
	 * @param type
	 * @return FormDetail Entities
	 */
	public List<FormDetail> getQuestions(EntityManager em, int type);
	
	/**
	 * A function that retrieves all categories for a specific form
	 * 
	 * @param em
	 * @param id
	 * @param type
	 * @return String
	 */
	public List<String> getCategories(EntityManager em, int id, int type);
	
	/**
	 * A function that retrieves all checkboxes
	 * 
	 * @param em
	 * @return FormDetail Entity
	 */
	public List<FormDetail> getChecks(EntityManager em);
	
}
