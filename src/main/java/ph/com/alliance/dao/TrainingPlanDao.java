package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.TrainingPlan;

public interface TrainingPlanDao {
	
		/**
		 * A function that creates a training plan
		 * 
		 * @param em
		 * @param p
		 * @return true, if training plan is created successfully
		 */
		public boolean createTrainingPlan(EntityManager em, TrainingPlan p);
		
		/**
		 * A function that updates a training plan
		 * 
		 * @param em
		 * @param p
		 * @return true, if training plan is updated successfully
		 */
		public boolean updateTrainingPlan(EntityManager em, TrainingPlan p);
		
		/**
		 * A function that deletes a training plan
		 * 
		 * @param em
		 * @param year
		 * @return true, if training plan is deletes successfully
		 */
		public boolean deleteTrainingPlan(EntityManager em, int year);
		
		/**
		 * A function that retrieves all training plans
		 * 
		 * @param em
		 * @return TrainingPlan Entities
		 */
		public List<TrainingPlan> getAllTrainingPlan(EntityManager em);
		
		/**
		 * A function that retrieves a training plan using year
		 * 
		 * @param em
		 * @param year
		 * @return TrainingPlan Entity
		 */
		public TrainingPlan getByYear(EntityManager em, int year);
		
		/**
		 * A function that retrieves a training plan by ID
		 * 
		 * @param em
		 * @param id
		 * @return TrainingPlan Entity
		 */
		public TrainingPlan getByID(EntityManager em, int id);
		
		/**
		 * A function that retrieves the latest year with a training plan
		 * 
		 * @param em
		 * @return Integer year
		 */
		public int getMaxYear(EntityManager em);
}
