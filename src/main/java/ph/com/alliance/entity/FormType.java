package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the form_type database table.
 * 
 */
@Entity
@Table(name="form_type")
@NamedQuery(name="FormType.findAll", query="SELECT f FROM FormType f")
public class FormType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int typeID;

	private String formType;

	//bi-directional many-to-one association to Form
	@OneToMany(mappedBy="formTypeBean")
	private List<Form> forms;

	//bi-directional many-to-one association to FormDetail
	@OneToMany(mappedBy="formTypeBean")
	private List<FormDetail> formDetails;

	public FormType() {
	}

	public int getTypeID() {
		return this.typeID;
	}

	public void setTypeID(int typeID) {
		this.typeID = typeID;
	}

	public String getFormType() {
		return this.formType;
	}

	public void setFormType(String formType) {
		this.formType = formType;
	}

	public List<Form> getForms() {
		return this.forms;
	}

	public void setForms(List<Form> forms) {
		this.forms = forms;
	}

	public Form addForm(Form form) {
		getForms().add(form);
		form.setFormTypeBean(this);

		return form;
	}

	public Form removeForm(Form form) {
		getForms().remove(form);
		form.setFormTypeBean(null);

		return form;
	}

	public List<FormDetail> getFormDetails() {
		return this.formDetails;
	}

	public void setFormDetails(List<FormDetail> formDetails) {
		this.formDetails = formDetails;
	}

	public FormDetail addFormDetail(FormDetail formDetail) {
		getFormDetails().add(formDetail);
		formDetail.setFormTypeBean(this);

		return formDetail;
	}

	public FormDetail removeFormDetail(FormDetail formDetail) {
		getFormDetails().remove(formDetail);
		formDetail.setFormTypeBean(null);

		return formDetail;
	}

	@Override
	public String toString() {
		return "FormType [formType=" + formType + "]";
	}
	
	

}