package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the attendance database table.
 * 
 */
@Entity
@NamedQuery(name="Attendance.findAll", query="SELECT a FROM Attendance a")
public class Attendance implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AttendancePK id;

	private String amAttendance;

	private String pmAttendance;

	//bi-directional many-to-one association to Participation
	@ManyToOne
	@JoinColumn(name="participationID", insertable=false, updatable=false)
	private Participation participation;

	public Attendance() {
	}

	public Attendance(AttendancePK id) {
		this.id = id;
	}


	public AttendancePK getId() {
		return this.id;
	}

	public void setId(AttendancePK id) {
		this.id = id;
	}

	public String getAmAttendance() {
		return this.amAttendance;
	}

	public void setAmAttendance(String amAttendance) {
		this.amAttendance = amAttendance;
	}

	public String getPmAttendance() {
		return this.pmAttendance;
	}

	public void setPmAttendance(String pmAttendance) {
		this.pmAttendance = pmAttendance;
	}

	public Participation getParticipation() {
		return this.participation;
	}

	public void setParticipation(Participation participation) {
		this.participation = participation;
	}

}