package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the pending database table.
 * 
 */
@Entity
@NamedQuery(name="Pending.findAll", query="SELECT p FROM Pending p")
public class Pending implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int pendingID;

	private byte isDone;

	//bi-directional many-to-one association to Employee
	@ManyToOne
	@JoinColumn(name="employeeID")
	private Employee employee;

	//bi-directional many-to-one association to Form
	@ManyToOne
	@JoinColumn(name="formID")
	private Form form;

	//bi-directional many-to-one association to Participation
	@ManyToOne
	@JoinColumn(name="participantID")
	private Participation participation;

	public Pending() {
	}

	
	public Pending(Employee employee, Form form) {
		this.employee = employee;
		this.form = form;
	}


	public int getPendingID() {
		return this.pendingID;
	}

	public void setPendingID(int pendingID) {
		this.pendingID = pendingID;
	}

	public byte getIsDone() {
		return this.isDone;
	}

	public void setIsDone(byte isDone) {
		this.isDone = isDone;
	}

	public Employee getEmployee() {
		return this.employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Form getForm() {
		return this.form;
	}

	public void setForm(Form form) {
		this.form = form;
	}

	public Participation getParticipation() {
		return this.participation;
	}

	public void setParticipation(Participation participation) {
		this.participation = participation;
	}

}