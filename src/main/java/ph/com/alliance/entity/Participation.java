package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the participation database table.
 * 
 */
@Entity
@NamedQuery(name="Participation.findAll", query="SELECT p FROM Participation p")
public class Participation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int participantID;

	//bi-directional many-to-one association to Attendance
	@OneToMany(mappedBy="participation")
	private List<Attendance> attendances;

	//bi-directional many-to-one association to Employee
	@ManyToOne
	@JoinColumn(name="employeeID")
	private Employee employee;

	//bi-directional many-to-one association to Training
	@ManyToOne
	@JoinColumn(name="trainingID")
	private Training training;

	//bi-directional many-to-one association to Pending
	@OneToMany(mappedBy="participation")
	private List<Pending> pendings;

	public Participation() {
	}

	
	public Participation(Training training, Employee employee) {
		this.training = training;
		this.employee = employee;
	}


	public int getParticipantID() {
		return this.participantID;
	}

	public void setParticipantID(int participantID) {
		this.participantID = participantID;
	}

	public List<Attendance> getAttendances() {
		return this.attendances;
	}

	public void setAttendances(List<Attendance> attendances) {
		this.attendances = attendances;
	}

	public Attendance addAttendance(Attendance attendance) {
		getAttendances().add(attendance);
		attendance.setParticipation(this);

		return attendance;
	}

	public Attendance removeAttendance(Attendance attendance) {
		getAttendances().remove(attendance);
		attendance.setParticipation(null);

		return attendance;
	}

	public Employee getEmployee() {
		return this.employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Training getTraining() {
		return this.training;
	}

	public void setTraining(Training training) {
		this.training = training;
	}

	public List<Pending> getPendings() {
		return this.pendings;
	}

	public void setPendings(List<Pending> pendings) {
		this.pendings = pendings;
	}

	public Pending addPending(Pending pending) {
		getPendings().add(pending);
		pending.setParticipation(this);

		return pending;
	}

	public Pending removePending(Pending pending) {
		getPendings().remove(pending);
		pending.setParticipation(null);

		return pending;
	}

}