package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the employee database table.
 * 
 */
@Entity
@NamedQuery(name="Employee.findAll", query="SELECT e FROM Employee e")
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String employeeID;

	private String address;

	private String businessGroup;

	private String contactNumber;

	private String email;

	private String employer;

	private String firstName;

	private String gender;

	private String jobTitle;

	private String lastName;

	private byte loggedIn;

	private String middleName;

	private String password;

	//bi-directional many-to-one association to Admin
	@OneToMany(mappedBy="employee")
	private List<Admin> admins;

	//uni-directional many-to-one association to Employee
	@ManyToOne
	@JoinColumn(name="buHead")
	private Employee employee1;

	//uni-directional many-to-one association to Employee
	@ManyToOne
	@JoinColumn(name="supervisor")
	private Employee employee2;

	//bi-directional many-to-one association to Participation
	@OneToMany(mappedBy="employee")
	private List<Participation> participations;

	//bi-directional many-to-one association to Pending
	@OneToMany(mappedBy="employee")
	private List<Pending> pendings;

	//bi-directional many-to-one association to Training
	@OneToMany(mappedBy="employee")
	private List<Training> trainings;

	public Employee() {
	}

	public String getEmployeeID() {
		return this.employeeID;
	}

	public void setEmployeeID(String employeeID) {
		this.employeeID = employeeID;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBusinessGroup() {
		return this.businessGroup;
	}

	public void setBusinessGroup(String businessGroup) {
		this.businessGroup = businessGroup;
	}

	public String getContactNumber() {
		return this.contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmployer() {
		return this.employer;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getJobTitle() {
		return this.jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public byte getLoggedIn() {
		return this.loggedIn;
	}

	public void setLoggedIn(byte loggedIn) {
		this.loggedIn = loggedIn;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Admin> getAdmins() {
		return this.admins;
	}

	public void setAdmins(List<Admin> admins) {
		this.admins = admins;
	}

	public Admin addAdmin(Admin admin) {
		getAdmins().add(admin);
		admin.setEmployee(this);

		return admin;
	}

	public Admin removeAdmin(Admin admin) {
		getAdmins().remove(admin);
		admin.setEmployee(null);

		return admin;
	}

	public Employee getEmployee1() {
		return this.employee1;
	}

	public void setEmployee1(Employee employee1) {
		this.employee1 = employee1;
	}

	public Employee getEmployee2() {
		return this.employee2;
	}

	public void setEmployee2(Employee employee2) {
		this.employee2 = employee2;
	}

	public List<Participation> getParticipations() {
		return this.participations;
	}

	public void setParticipations(List<Participation> participations) {
		this.participations = participations;
	}

	public Participation addParticipation(Participation participation) {
		getParticipations().add(participation);
		participation.setEmployee(this);

		return participation;
	}

	public Participation removeParticipation(Participation participation) {
		getParticipations().remove(participation);
		participation.setEmployee(null);

		return participation;
	}

	public List<Pending> getPendings() {
		return this.pendings;
	}

	public void setPendings(List<Pending> pendings) {
		this.pendings = pendings;
	}

	public Pending addPending(Pending pending) {
		getPendings().add(pending);
		pending.setEmployee(this);

		return pending;
	}

	public Pending removePending(Pending pending) {
		getPendings().remove(pending);
		pending.setEmployee(null);

		return pending;
	}

	public List<Training> getTrainings() {
		return this.trainings;
	}

	public void setTrainings(List<Training> trainings) {
		this.trainings = trainings;
	}

	public Training addTraining(Training training) {
		getTrainings().add(training);
		training.setEmployee(this);

		return training;
	}

	public Training removeTraining(Training training) {
		getTrainings().remove(training);
		training.setEmployee(null);

		return training;
	}

}