package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the form_result database table.
 * 
 */
@Entity
@Table(name="form_result")
@NamedQuery(name="FormResult.findAll", query="SELECT f FROM FormResult f")
public class FormResult implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int resultID;

	private String answerString;

	//bi-directional many-to-one association to Employee
	@ManyToOne
	@JoinColumn(name="employeeID")
	private Employee employee;

	//bi-directional many-to-one association to Form
	@ManyToOne
	@JoinColumn(name="formID")
	private Form form;

	//bi-directional many-to-one association to FormDetail
	@ManyToOne
	@JoinColumn(name="detailID")
	private FormDetail formDetail;

	public FormResult() {
	}
	
	

	public FormResult(Employee employee, Form form, FormDetail formDetail) {
		this.employee = employee;
		this.form = form;
		this.formDetail = formDetail;
	}



	public int getResultID() {
		return this.resultID;
	}

	public void setResultID(int resultID) {
		this.resultID = resultID;
	}

	public String getAnswerString() {
		return this.answerString;
	}

	public void setAnswerString(String answerString) {
		this.answerString = answerString;
	}

	public Employee getEmployee() {
		return this.employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Form getForm() {
		return this.form;
	}

	public void setForm(Form form) {
		this.form = form;
	}

	public FormDetail getFormDetail() {
		return this.formDetail;
	}

	public void setFormDetail(FormDetail formDetail) {
		this.formDetail = formDetail;
	}



	@Override
	public String toString() {
		return  formDetail+ "FormResult [answerString=" + answerString;
	}

	
}