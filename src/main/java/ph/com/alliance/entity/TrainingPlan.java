package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the training_plan database table.
 * 
 */
@Entity
@Table(name="training_plan")
@NamedQuery(name="TrainingPlan.findAll", query="SELECT t FROM TrainingPlan t")
public class TrainingPlan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int trainingPlanID;

	private int year;

	//bi-directional many-to-one association to Training
	@OneToMany(mappedBy="trainingPlan")
	private List<Training> trainings;

	public TrainingPlan() {
	}

	public int getTrainingPlanID() {
		return this.trainingPlanID;
	}

	public void setTrainingPlanID(int trainingPlanID) {
		this.trainingPlanID = trainingPlanID;
	}

	public int getYear() {
		return this.year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public List<Training> getTrainings() {
		return this.trainings;
	}

	public void setTrainings(List<Training> trainings) {
		this.trainings = trainings;
	}

	public Training addTraining(Training training) {
		getTrainings().add(training);
		training.setTrainingPlan(this);

		return training;
	}

	public Training removeTraining(Training training) {
		getTrainings().remove(training);
		training.setTrainingPlan(null);

		return training;
	}

}