package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the form database table.
 * 
 */
@Entity
@NamedQuery(name="Form.findAll", query="SELECT f FROM Form f")
public class Form implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int formID;

	private String formFor;

	//bi-directional many-to-one association to FormType
	@ManyToOne
	@JoinColumn(name="formType")
	private FormType formTypeBean;

	//bi-directional many-to-one association to Training
	@ManyToOne
	@JoinColumn(name="trainingID")
	private Training training;

	//bi-directional many-to-one association to FormResult
	@OneToMany(mappedBy="form")
	private List<FormResult> formResults;

	//bi-directional many-to-one association to Pending
	@OneToMany(mappedBy="form")
	private List<Pending> pendings;

	public Form() {
	}
	
	

	public Form(FormType formTypeBean, Training training) {
		this.formTypeBean = formTypeBean;
		this.training = training;
	}



	public int getFormID() {
		return this.formID;
	}

	public void setFormID(int formID) {
		this.formID = formID;
	}

	public String getFormFor() {
		return this.formFor;
	}

	public void setFormFor(String formFor) {
		this.formFor = formFor;
	}

	public FormType getFormTypeBean() {
		return this.formTypeBean;
	}

	public void setFormTypeBean(FormType formTypeBean) {
		this.formTypeBean = formTypeBean;
	}

	public Training getTraining() {
		return this.training;
	}

	public void setTraining(Training training) {
		this.training = training;
	}

	public List<FormResult> getFormResults() {
		return this.formResults;
	}

	public void setFormResults(List<FormResult> formResults) {
		this.formResults = formResults;
	}

	public FormResult addFormResult(FormResult formResult) {
		getFormResults().add(formResult);
		formResult.setForm(this);

		return formResult;
	}

	public FormResult removeFormResult(FormResult formResult) {
		getFormResults().remove(formResult);
		formResult.setForm(null);

		return formResult;
	}

	public List<Pending> getPendings() {
		return this.pendings;
	}

	public void setPendings(List<Pending> pendings) {
		this.pendings = pendings;
	}

	public Pending addPending(Pending pending) {
		getPendings().add(pending);
		pending.setForm(this);

		return pending;
	}

	public Pending removePending(Pending pending) {
		getPendings().remove(pending);
		pending.setForm(null);

		return pending;
	}

	@Override
	public String toString() {
		return "Form [formID=" + formID + ", formTypeBean=" + formTypeBean
				+ ", formFor=" + formFor + ", training=" + training + "]";
	}

	
}