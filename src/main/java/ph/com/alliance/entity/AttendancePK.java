package ph.com.alliance.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * The primary key class for the attendance database table.
 * 
 */
@Embeddable
public class AttendancePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private int participationID;

	@Temporal(TemporalType.DATE)
	private java.util.Date date;

	public AttendancePK() {
	}
	
	
	public AttendancePK(int participationID, Date date) {
		this.participationID = participationID;
		this.date = date;
	}


	public int getParticipationID() {
		return this.participationID;
	}
	public void setParticipationID(int participationID) {
		this.participationID = participationID;
	}
	public java.util.Date getDate() {
		return this.date;
	}
	public void setDate(java.util.Date date) {
		this.date = date;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AttendancePK)) {
			return false;
		}
		AttendancePK castOther = (AttendancePK)other;
		return 
			(this.participationID == castOther.participationID)
			&& this.date.equals(castOther.date);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.participationID;
		hash = hash * prime + this.date.hashCode();
		
		return hash;
	}
}