package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the form_details database table.
 * 
 */
@Entity
@Table(name="form_details")
@NamedQuery(name="FormDetail.findAll", query="SELECT f FROM FormDetail f")
public class FormDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int detailID;

	private String category;

	private int questionNumber;

	private String questionString;

	private String questionType;

	//bi-directional many-to-one association to FormType
	@ManyToOne
	@JoinColumn(name="formType")
	private FormType formTypeBean;

	//bi-directional many-to-one association to FormResult
	@OneToMany(mappedBy="formDetail")
	private List<FormResult> formResults;

	public FormDetail() {
	}

	public int getDetailID() {
		return this.detailID;
	}

	public void setDetailID(int detailID) {
		this.detailID = detailID;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getQuestionNumber() {
		return this.questionNumber;
	}

	public void setQuestionNumber(int questionNumber) {
		this.questionNumber = questionNumber;
	}

	public String getQuestionString() {
		return this.questionString;
	}

	public void setQuestionString(String questionString) {
		this.questionString = questionString;
	}

	public String getQuestionType() {
		return this.questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public FormType getFormTypeBean() {
		return this.formTypeBean;
	}

	public void setFormTypeBean(FormType formTypeBean) {
		this.formTypeBean = formTypeBean;
	}

	public List<FormResult> getFormResults() {
		return this.formResults;
	}

	public void setFormResults(List<FormResult> formResults) {
		this.formResults = formResults;
	}

	public FormResult addFormResult(FormResult formResult) {
		getFormResults().add(formResult);
		formResult.setFormDetail(this);

		return formResult;
	}

	public FormResult removeFormResult(FormResult formResult) {
		getFormResults().remove(formResult);
		formResult.setFormDetail(null);

		return formResult;
	}

	@Override
	public String toString() {
		return "FormDetail [category=" + category + ", questionString="
				+ questionString + ", questionType=" + questionType + "]";
	}

	
}