package ph.com.alliance.controller.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ph.com.alliance.entity.Attendance;
import ph.com.alliance.entity.AttendancePK;
import ph.com.alliance.entity.Employee;
import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.FormDetail;
import ph.com.alliance.entity.FormType;
import ph.com.alliance.entity.Participation;
/*
 * Import TrainingEntity and TrainingModel here
 */
import ph.com.alliance.entity.Training;
import ph.com.alliance.entity.TrainingPlan;
import ph.com.alliance.model.DateModel;
import ph.com.alliance.model.EmployeeModel;
import ph.com.alliance.model.FormReportModel;
import ph.com.alliance.model.ParticipationModel;
import ph.com.alliance.model.TrainingModel;
import ph.com.alliance.model.TrainingModel_Calendar;
import ph.com.alliance.model.TrainingPlanModel;
import ph.com.alliance.service.AccountDBService;
import ph.com.alliance.service.AttendanceService;
import ph.com.alliance.service.FormService;
import ph.com.alliance.service.TrainingService;

/**
 * Controller class used to handle api requests. All requests that falls through
 * /api/* servlet mapping goes through here.
 * 
 */
@Controller
@RequestMapping("/AllianceATES")
public class ModuleAPIController {

	@Autowired
	AttendanceService attendanceService;

	@Autowired
	TrainingService trainingService;

	@Autowired
	TrainingService dbSvc;

	@Autowired
	AccountDBService Accounts;

	@Autowired
	FormService formService;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	private EmployeeModel convertToModel_Employees(Employee u) {
		EmployeeModel userModel = null;

		if (u != null) {
			userModel = dozerBeanMapper.map(u, EmployeeModel.class);
		}

		return userModel;
	}

	private ParticipationModel convertToModel_Participation(Participation u) {
		ParticipationModel participationModel = null;

		if (u != null) {
			participationModel = dozerBeanMapper.map(u, ParticipationModel.class);
		}

		return participationModel;
	}

	private TrainingPlanModel convertToModel_TrainingPlan(TrainingPlan tp) {
		TrainingPlanModel tpModel = null;

		if (tp != null) {
			tpModel = dozerBeanMapper.map(tp, TrainingPlanModel.class);
		}

		return tpModel;
	}

	/**
	 * This is a sample object mapper. Model to entity mapping can be explicitly
	 * done via setters, or see convertToModel function for mapping using
	 * constructor
	 * 
	 * @param pUserModel
	 * @return
	 */
	private Training convertToEntity(TrainingModel pUserModel) {
		Training u = null;

		if (pUserModel != null) {
			u = dozerBeanMapper.map(pUserModel, Training.class);
		}

		return u;
	}

	/*
	 * Returns all trainings with complete details Replace User with Training
	 * Entity and UserModel with TrainingModel
	 */
	@RequestMapping(value = "/api/training/viewAll", method = RequestMethod.GET)
	@ResponseBody

	public List<TrainingModel> viewTrainingPlan(@RequestParam(value="q", required = false) String q) {
		List<Training> TrainingList = (q==null)?dbSvc.getAllTraining():dbSvc.getByTitle(q);
		List<TrainingModel> ModelTrainingList = new ArrayList<TrainingModel>();
		TrainingModel TM = new TrainingModel();

		for (Training u : TrainingList) {

			System.out.println(" Id no: " + u.getTrainingID());
			TM.setTrainingID(u.getTrainingID());
			TM.setTitle(u.getTitle());
			TM.setStartDate(u.getStartDate());
			TM.setEndDate(u.getEndDate());
			TM.setBusinessUnit(u.getBusinessUnit());
			TM.setObjective(u.getObjective());
			TM.setType(u.getType());
			TM.setNoOfPax(u.getNoOfPax());
			TM.setStartTime(u.getStartTime());
			TM.setEndTime(u.getEndTime());
			ModelTrainingList.add(TM);
			TM = new TrainingModel();

		}

		return ModelTrainingList;
	}

	/*
	 * Returns all training plans with details for calendar Replace User with
	 * Training Entity and UserModel with TrainingModel modified for Calendar
	 * view
	 */

	@RequestMapping(value = "/api/training/viewAllCalendar", method = RequestMethod.GET)
	@ResponseBody

	public List<TrainingModel_Calendar> viewTrainingPlan_Cal() {
		System.out.println("view all calendar training");
		List<Training> TrainingList = dbSvc.getAllTraining();
		List<TrainingModel_Calendar> ModelTrainingList = new ArrayList<TrainingModel_Calendar>();
		TrainingModel_Calendar TM_Cal = new TrainingModel_Calendar();

		for (Training u : TrainingList) {

			System.out.println(" Id no: " + u.getTrainingID());
			TM_Cal.setId(u.getTrainingID());
			TM_Cal.setTitle(u.getTitle());
			TM_Cal.setStart(u.getStartDate());
			TM_Cal.setEnd(u.getEndDate());
			ModelTrainingList.add(TM_Cal);
			TM_Cal = new TrainingModel_Calendar();

		}

		return ModelTrainingList;
	}

	@RequestMapping(value = "/api/training/viewCalendar/{id}", method = RequestMethod.GET)
	@ResponseBody

	public List<TrainingModel_Calendar> viewTrainingsOfPlan(@PathVariable(value = "id") int id) {
		System.out.println("view all calendar training by trainingplan");
		// TrainingPlan tp = dbSvc.getByID(id);

		// List<Training> TrainingList = tp.getTrainings();
		List<Training> TrainingList = trainingService.getByTrainingPlanID(id);
		List<TrainingModel_Calendar> ModelTrainingList = new ArrayList<TrainingModel_Calendar>();
		TrainingModel_Calendar TM_Cal = null;

		for (Training u : TrainingList) {

			TM_Cal = new TrainingModel_Calendar();
			System.out.println(" Id no: " + u.getTrainingID());
			TM_Cal.setId(u.getTrainingID());
			TM_Cal.setTitle(u.getTitle());
			TM_Cal.setStart(u.getStartDate());
			TM_Cal.setEnd(u.getEndDate());
			ModelTrainingList.add(TM_Cal);

		}

		return ModelTrainingList;
	}

	@RequestMapping(value = "/api/training/{id}/participants/", method = RequestMethod.GET)
	@ResponseBody
	public List<ParticipationModel> viewParticipants(@PathVariable("id") int trainingID) {
		List<ParticipationModel> listParticipant = new ArrayList<ParticipationModel>();
		List<Participation> participation = attendanceService.getParticipants(dbSvc.getByTrainingID(trainingID));
		for (Participation p : participation) {
			listParticipant.add(convertToModel_Participation(p));
		}

		return listParticipant;
	}

	/*
	 * Searches specific Training using id parameter
	 * 
	 */

	@RequestMapping(value = "/api/training/view/{id}", method = RequestMethod.GET)
	@ResponseBody
	public TrainingModel viewTraining(@PathVariable("id") int id) {
		System.out.println("view one training");
		Training u = new Training();
		TrainingModel TM = new TrainingModel();
		u = dbSvc.getByTrainingID(id);
		TM.setTrainingID(u.getTrainingID());
		TM.setTitle(u.getTitle());
		TM.setStartDate(u.getStartDate());
		TM.setType(u.getType());
		TM.setEndDate(u.getEndDate());
		TM.setBusinessUnit(u.getBusinessUnit());
		TM.setObjective(u.getObjective());
		TM.setNoOfPax(u.getNoOfPax());
		TM.setStartTime(u.getStartTime());
		TM.setEndTime(u.getEndTime());

		return TM;
	}

	

	
	/*
	 * Searches specific Training using id parameter
	 * 
	 */

	@RequestMapping(value = "/api/employee/view/{id}", method = RequestMethod.GET)
	@ResponseBody
	public EmployeeModel viewEmployee(@PathVariable("id") String id) {
		System.out.println("view one training");
		Employee emp = new Employee();
		emp = Accounts.getUserByID(id);
		return convertToModel_Employees(emp);
	}

	@RequestMapping(value = "/api/employee/viewAll", method = RequestMethod.GET)
	@ResponseBody
	public List<EmployeeModel> viewAllEmployee(@RequestParam(value="q", required = false) String q) {
		System.out.println("[INFO] Searching employee by name: " + q);
		
		List<Employee> empEntityList = (q == null)?Accounts.getAll():Accounts.getUserByName(q);
		List<EmployeeModel> empModelList = new ArrayList<EmployeeModel>();
		EmployeeModel empModel = null;
		for (Employee e : empEntityList) {
			empModel = convertToModel_Employees(e);
			empModelList.add(empModel);
		}

		return empModelList;
	}

	@RequestMapping(value = "/api/trainingPlan/viewAll", method = RequestMethod.GET)
	@ResponseBody
	public List<TrainingPlanModel> viewAllTrainingPlan() {
		List<TrainingPlan> tpEntityList = dbSvc.getAllTrainingPlan();
		List<TrainingPlanModel> tpModelList = new ArrayList<TrainingPlanModel>();
		TrainingPlanModel tpModel = null;
		for (TrainingPlan tp : tpEntityList) {
			tpModel = convertToModel_TrainingPlan(tp);
			tpModelList.add(tpModel);
		}

		return tpModelList;
	}

	@RequestMapping(value = "/api/attendance/save", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Boolean> saveAttendance(@RequestParam(value = "participationID") int[] participationID,
			@RequestParam(value = "amAttendance") String[] amAttendance,
			@RequestParam(value = "pmAttendance") String[] pmAttendance, @RequestParam(value = "date") Date date) {
		System.out.println("Date: " + date.toString());
		boolean success = false;
		List<Attendance> attendanceList = new ArrayList<Attendance>();
		Attendance a = null;
		for (int i = 0; i < participationID.length; i++) {
			System.out.println(
					"EmployeeID: " + participationID[i] + "\nAM: " + amAttendance[i] + "\nPM: " + pmAttendance[i]);
			a = new Attendance(new AttendancePK(participationID[i], date));
			a.setAmAttendance(amAttendance[i]);
			a.setPmAttendance(pmAttendance[i]);
			attendanceList.add(a);
		}

		if (null != attendanceList) {
			success = attendanceService.updateAttendanceByBatch(attendanceList);
		}

		return Collections.singletonMap("success", success);
	}

	@RequestMapping(value = "/api/attendance/{trainingID}/getAvailableDates", method = RequestMethod.GET)
	@ResponseBody
	public List<DateModel> getAvailableAttendanceDates(@PathVariable(value = "trainingID") String trainingID)
			throws ParseException {
		// get training
		Training t = trainingService.getByTrainingID(Integer.parseInt(trainingID));
		// get the dates that has an attendance already
		List<Date> usedDates = attendanceService.getDates(t);
		// get start and end dates
		Date startDate = t.getStartDate();
		Date endDate = t.getEndDate();
		// get dates that are available (not including dates with attendance
		// already)
		List<DateModel> betweenDates = getDaysBetweenDates(startDate, endDate, usedDates);

		return betweenDates;
	}

	@RequestMapping(value = "/api/attendance/{trainingID}/create", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Boolean> createAttendanceDates(@PathVariable(value = "trainingID") String trainingID,
			@RequestParam(value = "date") Date date) throws ParseException {
		// get training
		Training t = trainingService.getByTrainingID(Integer.parseInt(trainingID));
		// gets the list of participants
		List<Participation> participants = (null != t) ? attendanceService.getParticipants(t) : null;
		System.out.println("Create attendance for " + trainingID + ": " + date);
		boolean status = false;
		if (null != participants) {
			status = attendanceService.createAttendanceByBatch(participants, date);
		}

		return Collections.singletonMap("success", status);
	}

	@RequestMapping(value = "/api/form/{formID}/report", method = RequestMethod.GET)
	@ResponseBody
	public List<FormReportModel> getReport(@PathVariable(value = "formID") int formID) {

		List<FormReportModel> ReportList = new ArrayList<FormReportModel>();
		FormReportModel template = null;
		Form f = formService.getFormID(formID);

		Map<FormDetail, int[]> test = formService.getReport(f);

		Set<FormDetail> keys = test.keySet();
		Iterator<FormDetail> i = keys.iterator();
		while (i.hasNext()) {
			template = new FormReportModel();
			FormDetail fd = i.next();
			int[] data = test.get(fd);

			template.setQuestionNumber(fd.getQuestionNumber());
			template.setQuestionString(fd.getQuestionString());
			template.setQuestionType(fd.getQuestionType());
			template.setData(data);

			switch (template.getQuestionType()) {

			case "Agree":

				template.setLabels(
						new String[] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" });
				break;

			case "Comprehension":

				template.setLabels(
						new String[] { "3 or more levels", "2 levels", "1 level", "No Improvement", "Regressed" });
				break;

			case "Useful":

				template.setLabels(new String[] { "Very Useful", "Useful", "Fair", "Not so useful", "Not useful" });
				break;

			case "Objectives":

				template.setLabels(new String[] { "All of the objectives were met", "Most of the objectives were met",
						"Very little of the objectives were met", "No Response" });
				break;

			case "Suitable":

				template.setLabels(new String[] { "Suitable", "Difficult", "Easy", "No Response" });
				break;

			case "Well":

				template.setLabels(
						new String[] { "Very Well", "Well", "Fair", "Little", "Very Little", "No Response" });
				break;
				
			case "Expectations":
				template.setLabels(
						new String[] {"Exceeds Expectation", "Significant Strength", "Meets Expectations", "Development Neeeded", "Needs Improvement"});
				break;
			case "Check":
				template.setLabels(
						new String[]{"The textbooks/manuals were too difficult",
								"The textbooks/manuals were inadequate",
								"The explanation of the instructor was insufficient",
								"The content of the lectures was too difficult",
								"The size of the class was too big",
								"I  did not have enough time to finish the exercise",
								"I did not have pre-knowledge of this subject",
								"I was absent during critical period of the subject",
								"My computer experience was insufficient",
								"Other"});
			}
			ReportList.add(template);
		}

		return ReportList;
	}

	@RequestMapping(value = "/api/training/{trainingID}/edit/objective", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Boolean> updateTrainingObjective(@PathVariable(value = "trainingID") String trainingID,
			@RequestParam(value = "objective") String objective) {
		boolean updateStatus = false;
		Training t = trainingService.getByTrainingID(Integer.parseInt(trainingID));
		t.setObjective(objective);

		updateStatus = trainingService.updateTraining(t);

		return Collections.singletonMap("success", updateStatus);

	}

	@RequestMapping(value="/api/training/{trainingID}/form/status", method= RequestMethod.GET)
	@ResponseBody
	public Map<String, Integer> getFormStatus(@PathVariable(value="trainingID") int trainingID){
		Training t = trainingService.getByTrainingID(trainingID);
		Map<String, Integer> status = new HashMap<String, Integer>();
		if(null != t){
			List<Form> forms = formService.getFormbyTraining(t);
			Iterator<Form> i = forms.iterator();
			while(i.hasNext()){
				Form f = i.next();
				Integer stat = formService.isReleased(f);
				status.put(f.getFormTypeBean().getFormType(), stat);
			}
		}
		return status;
	}
	
	@RequestMapping(value="/api/training/{trainingID}/form/release/participants", method= RequestMethod.POST)
	@ResponseBody
	public Map<String, Boolean> releaseToParticipant(@PathVariable(value="trainingID") int trainingID,
			FormType ft){
		boolean success =false;
		System.out.println("[INFO]Releasing " + ft.getFormType() + " to participants of training " + trainingID);
		Training t = trainingService.getByTrainingID(trainingID);
		List<Participation> participants = attendanceService.getParticipants(t);
		Form form = formService.getForm(trainingID, ft);
		form.setFormFor("Post");
		if(formService.releaseToParticipants(participants, form) && formService.updateForm(form)){
			success = true;
		}
		
		
		return Collections.singletonMap("success", success);
	}
	
	@RequestMapping(value="/api/training/{trainingID}/form/release/supervisors", method= RequestMethod.POST)
	@ResponseBody
	public Map<String, Boolean> releaseToSupervisor(@PathVariable(value="trainingID") int trainingID,
			FormType ft){
		boolean success =false;
		System.out.println("[INFO]Releasing " + ft.getFormType() + " to Supervisors of the Participants in training " + trainingID);
		Training t = trainingService.getByTrainingID(trainingID);
		List<Participation> participants = attendanceService.getParticipants(t);
		Form form = formService.getForm(trainingID, ft);
		form.setFormFor("N/A");
		if(formService.releaseToSupervisor(participants, form) && formService.updateForm(form)){
			success = true;
		}
		
		
		return Collections.singletonMap("success", success);
	}
	
	
	private List<DateModel> getDaysBetweenDates(Date startdate, Date enddate, List<Date> usedDates) {
		List<DateModel> dates = new ArrayList<DateModel>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(startdate);

		while (calendar.getTime().before(enddate)) {

			Date d = calendar.getTime();
			System.out.println("before add  " + d);
			if (isAvailable(usedDates, d)) {

				DateModel result = new DateModel(calendar.getTime());
				System.out.println("added to result : " + result);
				dates.add(result);
			}

			calendar.add(Calendar.DATE, 1);
		}
		if (isAvailable(usedDates, enddate)) {
			dates.add(new DateModel(enddate));
		}

		return dates;
	}

	private boolean isAvailable(List<Date> usedDates, Date date) {
		boolean available = true;
		Iterator<Date> i = usedDates.iterator();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
		while (i.hasNext()) {
			Date current = i.next();
			System.out.println("compare : " + current + " with " + date);
			if (fmt.format(current).equals(fmt.format(date))) {
				available = false;
				break;
			}
		}

		return available;
	}
}
