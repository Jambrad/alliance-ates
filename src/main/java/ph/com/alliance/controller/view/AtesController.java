package ph.com.alliance.controller.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ph.com.alliance.entity.Attendance;
import ph.com.alliance.entity.Employee;
import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.FormDetail;
import ph.com.alliance.entity.FormResult;
import ph.com.alliance.entity.FormType;
import ph.com.alliance.entity.Participation;
import ph.com.alliance.entity.Pending;
import ph.com.alliance.entity.Training;
import ph.com.alliance.entity.TrainingPlan;
import ph.com.alliance.service.AccountDBService;
import ph.com.alliance.service.AttendanceService;
import ph.com.alliance.service.FormService;
import ph.com.alliance.service.TrainingService;


@Controller
@RequestMapping("/ates")
public class AtesController {

	@Autowired
	private AccountDBService accountService;
	
	@Autowired
	private TrainingService trainingService;
	
	@Autowired
	private AttendanceService attendanceService;
	
	@Autowired
	private FormService formService;
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	  public String viewHome(HttpServletRequest request, HttpServletResponse response, ModelMap map) {
	    	System.out.println("@HOME.");

	    
	        return "ates/test/index";
	    }
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	  public String viewLogin(ModelMap map) {
	    	System.out.println("LOGIN.");
	
	    	
	    	
	        return "ates/test/login";
	    }
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	  public String getLogin(HttpServletRequest request, ModelMap map, Employee acc) {
	    	System.out.println("LOGIN2.");
	    
	    	Employee b = accountService.getLogin(acc);
	    	
	    	if(b!=null){
	    		
	    		
	    		if(b.getLoggedIn()==0){
		    		map.put("user",b);
		    		return "ates/test/changepass";
		    	}
	    	
	    	}
	    	
	        return "ates/test/login";
	    }
	
	@RequestMapping(value = "/changepass", method = RequestMethod.GET)
	  public String viewchangePass(ModelMap map) {
			System.out.println("VIEW ADD MODULE CALLED");
			
	        return "ates/test/changepass";
	    }
	
	@RequestMapping(value = "/changepass", method = RequestMethod.POST)
	  public String changePass(HttpServletRequest request, HttpServletResponse response, ModelMap map) {
			System.out.println("VIEW ADD MODULE CALLED");
			
			Employee user = accountService.getUserByID(request.getParameter("employeeID"));
			user.setPassword(request.getParameter("password"));
			
			if(accountService.changePass(user)){
				user.setLoggedIn((byte) 1);
				accountService.changePass(user);
			}
	        return "ates/test/login";
	    }
	
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	  public String viewAll(HttpServletRequest request, ModelMap map) {
			List<Employee> users = accountService.getUserByName(request.getParameter("name"));
			
			map.put("employee", users);	//get users by name nga data
			
			//map.put("employee", accountService.getAll()); //get all nga data
			
			System.out.println("VIEW ALL USERS MODULE CALLED");
	        return "ates/test/view";
	    }
	
	
	
	@RequestMapping(value = "/trainingplan", method = RequestMethod.GET)
	  public String viewAllTP(ModelMap map) {
			//returns List<TP>
			map.put("tp",trainingService.getAllTrainingPlan());	
			
			System.out.println("training plan");
	        return "ates/test/viewTP";
	    }
	
	@RequestMapping(value = "/trainingplan/trainings", method = RequestMethod.GET)
	  public String viewAllTraining(HttpServletRequest request, ModelMap map) {
			
			 int tpID = Integer.parseInt(request.getParameter("trainingPlanID"));
			 map.put("trainPID", trainingService.getByID(tpID));
			 map.put("training", trainingService.getByTrainingPlanID(tpID));	
			
			System.out.println("training");
	        return "ates/test/viewtraining";
	    }
	
	@RequestMapping(value = "/trainingplan/addtrainingplan", method = RequestMethod.GET)
	  public String addTrainingPlan(HttpServletRequest request, ModelMap map) {
			int latest = trainingService.getLatestYear();
			TrainingPlan tp = new TrainingPlan();
			tp.setYear(++latest);
			
			trainingService.createTrainingPlan(tp);
			System.out.println("tp added");
			return this.viewAllTP(map);
	    }
	
	@RequestMapping(value = "/trainingplan/addtraining", method = RequestMethod.GET)
	  public String viewAddTraining(HttpServletRequest request, ModelMap map) {
			map.put("id",request.getParameter("tpID"));
			
			return "ates/test/addtraining";
	    }
	
	@RequestMapping(value = "/trainingplan/addtraining", method = RequestMethod.POST)
	  public String AddTraining(HttpServletRequest request, ModelMap map, Training t) {
			
			//get and set training plan
			TrainingPlan tp = trainingService.getByID(Integer.parseInt(request.getParameter("tpID")));
			t.setTrainingPlan(tp);

			//set faci, dummy data, get faci from front end
			t.setEmployee(accountService.getUserByID("522-0000"));
			
			//list of users, dummy data raning all. get from frontend
			List<Employee> emp = accountService.getAll();
		
			//create training
			trainingService.createTraining(t);
			
			//create participants, needs training and list of employee objects
			attendanceService.addParticipantsBatch(t, emp);
			
			return this.viewAllTP(map);
	    }
	
	@RequestMapping(value = "/trainingplan/deletetraining", method = RequestMethod.POST)
	  public String deleteTraining(HttpServletRequest request, ModelMap map, Training t) {
		
			trainingService.deleteTrainingByID(Integer.parseInt(request.getParameter("trainingID")));
		
			return this.viewAllTP(map);
	    }
	
	@RequestMapping(value = "/trainingplan/edittraining", method = RequestMethod.GET)
	  public String viewEditTraining(HttpServletRequest request, ModelMap map) {
		
			Training training = trainingService.getByTrainingID(Integer.parseInt(request.getParameter("trainingID")));
			map.put("training", training);

			
			return "ates/test/editTraining";
	    }
	
	@RequestMapping(value = "/trainingplan/edittraining", method = RequestMethod.POST)
	  public String editTraining(HttpServletRequest request, ModelMap map, Training t) {
		
			TrainingPlan tp = trainingService.getByID(Integer.parseInt(request.getParameter("tpID")));
			t.setTrainingPlan(tp);
			
			trainingService.updateTraining(t);
			return this.viewAllTP(map);
	    }
	

		@SuppressWarnings("deprecation")
		@RequestMapping(value = "/attendance", method = RequestMethod.GET)
		public String viewAttendance(HttpServletRequest request, ModelMap map) {
			
			//get training
			Training t = trainingService.getByTrainingID(1032);
			
			//get list of participants from training
			List<Participation> p = attendanceService.getParticipants(t);
			
			//date
			Date date = new Date(117,5,5);
			
			//get list of attendance by training and date
			List<Attendance> a = attendanceService.getAttendance(p, date);
			map.put("attendance", a);
			
			//get distinct dates. needs training object
			List<Date> d = attendanceService.getDates(t);
	    	
	    	for(Date b : d){
	    		System.out.println(b);
	    	}
	    	
			return "ates/test/attendance";
		  }
		
		@RequestMapping(value = "/participants", method = RequestMethod.GET)
		public String viewParitipants(HttpServletRequest request, ModelMap map) {
		
			
			Training t = trainingService.getByTrainingID(1015);
			List<Participation> participants = attendanceService.getParticipants(t);
			
			map.put("participants", participants);
		
		
			
				return "ates/test/participants";
		  }
			
		//ask razil wat is dis
		@RequestMapping(value = "/form/checkbox", method = RequestMethod.GET)
		public String displayChecks(HttpServletRequest request, ModelMap map){
			List<FormDetail> choices = formService.getChecks();
			map.put("choices", choices);
			return "ates/form/checkbox";
		}
		
		
		//After click sa Fill up, open new tab, wrap fill up button with form method GET target new
		//link from pending jsp to forms, send form object, employee
		//GET
		@RequestMapping(value = "form/evaluation", method = RequestMethod.POST)
		public String displayForm(HttpServletRequest request, ModelMap map){
			
			int formID = Integer.parseInt(request.getParameter("formID"));
			//String formType = request.getParameter("formType");
			String employeeID = request.getParameter("employeeID");
			Form f = formService.getFormID(formID);
			
			//pwede ra form.getFormTypeBean.getTypeID og form.getFormID ani
			 FormType type = f.getFormTypeBean();
			//FormType type = formService.getFormType(formType);
			List<String> categoryList = formService.getCategoryList(formID, type.getTypeID());
			List<FormDetail> questionList = formService.getQuestionList(type.getTypeID());
			
			//how to this
			map.put("training", f.getTraining());
			map.put("formID", formID);
			map.put("category", categoryList);
			map.put("question", questionList);
			map.put("formType", type);
			map.put("participant", employeeID);
			
			return "ates/form/evaluation";
		}
		
		
		@RequestMapping(value = "form/formTest", method = RequestMethod.GET)
		public String formTest(HttpServletRequest request, ModelMap map){
			String formID = request.getParameter("formID");
			String formType = request.getParameter("formType");
			String employeeID = request.getParameter("employeeID");
			
			map.put("formID", formID);
			map.put("formType", formType);
			
	
			map.put("employeeID", employeeID);
			return "ates/form/formTest";
		}
		
		
		//controller for submit form
		@RequestMapping(value = "/form/evaluation-done", method = RequestMethod.POST)
		public String viewSADone(HttpServletRequest request, @RequestParam(value="result") String[] answerList){
		
			//get form id from pending jsp
			Form f = formService.getFormID(Integer.parseInt(request.getParameter("formID")));
			//get formdetails
			List<FormDetail> fd = formService.getFormDetailByFormID(f);
			
			//dummy. get employee from pending jsp
			Employee e = accountService.getUserByID("522-0004");
			
			//something2 
			Iterator<String> ans = Arrays.asList(answerList).iterator();
			Iterator<FormDetail> itFD = fd.iterator();
			
			List<FormResult> fr = new ArrayList<FormResult>();
			
			while(ans.hasNext() && itFD.hasNext()){
				FormResult af = new FormResult(e,f,itFD.next());
				af.setAnswerString(ans.next());
				
				fr.add(af);
			}
			
			System.out.println(formService.createFormResults(fr));
			for(String a: answerList)
				System.out.println(a);
		
			return "ates/form/evaluation";
		}
		
		
		
		@RequestMapping(value = "/inbox", method = RequestMethod.GET)
		public String checkInbox(ModelMap map){
			
			Employee e = accountService.getUserByID("522-0000");
			List<Pending> p = formService.getPending(e);
			
			map.put("inbox", p);
			
			return "ates/test/inbox";
		}
		
		@RequestMapping(value = "/report", method = RequestMethod.GET)
		public String getReport(ModelMap map){
			
			Form f = formService.getFormID(410);
			
		    Map<FormDetail, int[]> test =formService.getReport(f);
		    
		    for(Map.Entry<FormDetail, int[]> a: test.entrySet()){
		    	System.out.println(a.getKey().getQuestionNumber() + ". " + a.getKey().getQuestionString() + " ");
		    	for(int af : a.getValue()){
		    		System.out.print("[" + af);
		    		System.out.print("]\n");
		    	}
		    }
		    
		    

			return "ates/test/inbox";
		}
		
		@RequestMapping(value = "/test", method = RequestMethod.GET)
		public String test(ModelMap map){
			
			
			return "ates/form/success";
		}
}
