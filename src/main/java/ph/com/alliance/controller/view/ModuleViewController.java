package ph.com.alliance.controller.view;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ph.com.alliance.entity.Employee;
import ph.com.alliance.service.AccountDBService;
import ph.com.alliance.service.TrainingService;


/**
 * 
 * Controller used to handle the logging in, logging out, and changing of the password of the user.
 * 
 *
 */

@Controller
@RequestMapping("/AllianceATES")
public class ModuleViewController {
	@Autowired
	private AccountDBService accountService;
	@Autowired
	private TrainingService dbSvc;

	/**
	 * Loads index jsp
	 * Serves as homepage of site
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String indexController(ModelMap map){
		System.out.println("arrived in index controller...");
		return "ates/index";
	}

	
	/**
	 * Loads index jsp
	 * 
	 * @param request
	 * @param response
	 * @param map
	 * @param Employee entity
	 * @return
	 */
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String getLoginPostController(HttpServletRequest request, HttpServletResponse response,Employee acc, ModelMap map){
		System.out.println("Login Controller GET");
		return "ates/index";
	
	}

	/** 
	 * Checks whether the account which contains the user and password exists then if the user is an admin, facilitator, or a general user.
	 * Redirects to login page stating the error if checking fails.
	 * Redirects to changePassword if its the user's first time logging in.
	 *
	 * @param request
	 * @param response
	 * @param map
	 * @param Employee entity
	 * @return
	 */
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginPostController(HttpServletRequest request, HttpServletResponse response,Employee acc, ModelMap map){
		System.out.println("Login Controller");
		
		if( (null != acc ) && !acc.getEmployeeID().equals("") && !acc.getPassword().equals("")){

			System.out.println("Account exists");
			Employee emp = accountService.getLogin(acc);
			boolean success = emp != null;				
			
			if(success){
				System.out.println("Setting up session attribs");
				request.getSession().setAttribute("isLoggedIn", "true");
				request.getSession().setAttribute("empID", emp.getEmployeeID());
				request.getSession().setAttribute("admin", "false");
				request.getSession().setAttribute("faci", "false");
				request.getSession().setAttribute("general", "false");
				try {

					System.out.println(request.getParameter("loginAs"));
					switch(request.getParameter("LoginAs")){
						
					case "admin":	
						if(accountService.isAdmin(emp)){
							System.out.println("Verifying administrator...");
							request.getSession().setAttribute("admin","true" );								
							System.out.println("Going to admin jsp...");

							if(0 == emp.getLoggedIn())
								response.sendRedirect(request.getContextPath() + "/AllianceATES/changePassword");
							else
							response.sendRedirect(request.getContextPath() + "/AllianceATES/admin");
						}
						else{
							request.getSession().invalidate();
							map.put("loginStatus", "notAdmin");
							return indexController(map);
						}
						break;
						
					case "facilitator":
						System.out.println("Verifying facilitator...");
						if(dbSvc.isFacilitator(emp)){
							request.getSession().setAttribute("faci","true" );
							System.out.println("Going to faci jsp...");

							if(0 == emp.getLoggedIn())
								response.sendRedirect(request.getContextPath() + "/AllianceATES/changePassword");
							else
							response.sendRedirect(request.getContextPath() + "/AllianceATES/facilitator");
						}
						else{
							request.getSession().invalidate();
							map.put("loginStatus", "notFacilitator");
							return indexController(map);
						}
						break;
						
					case "GeneralLogin":
						request.getSession().setAttribute("general", "true");
						System.out.println("Logging in as general user..");

						if(0 == emp.getLoggedIn())
							response.sendRedirect(request.getContextPath() + "/AllianceATES/changePassword");
						else
						response.sendRedirect(request.getContextPath() + "/AllianceATES/general");
						
						break;
						
					default:
						request.getSession().invalidate();
						System.out.println("Session destroyed.");
						map.put("loginStatus", "failed");
						return indexController(map);
					
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Moving to index..");
				return "ates/index";
			}else{
				request.getSession().invalidate();
				System.out.println("Session destroyed.");
				map.put("loginStatus", "failed");
				return indexController(map);
			}
			
			
		}else{
			request.getSession().invalidate();
			System.out.println("Session destroyed.");
			map.put("loginStatus", "failed");
			return indexController(map);
		}
		
	}


	/**
	 * Logs out the user by ending the session and redirects to the login page.
	 * 
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 * 
	 */
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public void logoutController(HttpServletRequest request,HttpServletResponse response,ModelMap map){
		System.out.println("You have logged out");
		request.getSession().invalidate();
		try {
			response.sendRedirect(request.getContextPath() + "/AllianceATES/index");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	

	/**
	 * Loads changePassword jsp
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/changePassword", method = RequestMethod.GET)
	public String changePasswordView(ModelMap map){
		return "ates/changePassword";
	}
	
	/** 
	 * Updates the database of the new password of the user.
	 * 
	 * @param newPass
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */
	
	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)
	public String changePassword(@RequestParam(value="newPass", required = true) String newPass,
			HttpServletRequest request,HttpServletResponse response,ModelMap map){
		
		Employee user = accountService.getUserByID(request.getSession().getAttribute("empID").toString());
		user.setPassword(newPass);
		user.setLoggedIn((byte) 1);
		if(!accountService.changePass(user)){
			map.put("changePasswordStatus", "fail");
			return "ates/changePassword";
		}
		
		try {
			System.out.println("Changed Pass success");
			response.sendRedirect(request.getContextPath() + "/AllianceATES/index");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "ates/index";
	}
	
	
		
	
}
