package ph.com.alliance.controller.view;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ph.com.alliance.entity.Employee;
import ph.com.alliance.entity.Participation;
import ph.com.alliance.entity.Pending;
import ph.com.alliance.entity.Training;
import ph.com.alliance.entity.TrainingPlan;
import ph.com.alliance.service.AccountDBService;
import ph.com.alliance.service.AttendanceService;
import ph.com.alliance.service.FormService;
import ph.com.alliance.service.TrainingService;


/**
 * Controller used to handle Facilitator priviledges
 * In this controller, the user can view the training plans and all the trainings in which they are assigned
 * as facilitators and also fill up pending forms they are to answer.
 * Facilitators can edit the trainings where they are assigned to.
 *
 */

@Controller
@RequestMapping("/AllianceATES/facilitator")
public class FacilitatorController {
	@Autowired
	private AccountDBService accountService;

	@Autowired
	private TrainingService trainingService;
	
	@Autowired
	private AttendanceService attendanceService;

	@Autowired
	private FormService formService;


	/**
	 * This loads the facilitator's main page.
	 * 
	 * @param request
	 * @param map
	 * @return
	 */	
	@RequestMapping(method = RequestMethod.GET)
	public String faciPage(HttpServletRequest request, ModelMap map) {
		System.out.println("Entered Facilitator Page");
		Employee emp = accountService.getUserByID(request.getSession().getAttribute("empID").toString());
		map.put("emp", emp);
		int numOfPending = formService.getPending(emp).size();
		map.put("pendingCount", numOfPending);
		System.out.println("Loading Facilitator page...");
		return "ates/facilitator/faci";
	}

	/**
	 * Maps all the Training Plans.
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/trainingPlans", method = RequestMethod.GET)
	public String faciUsersPage(ModelMap map) {
		//Get all Training Plans
		List<TrainingPlan> tpList = trainingService.getAllTrainingPlan();
		map.put("tp", tpList);
		return "ates/facilitator/trainingPlans";
	}
	
	
	/**
	 * Maps the Training Plan specified using the parameter id.
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/trainingPlan/{id}", method = RequestMethod.GET)
	public String facitrainingPlan(@PathVariable(value = "id") int id, ModelMap map) {
		TrainingPlan tp = trainingService.getByID(id);
		System.out.println("Trainining Plan year " + tp.getYear());
		map.put("tp", tp);
		return "ates/facilitator/specificTrainingPlan";
	}

	/**
	 * Maps the List of Trainings in which the user is assigned as facilitator.
	 * 
	 * @param request
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/training/view", method = RequestMethod.GET)
	public String facitrainingView(HttpServletRequest request,ModelMap map) {
		String empID = request.getSession().getAttribute("empID").toString();
		Employee faci = accountService.getUserByID(empID);
		List<Training> trainings = trainingService.getTrainingByFacilitator(faci);
		map.put("trainings", trainings);
		return "ates/facilitator/trainings";
	}

	/**
	 * Maps the details of the Training using the trainingID parameter.
	 * @param trainingID
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/training/details/{trainingID}", method = RequestMethod.GET)
	public String trainingDetails(@PathVariable(value = "trainingID") String trainingID, ModelMap map) {
		if (null != trainingID) {
			// Get training
			Training t = trainingService.getByTrainingID(Integer.parseInt(trainingID));

			// Get participants
			List<Participation> participants = attendanceService.getParticipants(t);
			map.put("training", t);
			map.put("participants", participants);

		}

		return "ates/facilitator/trainingDetails";
	}
	
	/**
	 * Loads and maps all the pending forms to be answered by the user as a facilitator
	 * 
	 * @param empID
	 * @param map
	 * @return
	 */
	
	@RequestMapping(value="/pending/{empID}", method = RequestMethod.GET)
	public String getPending(@PathVariable(value="empID") String empID, ModelMap map){
		Employee emp = accountService.getUserByID(empID);
		List<Pending> pending = formService.getPendingFaci(emp);
		map.put("pending", pending);
		return "ates/form/pending";
	}
	
}
