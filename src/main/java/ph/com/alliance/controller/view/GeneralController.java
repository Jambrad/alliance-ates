package ph.com.alliance.controller.view;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ph.com.alliance.entity.Employee;
import ph.com.alliance.entity.Pending;
import ph.com.alliance.entity.Training;
import ph.com.alliance.entity.TrainingPlan;
import ph.com.alliance.service.AccountDBService;
import ph.com.alliance.service.AttendanceService;
import ph.com.alliance.service.FormService;
import ph.com.alliance.service.TrainingService;

/**
 * Controller used to handle General User priviledges
 * The general user includes both the Participant and the Supervisor
 * In this controller, the user can only view the training plans and 
 * fill up pending forms they are to answer.
 *
 */
@Controller

@RequestMapping("/AllianceATES/general")
public class GeneralController {
	@Autowired
	private AccountDBService accountService;

	@Autowired
	private TrainingService trainingService;
	
	@Autowired
	private FormService formService;
	
	@Autowired
	private AttendanceService attendanceService;
	
	
	/**
	 * This loads the general user's main page.
	 * 
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */	
    @RequestMapping(method=RequestMethod.GET)
    public String generalUserMain(HttpServletRequest request, HttpServletResponse response, ModelMap map){
    	System.out.println("Entered general controller");
		try {
			Employee emp = accountService.getUserByID(request.getSession().getAttribute("empID").toString());
			map.put("emp", emp);
			int numOfPending = formService.getPending(emp).size();
			map.put("pendingCount", numOfPending);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Loading general page...");
		return "ates/general/general";
	}

    
    /**
     * Maps all the Training Plans
     * 
     * @param map
     * @return
     */
	@RequestMapping(value = "/trainingPlans", method = RequestMethod.GET)
	public String generalUsersPage(ModelMap map) {
		//Get all Training Plans
		List<TrainingPlan> tpList = trainingService.getAllTrainingPlan();
		map.put("tp", tpList);
		return "ates/general/trainingPlans";
	}

	
	/**
	 * Maps the Training Plan specified using the parameter id.
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/trainingPlan/{id}", method = RequestMethod.GET)
	public String generaltrainingPlan(@PathVariable(value = "id") int id, ModelMap map) {
		TrainingPlan tp = trainingService.getByID(id);
		System.out.println("Trainining Plan year " + tp.getYear());
		map.put("tp", tp);
		return "ates/general/specificTrainingPlan";
	}

		
	/**
	 * Redirects to trainings jsp when GET
	 * 
	 * @param tpID
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/training/view", method = RequestMethod.GET)
	public String generaltrainingView(@RequestParam(value = "trainingPlanID", required= false) String tpID, ModelMap map) {
		return "ates/general/trainings";
	}
	
	
	/**
	 * Retrieves the list of trainings using the trainingPlanID parameter
	 * 
	 * @param tpID
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/training/view/{trainingPlanID}", method = RequestMethod.POST)
	public String facitrainingViewByTpId(@PathVariable(value="trainingPlanID") String tpID, ModelMap map) {
		if(null != tpID) {
			List<Training> trainingList = trainingService.getByTrainingPlanID(Integer.parseInt(tpID));
			map.put("trainings", trainingList);
		}
		return "ates/general/trainings";
	}


	/**
	 * Retrieves the list of forms to be answered by the participant/supervisor.
	 * 
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/getPending/{id}", method = RequestMethod.GET)
	public String getPendings(@PathVariable(value="id") String id,ModelMap map) {
		Employee emp = accountService.getUserByID(id);
		List<Pending> pending = formService.getPending(emp);
		map.put("pending", pending);
		return "ates/form/pending";
	}

	
}

    
    
    
    
    
    
    
    


