package ph.com.alliance.controller.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ph.com.alliance.entity.Employee;
import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.FormDetail;
import ph.com.alliance.entity.FormResult;
import ph.com.alliance.entity.Pending;
import ph.com.alliance.service.AccountDBService;
import ph.com.alliance.service.FormService;


@Controller
@RequestMapping("/AllianceATES/form")
public class FormController {
	
	@Autowired
	private FormService formService;
	
	@Autowired
	private AccountDBService accountService;
	
	/**
	 * A function to load a specific form
	 * 
	 * @param request
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String loadForm(HttpServletRequest request, ModelMap map){
		
		//from pending
		Form form = formService.getFormID(Integer.parseInt(request.getParameter("formID")));
		String employeeID = request.getParameter("employeeID");
		
		//get categories and questions
		List<String> categoryList = formService.getCategoryList(form.getFormID(), form.getFormTypeBean().getTypeID());
		List<FormDetail> questionList = formService.getQuestionList(form.getFormTypeBean().getTypeID());
		
		//for jsp
		map.put("training", form.getTraining());
		map.put("category", categoryList);
		map.put("question", questionList);
		map.put("formType", form.getFormTypeBean().getFormType());
		map.put("formID", form.getFormID());
		map.put("employeeID", employeeID);
		map.put("pendingID", request.getParameter("pendingID"));
		//system log
		System.out.println("[INFO] Form " + form.getFormID() + " has been loaded.");
		
		return "ates/form/form";
	}
	
	/**
	 * A function to display all checkbox options
	 * 
	 * @param request
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/checkbox", method = RequestMethod.GET)
	public String displayCheckboxes(HttpServletRequest request, ModelMap map){
		List<FormDetail> choices = formService.getChecks();
		map.put("choices", choices);
		return "ates/form/checkbox";
	}
	
	/**
	 * A function to submit form results
	 * 
	 * @param request
	 * @param answerList
	 * @return
	 */
	@RequestMapping(value = "/submit", method = RequestMethod.POST)
	public String submitForm(HttpServletRequest request, @RequestParam(value="result") String[] answerList, ModelMap map){
		
		//get form id from pending jsp
		Form f = formService.getFormID(Integer.parseInt(request.getParameter("formID")));
		//get formdetails
		List<FormDetail> fd = formService.getFormDetailByFormID(f);
		
		//Get employee
		Employee e = accountService.getUserByID(request.getParameter("employeeID"));
		
		//Get iterators 
		Iterator<String> ans = Arrays.asList(answerList).iterator();
		Iterator<FormDetail> itFD = fd.iterator();
		
		List<FormResult> fr = new ArrayList<FormResult>();
		
		while(ans.hasNext() && itFD.hasNext()){
			FormResult af = new FormResult(e,f,itFD.next());
			af.setAnswerString(ans.next());
			
			fr.add(af);
		}
		
		boolean success = formService.createFormResults(fr);
		//system log
		System.out.println("[INFO] Results have been stored: " + success);
		
		if(success){
			Pending p = formService.getPendingByID(Integer.parseInt(request.getParameter("pendingID")));
			if(null != p){
				p.setIsDone((byte) 1);
				formService.updatePendingObject(p);
			}
			map.put("status", "success");
			//return success page
		}else{
			map.put("status", "fail");
			//return fail page
		}
		return "ates/form/status";
	}
	
}
