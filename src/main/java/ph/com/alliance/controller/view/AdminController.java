package ph.com.alliance.controller.view;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ph.com.alliance.entity.Attendance;
import ph.com.alliance.entity.Employee;
import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.FormDetail;
import ph.com.alliance.entity.FormType;
import ph.com.alliance.entity.Participation;
import ph.com.alliance.entity.Training;
import ph.com.alliance.entity.TrainingPlan;
import ph.com.alliance.service.AccountDBService;
import ph.com.alliance.service.AttendanceService;
import ph.com.alliance.service.FormService;
import ph.com.alliance.service.TrainingService;

/**
 * Controller used to handle Admin priviledges
 * In this controller, the user can manage the training plans, trainings, attendance, and generation of reports.
 * User can add, edit, and view all the training plans.
 * User can add, edit, view, and delete the trainings.
 * User can create and record the attendance throughout the duration of the training.
 * User can release forms and generate reports from them.
 */

@Controller
@RequestMapping("/AllianceATES/admin")
public class AdminController {
	@Autowired
	private AccountDBService accountService;

	@Autowired
	private TrainingService trainingService;

	@Autowired
	private AttendanceService attendanceService;
	
	@Autowired
	private FormService formService;
	

	/**
	 * Displays the admin jsp and maps the employee object
	 * 
	 * @param request
	 * @param map
	 * @return
	 */

	@RequestMapping(method = RequestMethod.GET)
	public String adminPage(HttpServletRequest request, ModelMap map) {
		System.out.println("Entered Admin Controller Page");
		try {
			Employee emp = accountService.getUserByID(request.getSession().getAttribute("empID").toString());
			map.put("emp", emp);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "ates/administrator/admin";
	}


	/** 
	 * Adds more training plans. Training Plan IDs and year are auto incremented
	 * 
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 * 
	 */
	@RequestMapping(value = "/trainingPlan/add", method = RequestMethod.POST)
	public void addTrainingPlan(HttpServletRequest request, HttpServletResponse response, ModelMap map) {
		// Gets the latest year of training plan
		int latest = trainingService.getLatestYear();

		// Creates new training plan with latest year
		TrainingPlan tp = new TrainingPlan();
		tp.setYear(++latest);

		// Add training plan
		trainingService.createTrainingPlan(tp);
		System.out.println("tp added");

		try {
			response.sendRedirect(request.getContextPath() + "/AllianceATES/admin/trainingPlans");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	/**
	 * Maps the list of Training Plans for display in the trainingPlans jsp.
	 * 
	 * @param map
	 * @return
	 */

	@RequestMapping(value = "/trainingPlans", method = RequestMethod.GET)
	public String adminUsersPage(ModelMap map) {
		// Get all Training Plans
		List<TrainingPlan> tpList = trainingService.getAllTrainingPlan();
		map.put("tp", tpList);
		return "ates/administrator/trainingPlans";
	}

	/**
	 * Retrieves the training plan using the training plan id
	 * 
	 * @param id
	 * @param map
	 * @return 	
	 */
	@RequestMapping(value = "/trainingPlan/{id}", method = RequestMethod.GET)
	public String trainingPlan(@PathVariable(value = "id") int id, ModelMap map) {
		TrainingPlan tp = trainingService.getByID(id);
		System.out.println("Trainining Plan year " + tp.getYear());
		map.put("tp", tp);
		return "ates/administrator/specificTrainingPlan";
	}

	/**
	 * Loads the addTraining jsp
	 * 
	 * @param trainingPlanID
	 * @param map
	 * @return
	 */
	
	@RequestMapping(value = "/training/add", method = RequestMethod.GET)
	public String addTrainingView(@RequestParam(value = "trainingPlanID") String tpID, ModelMap map) {
	    System.out.println("TPID: " + tpID);
		map.put("tpID", tpID);
		return "ates/administrator/addTraining";
	}

	/**
	 * Trainings are created here. 
	 * Facilitators and participants are added here in this controller.
	 * 
	 * @param request
	 * @param tpID
	 * @param t
	 * @param map
	 * @return
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/training/add", method = RequestMethod.POST)
	public String addTraining(HttpServletRequest request, @RequestParam(value = "trainingPlanID") String tpID,
			Training t, ModelMap map) {
		// get and set training plan
		TrainingPlan tp = trainingService.getByID(Integer.parseInt(tpID));
		t.setTrainingPlan(tp);

		// set faci
		Employee faci = accountService.getUserByID(request.getParameter("facilitator"));
		if (null != faci) {
			t.setEmployee(faci);
		}

		// Simplify dates
		try {
			t.setStartDate(new Date(simplifyDate(t.getStartDate().toString())));
			t.setEndDate(new Date(simplifyDate(t.getEndDate().toString())));
		} catch (ParseException e) {
			System.out.println("Error simplfying dates");
			e.printStackTrace();
		}

		// convert empID to list<emp>
		String[] participantIDs = request.getParameterValues("participantsToAdd[]");
		List<Employee> employeeList = new ArrayList<Employee>();
		Employee emp = null;
		if (participantIDs != null) {
			for (String pID : participantIDs) {
				emp = accountService.getUserByID(pID);
				if (null != emp) {
					employeeList.add(emp);
				}
			}

		}

		System.out.println("Creating training....");
		// create training
		if(trainingService.createTraining(t)){
			System.out.println("Creating participants and attendance....");
			// create participants and attendance
			if (attendanceService.addParticipantsBatch(t, employeeList)) {
				// If success
				map.put("trainingStatus", "success");
				System.out.println("Success! \n" + t.toString());
			} else {
				// If not success
				map.put("trainingStatus", "fail");
				System.out.println("Failed! \n" + t.toString());
			}
		}else{
			map.put("trainingStatus", "fail");
			System.out.println("Failed! \n" + t.toString());
		}
		
		//generate forms
				List<FormType> ft = new ArrayList<FormType>();
				ft.add(formService.getFormType("Skills Assessment"));
				ft.add(formService.getFormType("Course Feedback"));
				ft.add(formService.getFormType("Training Effectiveness Assessment"));

				List<Form> f = new ArrayList<Form>();
				for(FormType ftt : ft){
					Form form = new Form(ftt, t);
					form.setFormFor("Pre");
					
					f.add(form);
				}
				
				System.out.println("Forms created: " + formService.createForms(f));

		return addTrainingView(String.valueOf(tpID),map);
	}

	/**
	 * Loads the trainings jsp
	 * 
	 * @param tpID
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/training/view", method = RequestMethod.GET)
	public String trainingView(@RequestParam(value = "trainingPlanID", required = false) String tpID, ModelMap map) {
		return "ates/administrator/trainings";
	}

	/**
	 * Retrieves the list of trainings under the selected training plan using the parameter trainingPlanID
	 * 
	 * @param tpID
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/training/view/{trainingPlanID}", method = RequestMethod.POST)
	public String trainingViewByTpId(@PathVariable(value = "trainingPlanID") String tpID, ModelMap map) {
		if (null != tpID) {
			List<Training> trainingList = trainingService.getByTrainingPlanID(Integer.parseInt(tpID));
			map.put("tpID", tpID);
			map.put("trainings", trainingList);
		}
		return "ates/administrator/trainings";
	}
	
	/**
	 * Retrieves the list of participants for the specific training using the parameter trainingID
	 * 
	 * @param trainingID
	 * @param map
	 * @return
	 */

	@RequestMapping(value = "/training/details/{trainingID}", method = RequestMethod.GET)
	public String trainingDetails(@PathVariable(value = "trainingID") String trainingID, ModelMap map) {
		if (null != trainingID) {
			// Get training
			Training t = trainingService.getByTrainingID(Integer.parseInt(trainingID));

			// Get participants
			List<Participation> participants = attendanceService.getParticipants(t);
			map.put("training", t);
			map.put("participants", participants);

		}

		return "ates/administrator/trainingDetails";
	}
	
	/**
	 * Deletes the checked trainings displayed in the jsp page.
	 * 
	 * @param map
	 * @param request
	 * @return
	 */

	@RequestMapping(value = "/training/delete", method = RequestMethod.POST)
	public String deleteTrainings(ModelMap map, HttpServletRequest request) {
		String[] ids = request.getParameterValues("trainingID");
		List<Integer> idsToDelete = new ArrayList<Integer>();
		String tpID = request.getParameter("trainingPlanID");
		boolean result = false;
		// convert to List<Integer>
		for (String id : ids) {
			idsToDelete.add(Integer.parseInt(id));
		}

		result = trainingService.deleteTrainingBatch(idsToDelete);
		map.put("deleteStatus", (result) ? "success" : "fail");

		return trainingViewByTpId(tpID, map);

	}

	/**
	 * Deletes training selected from the Calendar view
	 * 
	 * @param map
	 * @param id
	 * @param tpID
	 * @return
	 */
	@RequestMapping(value = "/training-calendar/delete", method = RequestMethod.POST)
	public String deleteTrainingFromCalendar(ModelMap map, @RequestParam(value = "trainingID") int id,
			@RequestParam(value = "tpID") int tpID) {
		boolean status = trainingService.deleteTrainingByID(id);
		map.put("deleteStatus", (status) ? "success" : "fail");
		return trainingPlan(tpID, map);
	}

	/**
	 * This controller maps the specified training object so it can be edited in the jsp
	 *
	 * @param request
	 * @param map
	 * @param trainingID
	 * @return
	 */
	@RequestMapping(value = "/training/edit", method = RequestMethod.GET)
	public String editTraining(HttpServletRequest request, ModelMap map,
			@RequestParam(value = "trainingID") int trainingID) {
		Training t = trainingService.getByTrainingID(trainingID);
		map.put("training", t);
		return "ates/administrator/editTraining";
	}

	/**
	 * This controller  retrieves the values sumbitted by the jsp and calls the services to apply the changes to the database
	 * 
	 * @param request
	 * @param response
	 * @param map
	 * @param t
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/training/edit", method = RequestMethod.POST)
	public void saveEditTraining(HttpServletRequest request, HttpServletResponse response, ModelMap map, Training t) {
		// get and set training plan
		Training training = trainingService.getByTrainingID(t.getTrainingID());
		t.setTrainingPlan(training.getTrainingPlan());

		// set faci
		Employee faci = accountService.getUserByID(request.getParameter("facilitator"));
		if (null != faci) {
			t.setEmployee(faci);
		}

		// Simplify dates
		try {
			t.setStartDate(new Date(simplifyDate(t.getStartDate().toString())));
			t.setEndDate(new Date(simplifyDate(t.getEndDate().toString())));
		} catch (ParseException e) {
			System.out.println("Error simplfying dates");
			e.printStackTrace();
		}

		// convert empID to list<emp>
		String[] participantIDs = request.getParameterValues("participantsToAdd[]");
		List<Employee> employeeList = new ArrayList<Employee>();
		Employee emp = null;
		if (participantIDs != null) {
			for (String pID : participantIDs) {
				emp = accountService.getUserByID(pID);
				if (null != emp) {
					employeeList.add(emp);
				}
			}

		}

		System.out.println("updating training....");
		// create training
		trainingService.updateTraining(t);

		System.out.println("Updating participants and attendance....");
		// create participants and attendance
		List<Participation> old = attendanceService.getParticipants(training);
		if (attendanceService.deleteParticipantsBatch(old)) {
			if(attendanceService.addParticipantsBatch(t, employeeList)){
				map.put("trainingStatus", "success");
				System.out.println("Success! \n" + t.toString());
			}else {
				// If not success
				map.put("trainingStatus", "fail");
				System.out.println("Failed in adding! \n" + t.toString());
			}
			
		} else {
			// If not success
			map.put("trainingStatus", "fail");
			System.out.println("Failed in deleting! \n" + t.toString());
		}
		
		try {
			response.sendRedirect(request.getContextPath() + "/AllianceATES/admin/training/details/" + t.getTrainingID());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Loads the attendance jsp
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/attendance", method=RequestMethod.GET)
	public String viewAttendance(ModelMap map){
		return "ates/administrator/attendance";
	}
	
	/**
	 * Retrieves the attendance on a specific date of a specific training and maps it for viewing in the jsp.
	 * 
	 * @param trainingID
	 * @param date
	 * @param map
	 * @return
	 */
	
	@RequestMapping(value="/attendance/{trainingID}", method=RequestMethod.GET)
	public String viewAttendanceTraining(@PathVariable(value="trainingID") int trainingID,@RequestParam(value="date", required = false) Date date ,ModelMap map){
		Training training = trainingService.getByTrainingID(trainingID);
		if(null != date){
			System.out.println("displaying attendance by date.. " + date.toString());
			List<Participation> participants = attendanceService.getParticipants(training);
			List<Attendance> attendanceList = attendanceService.getAttendance(participants, date);
			map.put("attendanceList", attendanceList);
		}else{
			List<Date> dates = attendanceService.getDates(training);
			map.put("dates", dates);
		}
	
		return "ates/administrator/attendance";
	}
	
	
	/**
	 * Retrieves the details(questions, type of form) of a specific form from the database and maps it to generate the form in the jsp
	 * 
	 * @param trainingID
	 * @param map
	 * @param ft
	 * @return
	 */
	
	@RequestMapping(value="/training/{trainingID}/form/generate", method = RequestMethod.GET)
	public String generateReport(@PathVariable(value="trainingID") int trainingID, ModelMap map, FormType ft){
		Form form = formService.getForm(trainingID, ft);
		List<FormDetail> questions = formService.getFormDetailByFormID(form);
		map.put("form", form);
		map.put("questions", questions);
		
		return "ates/form/report";
	}
	
	/**
	 * Simplifies the date format into a YYYY/MM/DD string
	 * 
	 * @param dateStr
	 * @return
	 * @throws ParseException
	 */
	private String simplifyDate(String dateStr) throws ParseException {

		DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
		Date date = (Date) formatter.parse(dateStr);
		System.out.println(date);

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		String formatedDate = cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/"
				+ cal.get(Calendar.DATE);
		System.out.println("formatedDate : " + formatedDate);
		return formatedDate;
	}

}
