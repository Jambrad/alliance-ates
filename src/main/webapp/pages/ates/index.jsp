<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>Alliance - ATES</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/styles.ates.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/lib/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/media-queries.css" />
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<head>
<body style="background-color: rgb(181, 0, 0) !important;">


	<div id="main-container" class="container">
		<div>
			<div class="wrapper">
				<form class="form-signin"
					action="${pageContext.request.contextPath}/AllianceATES/login"
					method="post">

					<img src="${pageContext.request.contextPath}/images/asi_logo.png"
						style="width: 310px; padding: 10px; margin-bottom: 20px;" />
					<c:if test="${loginStatus eq 'failed'}">
						<div class="alert alert-danger">
							<strong>Failed!</strong> Employee ID and password doesn't match.
						</div>
					</c:if>
					<c:if test="${loginStatus eq 'notFacilitator'}">
						<div class="alert alert-danger">
							<strong>Failed!</strong> User is not assigned as Facilitator.
						</div>
					</c:if>
					<c:if test="${loginStatus eq 'notAdmin'}">
						<div class="alert alert-danger">
							<strong>Failed!</strong> User is not assigned as Administrator.
						</div>
					</c:if>
					<input type="text" class="form-control" name="employeeID" id="employeeID"
						placeholder="ID Number" required="" autofocus="" /> <input
						type="password" class="form-control" name="password" id="password"
						placeholder="Password" required="" />
					
					<div class="radio">
 						 <label><input type="radio" name="LoginAs" value="GeneralLogin" checked="checked">Participant/Supervisor</label>
					</div>
					<div class="radio">
  						<label><input type="radio"  name="LoginAs"  value="admin">Administrator</label>
					</div>
					<div class="radio">
  						<label><input type="radio" name="LoginAs" value="facilitator">Facilitator</label>
					</div>
					<button type="submit" id="btnSubmit" class="btn btn-danger btn-block">Login</button>
					
				</form>
			</div>
		</div>

	</div>

	<script
		src="${pageContext.request.contextPath}/lib/js/jquery-1.10.1.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/lib/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/index.ates.js"></script>
	<script
		src="${pageContext.request.contextPath}/lib/js/jquery.blockUI.js"></script>
</body>

</html>