<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>Alliance - ATES</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/styles.ates.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/lib/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/media-queries.css" />
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<head>
<body style="background-color: rgb(181, 0, 0) !important;">


	<div id="main-container" class="container">
		<div>
			<div class="wrapper">
				<form class="form-signin" id="formChangePass"
					action="${pageContext.request.contextPath}/AllianceATES/changePassword"
					method="post">

					<img src="${pageContext.request.contextPath}/images/asi_logo.png"
						style="width: 310px; padding: 10px; margin-bottom: 20px;" />
					<div class="alert alert-danger" style="display: none">
						<strong>Failed!</strong> Passwords do not match.
					</div>
					<div class="form-group">
						<label for="newPass">New Password</label> 
						<input type="password" class="form-control" name="newPass" id="newPass" placeholder="Password" required="" autofocus=""/> 
						
					</div>
					<div class="form-group">
						<label for="confirmPass">Confirm New Password</label> 
						<input type="password" class="form-control" name="confirmPass" id="confirmPass" aria-describedby="confirmPassHelp"  required="" placeholder="Confirm Password"/> 
						<small id="confirmPassHelp" class="form-text text-muted">Never share your password with anyone else.</small>
					</div>
					<button id="btnSave" class="btn btn-lg btn-danger btn-block"
						type="submit">Save</button>
				</form>
			</div>
		</div>

	</div>

	<script
		src="${pageContext.request.contextPath}/lib/js/jquery-1.10.1.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/lib/js/bootstrap.min.js"></script>
	<script>
		$(function(){
			
			$('#formChangePass').submit(function(){
				if($('#newPass').val() != $('#confirmPass').val()){
			    	$('.alert').toggle();
					return false;
				}
				return true;
			});
			
		});
	</script>
	<script
		src="${pageContext.request.contextPath}/lib/js/jquery.blockUI.js"></script>
</body>

</html>