<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form action="${pageContext.request.contextPath }/AllianceATES/form" method="POST">
		<input type="text" name="formID" placeholder="Form ID"><br>
		<select name="formType">
			<option value="Skills Assessment">Skills Assessment</option>
			<option value="Training Needs Analysis">Training Needs Analysis</option>
			<option value="Course Feedback">Course Feedback</option>
			<option value="Training Effectiveness Assessment">Training Effectiveness Assessment</option>
		</select><br>
		<input type="text" name="employeeID" placeholder="Employee ID"><br>
		<input type="submit">
	</form>
</body>
</html>