<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div style="min-height: 881px;">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	<div class="row">
		<div class="col-xs-5 pull-left">
			<h1>${form.formTypeBean.formType } Report</h1>
			<h3>${form.training.title }</h3>
		</div>
	</div>
	<div id="formID" data-id="${form.formID }"></div>
	<section class="content">
	<div class="row">
		<c:forEach items="${questions }" var="q">
			
				<div class="col-xs-12 col-md-9">
					<div class="box box-solid">
						<div class="box-header with-border">
						 	<h2 class="box-title">${q.questionNumber }. ${q.questionString }</h2>
						</div>
						<div class="box-body">
							<canvas id="${q.questionNumber }" width="400" height="260"></canvas>
						</div>
						<div class="overlay">
              				<i class="fa fa-refresh fa-spin"></i>
            			</div>
					</div>
				
				</div>
			
			
		
		</c:forEach>
	</div>
	</section>
	
	</div>
	
	<script>
		$(function(){
			loadCharts();
		});
	</script>
	