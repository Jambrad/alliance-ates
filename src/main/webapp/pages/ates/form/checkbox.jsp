<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div>
	<input type="hidden" value="null" name="result">
	<c:forEach var="choice" items="${choices }">
		<input type="checkbox" data-key="${choice.detailID }">${choice.questionString }
    	<input type="hidden" name="result" value="0" id="bin${choice.detailID }"><br>
	</c:forEach>
</div>
<script>
	$(function(){
		bindCheckboxes();
	});
</script>