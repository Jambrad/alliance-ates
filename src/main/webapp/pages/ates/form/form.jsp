<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${formType }</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/lib/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">



<!-- Theme style -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/lib/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/lib/css/skins/_all-skins.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body
	class="hold-transition skin-red-light sidebar-collapse layout-boxed">
	<!-- Site wrapper -->
	<div class="wrapper">

		<header class="main-header"> <!-- Logo --> <a href="#"
			class="logo"> <!-- mini logo for sidebar mini 50x50 pixels --> <span
			class="logo-lg"><img
				src="${pageContext.request.contextPath}/images/asi_logo_dark.png"
				style="width: 150px" /></span>
		</a> <!-- Header Navbar: style can be found in header.less --> <nav
			class="navbar navbar-static-top"> </nav> </header>

		<!-- =============================================== -->

		<!-- =============================================== -->
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<div id="main-content" style="padding-left:20px; padding-right: 20px; padding-bottom: 60px">
				
				<form method="POST"
					action="${pageContext.request.contextPath }/AllianceATES/form/submit">

					<!-- TITLE -->
					<c:if test="${formType ne 'Training Needs Assessment' }">
						
						<div class="row">
							<div class="col-xs-12">
								<center>
									<h1>${formType } Form</h1>
								</center>
							</div>
							<div class="col-xs-12">
								<center>
									<h3>${training.title }</h3>
								</center>
							</div>
						</div>

					</c:if>
					<!-- new box per category -->
					<c:forEach var="category" items="${category }">
						<div class="row">
							<div class="col-xs-12">

								<div class="box box-danger">

									<div class="box-header with-border">
										<div class="box-title">
											<c:set var="category"
												value="${not empty category?category:'' }"></c:set>
											<c:out value="${category }"></c:out>

										</div>
									</div>

									<div class="box-body">
										<c:forEach var="question" items="${question }">
											<c:if test="${(not empty question.category?question.category:'') eq category }">
												<div class="row" style="padding-bottom: 15px;">
													<div class="col-xs-5">
														<strong>${question.questionNumber }. ${question.questionString }</strong>
													</div>
													<c:choose>
														<c:when test="${question.questionType eq 'Check' || question.questionType eq 'Essay'}">
															<div class="col-xs-8">
																<div class="row">
																	<div class="${question.questionType } col-xs-12"></div>
																</div>
															</div>
														</c:when>
														<c:otherwise>
															<div class="col-xs-7">
																<div class="${question.questionType }"></div>
															</div>
														</c:otherwise>
													</c:choose>
													
												</div>
											</c:if>
										</c:forEach>
									</div>
								</div>
							</div>
						</div>
					</c:forEach>
					<input type="hidden" name="pendingID" value="${pendingID}">
					<input type="hidden" name="formID" value="${formID}"> <input
						type="hidden" name="employeeID" value="${employeeID }">
					<div class="row">
						<div class="col-xs-12"><center>
							<input type="submit" class="btn btn-danger btn-lg" value="Submit">
							<input type="reset"	class="btn btn-default btn-lg" value="Clear">
							</center>
						</div>
					
					</div>
					
				
				</form>
				<!-- /.content -->
			</div>
		</div>
		<!-- /.content-wrapper -->


	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.2.3 -->
	<script
		src="${pageContext.request.contextPath}/lib/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script
		src="${pageContext.request.contextPath}/lib/js/bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script
		src="${pageContext.request.contextPath}/lib/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script
		src="${pageContext.request.contextPath}/lib/plugins/fastclick/fastclick.js"></script>

	<!-- AdminLTE App -->
	<script src="${pageContext.request.contextPath}/js/blank.min.js"></script>

	<!-- admin js -->
	<script src="${pageContext.request.contextPath}/js/form.js"></script>
</body>
</html>


