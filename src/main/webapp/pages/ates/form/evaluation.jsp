<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${formType.formType } Form</title>
</head>
<body>
	<h1>${formType.formType }</h1>
		<form method="POST" action="${pageContext.request.contextPath }/ates/form/evaluation-done">
		<table>
			<c:if test="${formType.formType ne 'Training Needs Assessment' }">
				<tr>
					<td colspan=2>
						<fieldset>
							<legend>${training.title }</legend>
							<p>${training.objective }</p>
						</fieldset>
					</td>
				</tr>
			</c:if>
			<c:forEach var="category" items="${category }">
				<tr>
					<td colspan=2><hr></td>
				</tr>
				<tr>
					<th colspan=2>
						<c:set var="category" value="${not empty category?category:'' }"></c:set>
						<c:out value="${category }"></c:out>
					</th>
				</tr>
				<c:forEach var="question" items="${question }">
					<c:if test="${(not empty question.category?question.category:'') eq category }">
						<tr>
							<td>${question.questionNumber }. ${question.questionString }</td>
							<td class="${question.questionType }"></td>
						</tr>
					</c:if>
				</c:forEach>
			</c:forEach>
		</table>
		<input type="hidden" name="formID" value="${formID}">
		<input type="hidden" name="participant" value="${participant }">
		<input type="submit" value="Submit"><input type="reset" value="Clear">
		</form>
	<script src="${pageContext.request.contextPath}/lib/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/choices.js"></script>
</body>
</html>