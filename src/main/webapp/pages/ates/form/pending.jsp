<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div style="min-height: 881px;">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	<div class="row">
		<div class="col-xs-5 pull-left">
			<h1>Pending Forms</h1>
		</div>
	</div>
	<section class="content">
	
	<c:choose>
		<c:when test="${not empty pending}">	
			
			<c:forEach items="${pending}" var="Form">
			<form action="${pageContext.request.contextPath }/AllianceATES/form" method="post">
			<div class ="row">
				<div class="col-md-8 col-xs-12">
					<div class="box box-solid">
						<div class="box-header with-border">
							<h3 class="box-title"><b>${Form.form.training.title}</b></h3>
							<c:choose>
								<c:when test="${Form.form.formTypeBean.formType eq 'Course Feedback'}">
									<div style="display:inline">
									 	<h4 style="display:inline"><span class="pull-right"><i class="fa fa-circle-o text-red" style="margin-right: 15px;"></i>Course Feedback</span></h4>		
  									</div>
								</c:when>
								<c:when test="${Form.form.formTypeBean.formType eq 'Skills Assessment'}">
									<div style="display:inline">
									 	<h4 style="display:inline"><span class="pull-right"><i class="fa fa-circle-o text-yellow" style="margin-right: 15px;"></i>Skills Assessment</span></h4>		
  									</div>
								</c:when>
								<c:when test="${Form.form.formTypeBean.formType eq 'Training Effectiveness Assessment'}">
									<div style="display:inline">
										<h4 style="display:inline"><span class="pull-right"><i class="fa fa-circle-o text-red" style="margin-right: 15px;"></i>Training Effectiveness</span></h4>		
  									</div>
								</c:when>
							</c:choose>
							
						</div>
						<div class="box-body">
							<div id="objective">
							<div>Training ID : ${Form.form.training.trainingID} </div>
							<div><i class="fa fa-calendar" style="margin-right: 15px"></i><span>Start Date	 : ${Form.form.training.startDate} </span></div>
							<div><i class="fa fa-calendar" style="margin-right: 15px"></i><span>End Date	 : ${Form.form.training.endDate} </span></div>
							<div>Facilitator : ${Form.form.training.employee.lastName}, ${Form.form.training.employee.firstName}</div>
							<c:if test="${not empty Form.participation}">
							<div><b>To Evaluate   :</b> <u>${Form.participation.employee.lastName},${Form.participation.employee.firstName}</u></div>
							</c:if>
							</div>
						</div>
						<div class="box-footer">
						<input type="hidden" name="pendingID" value="${Form.pendingID}"></input>
						<input type="hidden" name="employeeID" value="${Form.employee.employeeID}"></input>
						<input type="hidden" name="formID" value="${Form.form.formID}"></input>
							<button type="submit" id="btnAnswerForm" class="btn btn-danger pull-right"  name="Fill up">Fill Up</button>
					
						</div>
						<!-- /.box-body -->
					</div>
				</div>
	
			</div>
			</form>
			</c:forEach>
	
		</c:when>
		<c:otherwise>
		
		<div class="row">
		<div class="col-xs-5">
			<h1 style="color:#b2acab">Empty</h1>
		</div>
		</div>
		</c:otherwise>
	</c:choose>


	</section>
	
	</div>
	
	<script>
	</script>
	