<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div>

		<form action="${pageContext.request.contextPath}/ates/add" method="POST">
			First Name: <input type="text" name="firstName"/><br/>
			Last Name : <input type="text" name="lastName"/><br/>
			Email Address : <input type="text" name="email"/><br/>
			Contact number : <input type="number" name="contactNumber"/><br/>
			Address : <input type="text" name="address"/><br/>
			Gender: 
			<select name="gender">
				<option value="Male">Male</option>	
				<option value="Female">Female</option>			
			</select><br/>
			Type: 
			<select name="type">
				<option value="Student">Student</option>	
				<option value="Professional">Professional</option>		
				<option value="Employee">Employee</option>	
			</select><br/>
			Password : <input type="text" name="password"/><br/>
			<button type="submit">Add</button>
		</form>
	
</div>
