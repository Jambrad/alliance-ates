<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div>

		<form action="${pageContext.request.contextPath}/ates/trainingplan/addtraining" method="POST">
				<input type="hidden" name="tpID" value="${id }">
			Title: <input type="text" name="title"/><br/>
			Objective : <input type="text" name="objective"/><br/>
			Business Unit : <input type="text" name="businessUnit"/><br/>
			No of pax : <input type="number" name="noOfPax"/><br/>
			
			Type: 
			<select name="type">
				<option value="Int">Int</option>	
				<option value="Ext">Ext</option>			
				<option value="Cert">Cert</option>		
			</select><br/>
			
			Start Date:
  			<input type="date" name="startDate">
  			
  			End Date:
  			<input type="date" name="endDate">
  			
			<button type="submit">Add</button>
		</form>
	
</div>
