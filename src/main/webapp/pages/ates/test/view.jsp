<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div>

			<form action="${pageContext.request.contextPath}/ates/view" method="GET">
				<input type="text" name="name" placeholder="Search">
				<button type="submit">Search</button>
			</form>
								
			<table>
				<tr>
					<td>ID</td>
					<td>Name</td>
					<td>Address</td>
					<td>Contact Number</td>
					<td>Job Title</td>
				</tr>

				<c:forEach items="${employee}" var="item">
					<tr>
						<td>${item.employeeID}</td>
						<td>${item.firstName} ${item.middleName} ${item.lastName}</td>
						<td>${item.address}</td>
						<td>${item.contactNumber}</td>
						<td>${item.jobTitle}</td>
					
					</tr>
					<br>
				</c:forEach>
			</table>

			
</div>
