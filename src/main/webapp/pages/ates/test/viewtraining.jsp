<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div>
			<table>
				<tr>
					<td>ID</td>
					<td>Training Plan ID</td>
					<td>Title</td>
					<td>Objective</td>
					<td>Business Unit</td>
					<td>Type</td>
					<td>No of pax</td>
					<td>Start Date</td>
					<td>End date</td>
					
				</tr>

				<c:forEach items="${training}" var="item">
					<tr>
						<td>${item.trainingID}</td>
						<td>${item.trainingPlan.trainingPlanID}</td>
						<td>${item.title}</td>
						<td>${item.objective}</td>
						<td>${item.businessUnit}</td>
						<td>${item.type}</td>
						<td>${item.noOfPax}</td>
						<td>${item.startDate}</td>
						<td>${item.endDate}</td>
						
						<td>
						<form action="${pageContext.request.contextPath}/ates/trainingplan/deletetraining" method="POST">
						<button type="submit" name="trainingID" value="${item.trainingID}">Delete</button>
						</form>
						
						<td>
						<form action="${pageContext.request.contextPath}/ates/trainingplan/edittraining" method="GET">
						<button type="submit" name="trainingID" value="${item.trainingID}">Edit</button>
						</form>
						
						</td>
					</tr>
					<br>
				</c:forEach>
			</table>


<br><br>

		<form action="${pageContext.request.contextPath}/ates/trainingplan/addtraining" method="GET">
				<button type="submit" name="tpID" value="${trainPID.trainingPlanID}">Add</button>
		</form>
			
</div>
