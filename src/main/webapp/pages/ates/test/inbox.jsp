<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div>

			<form action="${pageContext.request.contextPath}/ates/view" method="GET">
				<input type="text" name="name" placeholder="Search">
				<button type="submit">Search</button>
			</form>
			
	Employee : ${inbox[0].employee.firstName} ${inbox[0].employee.middleName} ${inbox[0].employee.lastName}

			<table>
				<tr>
					<td>Training Name</td>
					<td>Form</td>
					<td>Form Type</td>
				</tr>

				<c:forEach items="${inbox}" var="item">
					<tr>
						<td>${item.form.training.title}</td>
						<td>${item.form.formTypeBean.formType }</td>
						<td>${item.form.formFor }</td>
			
					</tr>
					<br>
				</c:forEach>
			</table>

			
</div>
