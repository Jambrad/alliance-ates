<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div>

Training: ${attendance[0].participation.training.title}

			<table>
				<tr>
					<td>Date</td>
					<td>Employee Name</td>
					<td>AM</td>
					<td>PM</td>
					
				</tr>

				<c:forEach items="${attendance}" var="item">
					<tr>
						
						<td>${item.id.date}</td>
						<td>${item.participation.employee.firstName} ${item.participation.employee.lastName}
						<td>${item.amAttendance}</td>
						<td>${item.pmAttendance}</td>

					</tr>
					<br>
				</c:forEach>
			</table>
</div>
