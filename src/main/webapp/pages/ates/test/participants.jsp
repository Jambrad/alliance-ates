<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div>

Training: ${participants[0].training.title}

			<table>
				<tr>
					<td>Employee ID</td>
					<td>Training ID</td>
					
				</tr>

				<c:forEach items="${participants}" var="item">
					<tr>
						<td>${item.employee.firstName} ${item.employee.lastName} </td>
						<td>${item.training.trainingID}</td>
					</tr>
					<br>
				</c:forEach>
			</table>
</div>
