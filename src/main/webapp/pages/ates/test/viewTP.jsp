<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div>					

	<p>Annual Training</p>
				
			
		<form action="${pageContext.request.contextPath}/ates/trainingplan/trainings" method="GET">
				<c:forEach items="${tp}" var="item">
					<button type="submit" name="trainingPlanID" value="${item.trainingPlanID }">${item.year}</button>
				</c:forEach>
		</form>
			
		<form action="${pageContext.request.contextPath}/ates/trainingplan/addtrainingplan" method="GET">
				<button type="submit">Add</button>
		</form>

			
</div>
