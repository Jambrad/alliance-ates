<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div>

		<form action="${pageContext.request.contextPath}/ates/trainingplan/edittraining" method="POST">
			Title: <input type="text" name="title" value="${training.title}"/><br/>
			Objective : <input type="text" name="objective" value="${training.objective}"/><br/>
			Business Unit : <input type="text" name="businessUnit" value="${training.businessUnit}"/><br/>
			No of pax : <input type="number" name="noOfPax" value="${training.noOfPax}"/><br/>
			
			Type: 
			<select name="type">
				<option value="Int">Int</option>	
				<option value="Ext">Ext</option>			
				<option value="Cert">Cert</option>		
			</select><br/>
			
			<input type="hidden" name="tpID" value="${training.trainingPlan.trainingPlanID}">
			<input type="hidden" name="trainingID" value="${training.trainingID }">
		
			<button type="submit">Edit</button>
		</form>
	
</div>
