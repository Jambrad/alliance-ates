<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div style="min-height: 881px;">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	<h1>Attendance</h1>

	</section>

	<section class="content">
	<div class="row" style="margin-bottom: 10px">
		<div class="col-md-12">
			<select id="trainingSelect" class="select2 col-md-10">

			</select>
			<button class="btn btn-default" id="selectTraining">Select</button>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<div class="box box-danger">
				<div class="box-header with-border">
					<h1 class="box-title">Dates</h1>
				</div>
				<div class="box-body">
					<ul class="sidebar-menu" id="dateDiv">

						<c:forEach items="${dates }" var="date">
							<li><a href="#" data-date="${date }">${date }</a></li>
						</c:forEach>

					</ul>
				</div>
				<div class="box-footer">
					<button class="btn btn-danger pull-right" id="addAttendance">Add Attendance</button>
				</div>
			</div>
		</div>


		<div class="col-md-8">

			<div class="box box-danger">
				<div class="box-header with-border">
					<h1 class="box-title">Attendance</h1>
				</div>
				<form>
				<div class="box-body">
					<table class="table table-bordered">
						<thead>
							<th><strong>ID</strong></th>

							<th><strong>Participant</strong></th>
							<th><strong>AM</strong></th>
							<th><strong>PM</strong></th>
						</thead>
						<tbody id="attendanceDiv">
							<c:forEach items="${attendanceList }" var="attendance">
								<tr>
									<td><input type="hidden" name="participationID"
										value="${attendance.participation.participantID }">${attendance.participation.employee.employeeID }</td>
									<td>${attendance.participation.employee.firstName }
										${attendance.participation.employee.lastName }</td>
									<td><select name="amAttendance">
											<c:choose>
												<c:when test="${attendance.amAttendance eq 'N/A' }">
													<option value="N/A" selected>N/A</option>
													<option value="Present">Present</option>
													<option value="Tardy">Tardy</option>
													<option value="Absent">Absent</option>
												</c:when>
												<c:when test="${attendance.amAttendance eq 'Present' }">
													<option value="N/A">N/A</option>
													<option value="Present" selected>Present</option>
													<option value="Tardy">Tardy</option>
													<option value="Absent">Absent</option>
												</c:when>
												<c:when test="${attendance.amAttendance eq 'Tardy' }">
													<option value="N/A">N/A</option>
													<option value="Present">Present</option>
													<option value="Tardy" selected>Tardy</option>
													<option value="Absent">Absent</option>
												</c:when>
												<c:when test="${attendance.amAttendance eq 'Absent' }">
													<option value="N/A">N/A</option>
													<option value="Present">Present</option>
													<option value="Tardy">Tardy</option>
													<option value="Absent" selected>Absent</option>
												</c:when>
												<c:otherwise>
													<option value="N/A" selected>N/A</option>
													<option value="Present">Present</option>
													<option value="Tardy">Tardy</option>
													<option value="Absent">Absent</option>
												</c:otherwise>
											</c:choose>

									</select></td>
									<td><select name="pmAttendance">
											<c:choose>
												<c:when test="${attendance.pmAttendance eq 'N/A' }">
													<option value="N/A" selected>N/A</option>
													<option value="Present">Present</option>
													<option value="Tardy">Tardy</option>
													<option value="Absent">Absent</option>
												</c:when>
												<c:when test="${attendance.pmAttendance eq 'Present' }">
													<option value="N/A">N/A</option>
													<option value="Present" selected>Present</option>
													<option value="Tardy">Tardy</option>
													<option value="Absent">Absent</option>
												</c:when>
												<c:when test="${attendance.pmAttendance eq 'Tardy' }">
													<option value="N/A">N/A</option>
													<option value="Present">Present</option>
													<option value="Tardy" selected>Tardy</option>
													<option value="Absent">Absent</option>
												</c:when>
												<c:when test="${attendance.pmAttendance eq 'Absent' }">
													<option value="N/A">N/A</option>
													<option value="Present">Present</option>
													<option value="Tardy">Tardy</option>
													<option value="Absent" selected>Absent</option>
												</c:when>
												<c:otherwise>
													<option value="N/A" selected>N/A</option>
													<option value="Present">Present</option>
													<option value="Tardy">Tardy</option>
													<option value="Absent">Absent</option>
												</c:otherwise>
											</c:choose>
									</select></td>
								</tr>
							</c:forEach>

						</tbody>
					</table>
				</div>

				<div class="box-footer">
					<button type="button" class="btn btn-danger pull-right" id="saveAttendance">Save</button>
				</div>
				</form>

			</div>

		</div>

	</div>

	</section>
	<div class="modal modal-danger fade" tabindex="-1" role="dialog" id="saveSuccessModal">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Success!</h4>
				</div>
				<div class="modal-body">
					<p>Attendance saved!</p>
				</div>
				
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	
	<div class="modal modal fade" tabindex="-1" role="dialog" id="addAttendanceModal">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Pick a date</h4>
				</div>
				<div class="modal-body">
					<select class="select col-xs-12" id="dateSelect">
						
					</select>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger pull-right" id="add" data-dismiss="modal">Add</button>
				</div>
				
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
</div>
<script>
	$(function() {
		bindAttendance();
	});
</script>
