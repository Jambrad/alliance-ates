<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div>
	<section class="content-header">
	<h1 data-trainingid = "${training.trainingID}">Edit Training</h1>

	</section>


	<section class="content">
	<form >
		<input type="hidden" name="trainingID" value="${training.trainingID }"/>
		<div class="row">
			<!--First Box-->
			<div class="col-md-12">
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title">Basic Details</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<!--Left divide-->
							<div class="col-xs-6">

								<!--Title of Training-->
								<div class="form-group">
									<label for="title">Title*</label> <input type="text"
										class="form-control" name="title" id="title"
										placeholder="Enter title of training" value="${training.title }">
								</div>

								<!-- Type and BU row -->
								<div class="row">
									<div class="form-group col-xs-6">
										<label>Type of Training</label> <select name="type" id="type"
											class="form-control">
											<option value="Int">Internal Training</option>
											<option value="Ext">External Training</option>
											<option value="Cert">Certification Training</option>
										</select>
									</div>
									<div class="form-group col-xs-6">
										<label>Business Unit</label> <select name="businessUnit"
											class="form-control" id="businessUnit" value="${training.businessUnit }">
											<option value="ASJ">ASJ</option>
											<option value="JOC">JOC</option>
											<option value="AEES">AEES</option>
											<option value="BSG">BSG</option>
										</select>
									</div>
								</div>

								<!--T# of pax -->
								<div class="form-group">
									<label for="noOfPax"># of Pax</label> <input type="number"
										class="form-control" id="noOfPax" name="noOfPax"
										placeholder="Number of Pax" value="${training.noOfPax }">
								</div>

								<!--Dates-->
								<div class="row">

									<!-- Start date  -->
									<div class="form-group col-xs-6">
										<label>Start Date:</label>

										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" name="startDate" id="startDate"
												class="form-control pull-right datepicker" value="${training.startDate } ">
										</div>
									</div>

									<!-- End date  -->
									<div class="form-group col-xs-6">
										<label>End Date:</label>

										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" name="endDate" id="endDate"
												class="form-control pull-right datepicker" value="${training.endDate }">
										</div>
									</div>
								</div>

							</div>
							<!--Right divide-->

							<div class="form-group col-xs-6">
								<label>Remarks</label>
								<textarea id="remarks" name="remarks"
									class="form-control col-xs-6" rows="3" placeholder="Enter ..."
									style="margin: 0px -0.5px 0px 0px; height: 190px;">${training.remarks }</textarea>
							</div>



						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!--/col-->
		</div>
		<!--row-->

		<!--Course Objective and Outline -->
		<div class="row">
			<div class="col-md-8 col-xs-12">
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title">Course Objective and Outline</h3>
					</div>
					<div class="box-body">
						<textarea id="objective" name="objective" class="col-xs-12" value="${training.objective }"></textarea>
					</div>
					<!-- /.box-body -->
				</div>
			</div>
			<!--FACILITATOR -->
			<div class="col-md-4 col-xs-12">
				<!--Faci Box -->
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title">Facilitator</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-xs-12">
								<label>Select Facilitator</label> <select name="facilitator"
									id="facilitatorSelect" class="form-control select2"
									style="width: 100%;">

								</select>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!--/col-->
		</div>


		<div class="row">


			<!-- PARTICIPANTS -->
			<div class="col-md-12">
				<!--Faci Box -->
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title">Participants</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-xs-12">
								<label>Search and Add Participants</label>
								<div class="row">

									<div class="col-xs-10">
										<select id="participantSelect" class="form-control select2"
											style="width: 100%;">

										</select>
									</div>
									<div class="col-xs-1">
										<button type="button" id="addParticipant"
											class="btn btn-danger">Add</button>
									</div>

								</div>
							</div>


							<div class="col-xs-12">
								<table class="table table-striped">
									<tbody>
										<tr>
											<th>Emp ID</th>
											<th>Employee Name</th>
											<th>Employer</th>
											<th>Job Title</th>
											<th>Business Group</th>
											<th style="width: 40px"></th>
										</tr>
										<tr>
											<!-- dynamic insertion of options -->
										</tr>

									</tbody>
								</table>
							</div>

						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>


		<button id="save" type="button" class="btn btn-danger"
			data-tpid="${tpID}">Save</button>
		<button id="cancel" type="button" class="btn btn-default">Cancel</button>
	</form>
	</section>
	
	<div data-status="${trainingStatus }" id="status"></div>
	
	<div class="modal modal-success fade" tabindex="-1" role="dialog" id="addSuccessModal">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Success!</h4>
				</div>
				<div class="modal-body">
					<p>Training added successfully...</p>
				</div>
				
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
</div>
<script>

	$(function() {
		bindEditTrainingForm();
	});
	
</script>
