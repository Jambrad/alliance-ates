<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div>
	<section class="content-header">
	<h1>Training Plans</h1>
	</section>

	<section class="content"> <!-- Training Boxes --> <c:forEach
		items="${tp}" var="training">
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-red">
				<div class="inner">
					<h3>${training.year}</h3>

					<p>Annual Training Plan</p>
				</div>
				<div class="icon">
					<i class="ion ion-calendar"></i>
				</div>
				<a href="#" class="small-box-footer moreTP"
					data-training="${training.trainingPlanID }"> More info <i
					class="fa fa-arrow-circle-right"></i>
				</a>
			</div>
		</div>
	</c:forEach>

	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-blue">
			<div class="inner">
				<h3>Add</h3>

				<p>New Annual Training Plan</p>
			</div>
			<div class="icon">
				<i class="ion ion-plus"></i>
			</div>
			<a id="newTP" href="#" class="small-box-footer"> Add <i
				class="fa fa-plus-circle"></i>
			</a>
		</div>
	</div>
	<!-- /.row --> </section>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Add New Training Plan</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left"
					data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger" id="btnAddTP">Confirm</button>
				
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<script>
	$(function() {
		bindTrainingPlansIndex();
	});
</script>
