<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div style="min-height: 881px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 data-tpid = "${tp.trainingPlanID }">
        ${tp.year} Annual Training Plan
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-9">
          <div class="box box-danger">
            <div class="box-body no-padding">
              <!-- THE CALENDAR -->
              <div id="calendar" class="fc fc-ltr fc-unthemed">
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3">
        	<div class="box box-danger">
        		<div class="box-header">
              		<i class="fa fa-tasks"></i>
              		<h3 class="box-title">Actions</h3>
            	</div>
            	<div class="box-body">
            		<button type="button" id="addNewTraining" class="btn btn-danger btn-block" data-tpid = "${tp.trainingPlanID}">Add New Training</button>
            		<button type="button" id="viewAllTraining" class="btn btn-default btn-block" data-tpid = "${tp.trainingPlanID}">View All Training</button>
            	</div>
        	</div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    
    <div class="modal" role="dialog" id="trainingModal">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Default Modal</h4>
              </div>
              <div class="modal-body">
                <span><i class="fa fa-calendar-o"></i></span><strong>&nbsp;Start Date:</strong>
                <p id="pstartDate"> MAy asdasdsad </p>
                <span><i class="fa fa-calendar-o"></i></span><strong>&nbsp;End Date:</strong>
                <p id="pendDate"> MAy ssss </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" id="delete" data-dismiss="modal">Delete</button>
                <button type="button" class="btn btn-default" id="edit" data-dismiss="modal">Edit</button>
                <button type="button" class="btn btn-danger" id="viewDetails" data-dismiss="modal">View Details</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
    </div>
    
    <div id="deleteStatus" data-status="${deleteStatus }"></div>
    <div class="modal modal-danger fade" tabindex="-1" role="dialog" id="deleteSuccessModal">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Success!</h4>
				</div>
				<div class="modal-body">
					<p>Training deleted successfully...</p>
				</div>
				
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
    <!-- /.content -->
</div>
<script>
	$(function(){
		loadAndBindCalendar();
	});
	
</script>
