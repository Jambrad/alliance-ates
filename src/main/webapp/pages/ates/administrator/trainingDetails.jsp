<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div style="min-height: 881px;">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	<div class="row">
		<div class="col-xs-5 pull-left">
			<h1>${training.title }</h1>
		</div>
		<div id="trainingID" data-id="${training.trainingID }"></div>
		<div class="col-xs-7" style="margin-top: 26px">
			<button class="btn btn-danger pull-right" id="editTraining" style="width:100px"><i class="fa fa-pencil pull-left" aria-hidden="true"><span></i>Edit</span></button>
		</div>
		
		
	</div>
		
	</section>


	<section class="content">

	<div class="row">
		<!-- Details Box-->
		<div class="col-md-6 col-xs-12">
			<div class="box box-danger">
				<div class="box-header with-border">
					<h3 class="box-title">Training Details</h3>
					
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-xs-5">
							<strong>Title:</strong>
						</div>
						<div class="col-xs-7">${training.title }</div>
					</div>
					<div class="row">
						<div class="col-xs-5">
							<strong>Type:</strong>
						</div>
						<div class="col-xs-7">${training.type}</div>
					</div>
					<div class="row">
						<div class="col-xs-5">
							<strong>Business Unit:</strong>
						</div>
						<div class="col-xs-7">${training.businessUnit }</div>
					</div>
					<div class="row">
						<div class="col-xs-5">
							<strong>No of Pax:</strong>
						</div>
						<div class="col-xs-7">${training.noOfPax }</div>
					</div>
					<div class="row">
						<div class="col-xs-5">
							<strong>Start Date:</strong>
						</div>
						<div class="col-xs-7">${training.startDate }</div>
					</div>
					<div class="row">
						<div class="col-xs-5">
							<strong>End Date:</strong>
						</div>
						<div class="col-xs-7">${training.endDate }</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!--/col-->


		<!--Faci Box -->
		<div class="col-md-6 col-xs-12">
			<div class="box box-danger">
				<div class="box-header with-border">
					<h3 class="box-title">Facilitator</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-xs-4">
							<strong>Employee ID: </strong>
						</div>
						<div class="col-xs-8">${training.employee.employeeID }</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<strong>Name: </strong>
						</div>
						<div class="col-xs-8">${training.employee.firstName } ${training.employee.lastName }</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<strong>Employer: </strong>
						</div>
						<div class="col-xs-8">${training.employee.employer }</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<strong>Job Title: </strong>
						</div>
						<div class="col-xs-8">${training.employee.jobTitle }</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<strong>Business Group: </strong>
						</div>
						<div class="col-xs-8">${training.employee.businessGroup }</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		
		
	</div>
	<!--row--> <!-- Course Objective and Outline and Forms -->
	<div class="row">
		<!-- Forms -->
		<div class="col-md-6 col-xs-12">
			<div class="box box-danger">
				<div class="box-header with-border">
					<h3 class="box-title">Forms</h3>
				</div>
				<div class="box-body">
					
					<div class="row">
						<div class="col-xs-12">
							<center><strong>Course Feedback Form</strong></center>
						</div>
						
					</div>
					<button class="btn btn-default btn-block" id="releaseCF" data-type="Course Feedback">Release to Participants</button>
					<button class="btn btn-danger btn-block" id="generateCF" data-type="Course Feedback" style="display:none">Generate Report</button>
					
					<div class="row" id="skills" style="margin-top: 15px;">
						<div class="col-xs-12">
							<center><strong>Skills Assessment Form</strong></center>
						</div>
						
					</div>
					<button class="btn btn-default btn-block" id="releaseParticipantSA" data-type="Skills Assessment">Release to Participants</button>
					<button class="btn btn-default btn-block" id="releaseSupervisorSA" data-type="Skills Assessment" style="display:none">Release to Supervisors</button>
					
					<div class="row">
						<div class="col-xs-12" style="margin-top: 15px;">
							<center><strong>Training Effectiveness Form</strong></center>
						</div>
					</div>
					<button class="btn btn-default btn-block" id="releaseTE" data-type="Training Effectiveness">Release to Supervisors</button>
					<button class="btn btn-danger btn-block" id="generateTE" data-type="Training Effectiveness" style="display:none">Generate Report</button>
					</div>
				</div>
			</div>
		
		
		<!-- Remarks -->
		<div class="col-md-6 col-xs-12">
			<div class="box box-danger">
				<div class="box-header with-border">
					<h3 class="box-title">Remarks</h3>
				</div>
				<div class="box-body">
					<p>${training.remarks }</p>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		
		<!-- Course Objective and Outline -->
		<div class="col-md-12 col-xs-12">
			<div class="box box-danger">
				<div class="box-header with-border">
					<h3 class="box-title">Course Objective and Outline</h3>
				</div>
				<div class="box-body">
					<div id="objective">
						${training.objective}
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		
		
		
		<!-- PARTICIPANTS -->
		<div class="col-md-12 col-xs-12">
			<!--Faci Box -->
			<div class="box box-danger">
				<div class="box-header with-border">
					<h3 class="box-title">Participants</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-xs-12">
							<table class="table table-striped">
								<tbody>
									<tr>
										<th>Emp ID</th>
										<th>Employee Name</th>
										<th>Employer</th>
										<th>Job Title</th>
										<th>Business Group</th>
									</tr>
									<c:forEach items="${participants }" var="p">
										<tr>
											<td>${p.employee.employeeID}</td>
											<td>${p.employee.firstName} ${p.employee.lastName}</td>
											<td>${p.employee.employer }</td>
											<td>${p.employee.jobTitle }</td>
											<td>${p.employee.businessGroup }</td>
										</tr>
									</c:forEach>
									

								</tbody>
							</table>
						</div>

					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	
		
		
		</div>
		
		
	</div>
	
	</section>
	
	<div class="modal modal-success fade" tabindex="-1" role="dialog" id="releaseSuccessModal">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Success!</h4>
				</div>
				<div class="modal-body">
					<p>Release Success!</p>
				</div>
				
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	
	<div class="modal modal-danger fade" tabindex="-1" role="dialog" id="releaseErrorModal">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Oops!</h4>
				</div>
				<div class="modal-body">
					<p>Some error occurred during the release... </p>
				</div>
				
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.content -->
</div>
<script>
	
	$(function() {
		bindTrainingDetails();
	});
</script>
