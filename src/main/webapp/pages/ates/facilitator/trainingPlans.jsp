<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div>
	<section class="content-header">
	<h1>Training Plans</h1>
	</section>

	<section class="content"> <!-- Training Boxes --> <c:forEach
		items="${tp}" var="training">
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-red">
				<div class="inner">
					<h3>${training.year}</h3>

					<p>Annual Training Plan</p>
				</div>
				<div class="icon">
					<i class="ion ion-calendar"></i>
				</div>
				<a href="#" class="small-box-footer moreTP"
					data-training="${training.trainingPlanID }"> More info <i
					class="fa fa-arrow-circle-right"></i>
				</a>
			</div>
		</div>
	</c:forEach>

	
	<!-- /.row --> </section>
</div>

<script>
	$(function() {
		bindTrainingPlansIndex();
	});
</script>
