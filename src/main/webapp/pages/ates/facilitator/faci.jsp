<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>ATES - Facilitator</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/lib/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- fullCalendar 2.2.5-->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/lib/plugins/fullcalendar/fullcalendar.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/lib/plugins/fullcalendar/fullcalendar.print.css"
	media="print">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/lib/plugins/datepicker/datepicker3.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/lib/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- Select2 -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/lib/plugins/select2/select2.min.css">

<!-- Theme style -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/lib/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/lib/css/skins/_all-skins.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-black sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><img
					src="${pageContext.request.contextPath}/images/asi_logo_min.png"
					style="width: 40px;" /></span> <!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><img
					src="${pageContext.request.contextPath}/images/asi_logo.png"
					style="width: 150px" /></span>
			</a>
			<!-- Header Navbar: style can be found in head
			er.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<div id="empID" data-id="${emp.employeeID }"></div>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">

						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu"><a href="#"
							class="dropdown-toggle" data-toggle="dropdown"> <img
								src="${pageContext.request.contextPath}/images/admin.png"
								class="user-image" alt="User Image"> <span
								class="hidden-xs">${emp.firstName} ${emp.lastName} </span>
						</a>
							<ul class="dropdown-menu">
								<!-- Menu Body -->
								<!-- User image -->
								<li class="user-header"><img
									src="${pageContext.request.contextPath}/images/admin.png"
									class="img-circle" alt="User Image">

									<p>
										${emp.firstName} ${emp.lastName} <small>${emp.jobTitle}</small>
									</p></li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a
											href="${pageContext.request.contextPath}/AllianceATES/changePassword "
											class="btn btn-default btn-flat">Change Pass</a>
									</div>
									<div class="pull-right">
										<a
											href="${pageContext.request.contextPath}/AllianceATES/logout"
											class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul></li>


					</ul>
				</div>
			</nav>
		</header>

		<!-- =============================================== -->

		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar" style="height: auto;">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="/SoaBaseCode/images/admin.png" class="img-circle"
							alt="User Image">
					</div>
					<div class="pull-left info" style="margin-top: 10px;">
						<p>${emp.firstName} ${emp.lastName}</p>

					</div>
				</div>

				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>

					<li class="active"><a href="#" id="faciTrainingPlans"> <i
							class="fa fa-calendar"></i> <span>Training Plans</span>
					</a></li>

					<li><a href="#" id="faciTrainings"> <i class="fa fa-tasks"></i>
							<span>Trainings</span> 
					</a></li>

					<li><a href="#" id="faciPending"> <i class="fa fa-inbox"></i>
							<span>Pending</span>
							<c:if test="${pendingCount > 0}">
								<span class="pull-right-container">
									<span class="label label-danger pull-right">${pendingCount }</span>
								</span>
							</c:if>
					</a></li>

				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- =============================================== -->
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<div id="main-content">

				<!-- /.content -->
			</div>
		</div>
		<!-- /.content-wrapper -->

		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017-2018 <a
				href="http://almsaeedstudio.com">Alliance Software Inc.</a>.
			</strong> All rights reserved.
		</footer>


	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.2.3 -->
	<script
		src="${pageContext.request.contextPath}/lib/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script
		src="${pageContext.request.contextPath}/lib/js/bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script
		src="${pageContext.request.contextPath}/lib/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script
		src="${pageContext.request.contextPath}/lib/plugins/fastclick/fastclick.js"></script>

	<!-- AdminLTE App -->
	<script src="${pageContext.request.contextPath}/js/blank.min.js"></script>

	<!--fullCalendar 2.2.5 -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/lib/plugins/fullcalendar/fullcalendar.min.js"></script>
	<!-- Select2 -->
	<script
		src="${pageContext.request.contextPath}/lib/plugins/select2/select2.full.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/lib/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/lib/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- facilitator js -->
	<script
		src="${pageContext.request.contextPath}/js/facilitator.index.js"></script>
</body>
</html>


