<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div style="min-height: 881px;">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	<div class="row">
		<div class="col-xs-5 pull-left">
			<h1>${training.title }</h1>
		</div>
		<div id="trainingID" data-id="${training.trainingID }"></div>



	</div>

	</section>


	<section class="content">

	<div class="row">
		<!-- Details Box-->
		<div class="col-md-6 col-xs-12">
			<div class="box box-danger">
				<div class="box-header with-border">
					<h3 class="box-title">Training Details</h3>

				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-xs-5">
							<strong>Title:</strong>
						</div>
						<div class="col-xs-7">${training.title }</div>
					</div>
					<div class="row">
						<div class="col-xs-5">
							<strong>Type:</strong>
						</div>
						<div class="col-xs-7">${training.type}</div>
					</div>
					<div class="row">
						<div class="col-xs-5">
							<strong>Business Unit:</strong>
						</div>
						<div class="col-xs-7">${training.businessUnit }</div>
					</div>
					<div class="row">
						<div class="col-xs-5">
							<strong>No of Pax:</strong>
						</div>
						<div class="col-xs-7">${training.noOfPax }</div>
					</div>
					<div class="row">
						<div class="col-xs-5">
							<strong>Start Date:</strong>
						</div>
						<div class="col-xs-7">${training.startDate }</div>
					</div>
					<div class="row">
						<div class="col-xs-5">
							<strong>End Date:</strong>
						</div>
						<div class="col-xs-7">${training.endDate }</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!--/col-->


		<!--Faci Box -->
		<div class="col-md-6 col-xs-12">
			<div class="box box-danger">
				<div class="box-header with-border">
					<h3 class="box-title">Facilitator</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-xs-4">
							<strong>Employee ID: </strong>
						</div>
						<div class="col-xs-8">${training.employee.employeeID }</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<strong>Name: </strong>
						</div>
						<div class="col-xs-8">${training.employee.firstName }
							${training.employee.lastName }</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<strong>Employer: </strong>
						</div>
						<div class="col-xs-8">${training.employee.employer }</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<strong>Job Title: </strong>
						</div>
						<div class="col-xs-8">${training.employee.jobTitle }</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<strong>Business Group: </strong>
						</div>
						<div class="col-xs-8">${training.employee.businessGroup }</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>


	</div>
	<!--row--> <!-- Course Objective and Outline and Forms -->
	<div class="row">
		<!-- Course Objective and Outline -->
		<div class="col-md-8 col-xs-12">
			<div class="box box-danger">
				<div class="box-header with-border">
					<div class="row">
						<div class="col-xs-8">
							<h3 class="box-title">Course Objective and Outline</h3>
						</div>
						<div class="col-xs-4 view">
							<button class="btn btn-danger pull-right" id="editObjective">
								<i class="fa fa-pencil pull-left" aria-hidden="true"></i>
							</button>

						</div>
						<div class="col-xs-4 edit" style="display:none">
							<button class="btn btn-danger pull-right" id="saveObjective" style="margin-left: 8px">
								<i class="fa fa-save pull-left" aria-hidden="true"></i>
							</button>
							
							<button class="btn btn-default pull-right" id="cancelObjective">
								<i class="fa fa-close pull-left" aria-hidden="true"></i>
							</button>

						</div>
					</div>
				</div>
				<div class="box-body view">
					<div id="objective">${training.objective}</div>


				</div>
				<div class="box-body edit" style="display: none">
					<textarea id="objectiveEdit" name="objective"
						class="col-xs-12 col-md-12" value="${training.objective }"></textarea>
				</div>

				<!-- /.box-body -->
			</div>
		</div>

		<!-- Remarks -->
		<c:if test="${not empty training.remarks}">
			<div class="col-md-4 col-xs-12">
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title">Remarks</h3>
					</div>
					<div class="box-body">
						<p>${training.remarks }</p>
					</div>
					<!-- /.box-body -->
				</div>
			</div>
		</c:if>

		<!-- PARTICIPANTS -->
		<div class="col-md-12 col-xs-12">
			<!--Faci Box -->
			<div class="box box-danger">
				<div class="box-header with-border">
					<h3 class="box-title">Participants</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-xs-12">
							<table class="table table-striped">
								<tbody>
									<tr>
										<th>Emp ID</th>
										<th>Employee Name</th>
										<th>Employer</th>
										<th>Job Title</th>
										<th>Business Group</th>
									</tr>
									<c:forEach items="${participants }" var="p">
										<tr>
											<td>${p.employee.employeeID}</td>
											<td>${p.employee.firstName}${p.employee.lastName}</td>
											<td>${p.employee.employer }</td>
											<td>${p.employee.jobTitle }</td>
											<td>${p.employee.businessGroup }</td>
										</tr>
									</c:forEach>


								</tbody>
							</table>
						</div>

					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>

	</div>

	</section>
	<!-- /.content -->
	
	<div class="modal modal-default fade" tabindex="-1" role="dialog" id="updateSuccessModal">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Success!</h4>
				</div>
				<div class="modal-body">
					<p>Course Objective and Outline successfully updated..</p>
				</div>
				
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
</div>
<script>
	$(function() {
		bindTrainingDetails();
	});
</script>
