<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div style="min-height: 881px;">
	<section class="content-header">
	<h1>Trainings</h1>

	</section>


	<section class="content">
	<div class="box box-danger">
		<div class="box-header with-border">
			<h3 class="box-title">List of Trainings</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<select id="tpSelect" class="select2" style="width: 200px">

			</select>
			<button class="btn btn-default" id="viewTraining">View</button>
			<form>
				<div id="tableDiv" style="margin-top: 20px">
					<table class="table table-striped">
						<tbody>
							<tr>
								<th><input type="checkbox" /></th>
								<th style="">Training ID</th>
								<th>Type</th>
								<th>Title</th>
								<th>BU</th>
								<th># of Pax</th>
								<th>Start Date</th>
								<th>End Date</th>
							</tr>
							<c:forEach items="${trainings }" var="t">
								<tr>
									<td><input type="checkbox" name="trainingID" value="${t.trainingID }"/></td>
									<td><a href="#">${t.trainingID }</a></td>
									<td>${t.type }</td>
									<td>${t.title }</td>
									<td>${t.businessUnit }</td>
									<td>${t.noOfPax }</td>
									<td>${t.startDate }</td>
									<td>${t.endDate }</td>
								</tr>
							</c:forEach>


						</tbody>
					</table>
				</div>
			</form>
		</div>
		<!-- /.box-body -->
		

	</div>
	</section>
</div>
<script>
	$(function() {
		bindTrainings();
	});
</script>
