var ROOT_URL = "http://localhost:8080/SoaBaseCode/AllianceATES";
var genTrainingPlans = $('#genTrainingPlans');
var genPendings = $('#genPendings');
var participantsToAdd = [];

$(function() {
	bindGeneralPage();
	loadTrainingPlans();
	

});


function loadForm(){
	console.log('loadForm entered');
	var empID = $('#employeeID').data('id');
	var formID= $('#formID').data('formid');
	$.ajax({
		url : ROOT_URL + '/general/getForm/'+ empID + '/'+ formID,
		type : "get",
		dataType : "text"
	}).done(function(data) {
		$('#main-content').html(data);
	});
		
}


function loadPending() {
	
	console.log('loadPending entered...');
	var empID = $('#employeeID').data('id');
	$.ajax({
		url : ROOT_URL + '/general/getPending/'+ empID,
		type : "get",
		dataType : "text"
	}).done(function(data) {
		$('#main-content').html(data);
	});
}

function loadTrainingPlans() {
	$.ajax({
		url : ROOT_URL + '/general/trainingPlans',
		type : "get",
		dataType : "text"
	}).done(function(data) {
		$('#main-content').html(data);
	});
}

function bindTrainingPlansIndex() {
	var tpModal = $('#addModal');
	
	$('.moreTP').click(function(event) {
		event.preventDefault();
		var id = $(this).data('training');
		console.log('viewing training plan id:  ' + id);

		$.ajax({
			url : ROOT_URL + '/general/trainingPlan/' + id,
			type : "get",
			dataType : "text"
		}).done(function(data) {
			$('#main-content').html(data);
		});

	});

	$('#newTP').click(function(event) {
		event.preventDefault();
		tpModal.modal('show');
	});
}

function bindGeneralPage() {
	$('#genTrainingPlans').click(function(event) {
		event.preventDefault();
		genTrainingPlans.closest('li').addClass('active');
		genPendings.closest('li').removeClass('active');
		loadTrainingPlans();

	});

	
	$('#genPendings').click(function(event) {
		event.preventDefault();
		genPendings.closest('li').addClass('active');
		genTrainingPlans.closest('li').removeClass('active');
		loadPending();

	});	

}

function bindPendingPage() {
	$('#btnAnswerForm').click(function(event) {
		event.preventDefault();
		faciTrainingPlans.closest('li').addClass('active');
		faciTrainings.closest('li').removeClass('active');
		loadForm();

	});	

}

function loadAndBindCalendar() {
	/*
	 * initialize the calendar
	 * -----------------------------------------------------------------
	 */
	// Date for the calendar events (dummy data)
	var date = new Date();
	var d = date.getDate(), m = date.getMonth(), y = date.getFullYear();
	var tpID = $('h1').data('tpid');
	$('#calendar').fullCalendar({
		header : {
			left : 'prev,next today',
			center : 'title',
			right : 'month,agendaWeek,listMonth'
		},
		buttonText : {
			today : 'today',
			month : 'month',
			week : 'week',
			list : 'list'
		},
		// JSON Feed from Server
		events : {
			url : ROOT_URL + '/api/training/viewCalendar/' + tpID,
			type : 'GET',
			color : '#e85140',
			textColor : 'white'

		},

		eventClick : function(calEvent, jsEvent, view) {

			var trainingModal = $('#trainingModal');
			var start = $.fullCalendar.moment(calEvent.start);
			var end = $.fullCalendar.moment(calEvent.end);
			trainingModal.find('.modal-title').text(calEvent.title);
			trainingModal.find('#pstartDate').text(start.format());
			trainingModal.find('#pendDate').text(end.format());

			trainingModal.modal('show');

		},

	});

	
}

