var ROOT_URL = "http://localhost:8080/SoaBaseCode/AllianceATES";
var btnTrainingPlans = $('#btnTrainingPlans');
var btnTrainings = $('#btnTrainings');
var btnAttendance = $('#btnAttendance');
var participantsToAdd = [];

$(function() {
	bindAdminPage();
	loadTrainingPlans();

});

function loadTrainingPlans() {
	$.ajax({
		url : ROOT_URL + '/admin/trainingPlans',
		type : "get",
		dataType : "text"
	}).done(function(data) {
		$('#main-content').html(data);
	});
}

function loadTrainings() {
	$.ajax({
		url : ROOT_URL + '/admin/training/view',
		type : "get",
		dataType : "text"
	}).done(function(data) {
		$('#main-content').html(data);
	});
}

function bindTrainings() {

	$('#addNewTraining').click(function() {
		var tpID = $("#tpSelect").val();
		if (tpID != null) {
			$.ajax({
				url : ROOT_URL + '/admin/training/add',
				type : "get",
				data : {
					trainingPlanID : tpID
				},
				dataType : "text"
			}).done(function(data) {
				$('#main-content').html(data);
			});
		} else {
			alert('Please select a training plan');
		}

	});

	$('#viewTraining').click(function() {
		var tpID = $('#tpSelect').val();
		if (tpID != null) {
			$.ajax({
				url : ROOT_URL + '/admin/training/view/' + tpID,
				method : "post",
				dataType : "text"
			}).done(function(data) {
				var table = $(data).find('#tableDiv').html();
				$('#tableDiv').html(table);
			});
		}

	});

	$('#deleteTraining').click(
			function() {
				var params = $('form').serialize() + '&trainingPlanID='
						+ $('#tpSelect').val();
				console.log(params);
				if (params != "") {
					$.ajax({
						url : ROOT_URL + '/admin/training/delete',
						data : params,
						method : "post",
						dataType : "text"
					}).done(function(data) {
						$('#main-content').html(data);
					});
				}
			});

	$('#tpSelect').select2({
		ajax : {
			data: function (params) {
			      return {
			        q: params.term, // search term
			        page: params.page
			      };
			 },
			dataType : "json",
			url : ROOT_URL + '/api/trainingPlan/viewAll',
			data : function(term) {
				return {
					q : term
				}
			},
			processResults : function(data) {
				return {
					results : $.map(data, function(obj) {
						return {
							id : obj.trainingPlanID,
							text : obj.year
						};
					})
				};
			}
		},
		tags : true,
		createTag : function(params) {
			return undefined;
		}
	});

	$('#tableDiv').delegate('a', 'click', function(event) {
		event.preventDefault();
		var trainingID = $(this).html();
		$.ajax({
			url : ROOT_URL + '/admin/training/details/' + trainingID,
			type : "get",
			dataType : "text"
		}).done(function(data) {
			$('#main-content').html(data);
		});

	});

	$('table').delegate('a', 'click', function(event) {
		event.preventDefault();
		console.log($(this).html());
	});

}

function bindTrainingPlansIndex() {
	var tpModal = $('#addModal');
	tpModal.find('#btnAddTP').click(function() {
		tpModal.modal('hide');
		$.ajax({
			url : ROOT_URL + '/admin/trainingPlan/add',
			type : "post",
			dataType : "text"
		}).done(function(data) {

			$('#main-content').html(data);
		});
	});

	$('.moreTP').click(function(event) {
		event.preventDefault();
		var id = $(this).data('training');
		console.log('viewing training plan id:  ' + id);

		$.ajax({
			url : ROOT_URL + '/admin/trainingPlan/' + id,
			type : "get",
			dataType : "text"
		}).done(function(data) {
			$('#main-content').html(data);
		});

	});

	$('#newTP').click(function(event) {
		event.preventDefault();
		tpModal.modal('show');
	});
}

function bindAdminPage() {
	$('#btnTrainingPlans').click(function(event) {
		event.preventDefault();
		btnTrainingPlans.closest('li').addClass('active');
		btnTrainings.closest('li').removeClass('active');
		btnAttendance.closest('li').removeClass('active');
		loadTrainingPlans();

	});

	$('#btnTrainings').click(function(event) {
		event.preventDefault();
		btnTrainings.closest('li').addClass('active');
		btnTrainingPlans.closest('li').removeClass('active');
		btnAttendance.closest('li').removeClass('active');
		loadTrainings();
	});
	
	$('#btnAttendance').click(function(event) {
		event.preventDefault();
		btnAttendance.closest('li').addClass('active');
		btnTrainings.closest('li').removeClass('active');
		btnTrainingPlans.closest('li').removeClass('active');
		loadAttendance();
	});
}

function loadAttendance(){
	$.ajax({
		url : ROOT_URL + '/admin/attendance',
		dataType: "text",
		method: "get"
	}).done(function(data){
		$('#main-content').html(data);
	});
}

function bindAttendance(){
	$('ul').delegate('a','click',function(event){
		event.preventDefault();
		var splittedDate =  $(this).data("date").split("-");
		var specificDate = new Date(splittedDate[0],splittedDate[1] - 1,splittedDate[2] - 0);
		console.log(specificDate);
		var trainingID = $("#trainingSelect").val();
		$('ul > li').removeClass('active');
		$(this).closest('li').addClass('active');
		
		$.ajax({
			url: ROOT_URL + "/admin/attendance/"+trainingID,
			data: {date: specificDate },
			method: "get",
			dataType: "text"
		}).done(function(data){
			var content = $(data).find("#attendanceDiv").html();
			$("#attendanceDiv").html(content);
		});
		
		
	});
	
	$('.select2').select2({
		ajax : {
			data: function (params) {
			      return {
			        q: params.term, // search term
			        page: params.page
			      };
			 },
			delay:250,
			dataType : "json",
			url : ROOT_URL + '/api/training/viewAll',
			processResults : function(data) {
				return {
					results : $.map(data, function(obj) {
						return {
							id : obj.trainingID,
							text : obj.title
						};
					})
				};
			}
		},
		createTag : function(params) {
			return undefined;
		}
	});
	
	$('#selectTraining').click(viewAttendanceTrainingDates);
	
	function viewAttendanceTrainingDates(){
		var trainingID = $("#trainingSelect").val();
		if(trainingID != null){
			$.ajax({
				url: ROOT_URL + '/admin/attendance/'+trainingID,
				method: "get",
				dataType: "text"
			}).done(function(data){
				$('table > tbody > tr').remove();
				var content = $(data).find("#dateDiv").html();
				$("#dateDiv").html(content);
			});
		}
	}
	
	$('#saveAttendance').click(function(){
		var trainingID = $("#trainingSelect").val();
		var rawDate = $('#dateDiv').find('.active > a').data("date");
		var splittedDate = rawDate.split("-");
		var specificDate = new Date(splittedDate[0],splittedDate[1]-1, splittedDate[2]);
		var serializedData = $('form').serializeArray();
		serializedData.push({name: 'date', value: specificDate});
		console.log(serializedData);
		$.ajax({
			url: ROOT_URL + '/api/attendance/save',
			data: serializedData,
			method: "post",
			dataType: "json"
		}).done(function(data){
			if(data.success){
				console.log("attendance save success");
				$("#saveSuccessModal").modal('show');
			}else{
				console.log("attendance save fail");
			}
		});
			
	});
	
	$('#addAttendance').click(function(){
		var trainingID = $("#trainingSelect").val();
		var modal = $('#addAttendanceModal');
		$.ajax({
			url : ROOT_URL + '/api/attendance/'+trainingID+'/getAvailableDates',
			method : "get",
			cache : false,
			dataType: "json"
		}).done(function(result){
			console.log(result);
			var options = $(modal).find('#dateSelect');
			options.empty();
			$.each(result, function() {
				console.log(this.date);
			    options.append($("<option />").val(this.date).text(this.date));
			});
			
			modal.modal('show');
		});
		
		
	});
	
	$('#add').click(function(data){
		var modal = $('#addAttendanceModal');
		var trainingID = $("#trainingSelect").val();
		var dateSelected = $(modal).find('#dateSelect').val();
		var splittedDate =  dateSelected.split("-");
		var specificDate = new Date(splittedDate[0],splittedDate[1] - 1,splittedDate[2]);
		console.log(specificDate);
		$.ajax({
			url: ROOT_URL + '/api/attendance/'+trainingID+'/create',
			data: {date : specificDate},
			method : "post",
			dataType : "json",
			cache : false
		}).done(function(data){
			if(data.success){
				console.log("attendance successfully created");
				viewAttendanceTrainingDates();
				
			}
		});
		
	});
}

function loadAndBindCalendar() {
	/*
	 * initialize the calendar
	 * -----------------------------------------------------------------
	 */
	// Date for the calendar events (dummy data)
	var date = new Date();
	var d = date.getDate(), m = date.getMonth(), y = date.getFullYear();
	var tpID = $('h1').data('tpid');
	$('#calendar').fullCalendar({
		header : {
			left : 'prev,next today',
			center : 'title',
			right : 'month,agendaWeek,listMonth'
		},
		buttonText : {
			today : 'today',
			month : 'month',
			week : 'week',
			list : 'list'
		},
		// JSON Feed from Server
		events : {
			url : ROOT_URL + '/api/training/viewCalendar/' + tpID,
			type : 'GET',
			color : '#e85140',
			textColor : 'white'

		},

		eventClick : function(calEvent, jsEvent, view) {

			var trainingModal = $('#trainingModal');
			var start = $.fullCalendar.moment(calEvent.start);
			var end = $.fullCalendar.moment(calEvent.end);
			trainingModal.find('.modal-title').text(calEvent.title);
			trainingModal.find('#pstartDate').text(start.format());
			trainingModal.find('#pendDate').text(end.format());
			trainingModal.find('#viewDetails').click(function() {
				trainingModal.hide();
				$.ajax({
					url : ROOT_URL + '/admin/training/details/' + calEvent.id,
					type : "get",
					dataType : "text"
				}).done(function(data) {
					$('#main-content').html(data);
				});
			});
			trainingModal.find("#delete").click(function() {
				trainingModal.hide();
				$.ajax({
					url : ROOT_URL + "/admin/training-calendar/delete",
					data : {
						tpID : tpID,
						trainingID : calEvent.id
					},
					method : "post",
					dataType : "text"
				}).done(function(data) {
					$('#main-content').html(data);
				});
			});

			trainingModal.find('#edit').click(function() {
				trainingModal.hide();
				$.ajax({
					url : ROOT_URL + '/admin/training/edit',
					data : {
						trainingID : calEvent.id
					},
					method : "get",
					dataType : "text"
				}).done(function(data) {
					$('#main-content').html(data);
				});
			});
			trainingModal.modal('show');

		},

	});

	$('#addNewTraining').click(function() {
		$.ajax({
			url : ROOT_URL + '/admin/training/add',
			type : "get",
			data : {
				trainingPlanID : tpID
			},
			dataType : "text"
		}).done(function(data) {
			$('#main-content').html(data);
		});
	});

	$('#viewAllTraining').click(function() {
		var tpID = $(this).data('tpid');
		$.ajax({
			url : ROOT_URL + '/admin/training/view/' + tpID,
			type : "post",
			dataType : "text"
		}).done(function(data) {
			$('#main-content').html(data);
		});
	});

	var deleteStatus = $('#deleteStatus').data('status');
	if (deleteStatus == 'success') {
		$('#deleteSuccessModal').modal('show');
	}
}

function bindAddTrainingForm() {
	particpantsToAdd = [];
	var status = $('#status').data('status');
	var successModal = $('#addSuccessModal');

	if (status == 'success') {
		successModal.modal('show');
	} else if (status == 'fail') {

	} else {

	}

	$('.datepicker').datepicker({
		autoclose : true
	});

	$('.select2').select2({
		ajax : {
			 data: function (params) {
			      return {
			        q: params.term, // search term
			        page: params.page
			      };
			 },
			delay:250,
			dataType : "json",
			url : ROOT_URL + '/api/employee/viewAll',
			processResults : function(data) {
				return {
					results : $.map(data, function(obj) {
						return {
							id : obj.employeeID,
							text : obj.firstName + ' ' + obj.lastName
						};
					})
				};
			}
		},
		createTag : function(params) {
			return undefined;
		}
	});

	$('#objective').wysihtml5({
		toolbar : {
			"link" : false,
			"image" : false
		}
	});

	$('#addParticipant')
			.click(
					function() {
						var select = $('#participantSelect');
						var empID = select.val();
						if (empID != "" && empID != null) {
							// Adds to the global variable
							participantsToAdd.push(empID);

							// Adds employee to the table
							$
									.ajax(
											{
												url : ROOT_URL
														+ '/api/employee/view/'
														+ empID,
												type : "get",
												dataType : "json"

											})
									.done(
											function(emp) {
												$('table > tbody')
														.append(
																'<tr>'
																		+ '<td>'
																		+ emp.employeeID
																		+ '</td>'
																		+ '<td>'
																		+ emp.firstName
																		+ ' '
																		+ emp.lastName
																		+ '</td>'
																		+ '<td>'
																		+ emp.employer
																		+ '</td>'
																		+ '<td>'
																		+ emp.jobTitle
																		+ '</td>'
																		+ '<td>'
																		+ emp.businessGroup
																		+ '</td>'
																		+ "<td><a href='#' id='removeParticipant' data-empid='"
																		+ emp.employeeID
																		+ "'>Remove</a></td>"
																		+ '</tr>');
											});

							// Disables the employee in selection (for unique)
							$('#participantSelect').find(
									"option[value=" + empID + "]").attr(
									'disabled', 'disabled');

						}

					});

	$('table').delegate(
			'a',
			'click',
			function(event) {
				event.preventDefault();
				var empID = $(this).data('empid');
				// removes empID in participantsToAdd
				var ary = window['participantsToAdd'];
				var index = ary.indexOf(empID);
				if (index != -1) {
					ary.splice(index, 1);
				}
				// removes row from table
				$(this).closest('tr').remove();
				// re-enables the option in select
				$('#participantSelect').find("option[value=" + empID + "]")
						.removeAttr('disabled');

			});

	$('form').on('submit', function() {
		var that = $(this);
		data = {};

		return false;
	});

	$('#save').click(function() {
		var params = {};
		params['trainingPlanID'] = $(this).data('tpid');
		params['participantsToAdd'] = participantsToAdd;
		console.log(params);
		$('form').find('[name]').each(function(index, value) {
			params[$(this).attr('name')] = $(this).val();

		});

		$.ajax({
			url : ROOT_URL + "/admin/training/add",
			method : "post",
			data : params,
			dataType : "text",
			error : function(e) {
				console.log(e);
			}
		}).done(function(data) {
			$('#main-content').html(data);
		});

	});

}

function bindEditTrainingForm() {
	var toAdd = [];
	var trainingID = $('h1').data('trainingid');
	$('#objective').wysihtml5({
		toolbar : {
			"link" : false,
			"image" : false
		}
	});

	$('.select2').select2({
		ajax : {
			dataType : "json",
			url : ROOT_URL + '/api/employee/viewAll',
			processResults : function(data) {
				return {
					results : $.map(data, function(obj) {
						return {
							id : obj.employeeID,
							text : obj.firstName + ' ' + obj.lastName
						};
					})
				};
			}
		},
		createTag : function(params) {
			return undefined;
		}
	});
	
	$.ajax({
		url : ROOT_URL + '/api/training/view/' + trainingID ,
		dataType: "json",
		cache: false,
		method: "get"
	}).done(function(training){
		$('.wysihtml5-sandbox').contents().find('body').html(training.objective);
		var startSplit = training.startDate.split("-");
		var endSplit = training.endDate.split("-");
		var newStartDate = new Date(startSplit[0],startSplit[1] - 1,startSplit[2]);
		var newEndDate = new Date(endSplit[0],endSplit[1] - 1,endSplit[2]);
		console.log("new end date : " + newEndDate);
		$('#startDate').datepicker('setDate', newStartDate);
		$('#endDate').datepicker('setDate', newEndDate);
		$('#type').val(training.type);
		$('#businessUnit').val(training.businessUnit);
	});
	
	$.ajax({
		url : ROOT_URL + '/api/training/' + trainingID + '/participants/',
		cache: false,
		dataType: "json",
		method : "get"
	}).done(function(participants){
		console.log('before pushing  : ' + toAdd);
		participants.forEach(function(currentValue){
			toAdd.push(currentValue.employee.employeeID);
			$('table > tbody')
			.append(
					'<tr>'
							+ '<td>'
							+ currentValue.employee.employeeID
							+ '</td>'
							+ '<td>'
							+ currentValue.employee.firstName
							+ ' '
							+ currentValue.employee.lastName
							+ '</td>'
							+ '<td>'
							+ currentValue.employee.employer
							+ '</td>'
							+ '<td>'
							+ currentValue.employee.jobTitle
							+ '</td>'
							+ '<td>'
							+ currentValue.employee.businessGroup
							+ '</td>'
							+ "<td><a href='#' id='removeParticipant' data-empid='"
							+ currentValue.employee.employeeID
							+ "'>Remove</a></td>"
							+ '</tr>');
			$('#participantSelect').find(
					"option[value=" + currentValue.employee.employteeID + "]").attr(
					'disabled', 'disabled');
		});
		
		console.log('after pushing: ' + toAdd );
		$('table').delegate(
				'a',
				'click',
				function(event) {
					event.preventDefault();
					var empID = $(this).data('empid');
					// removes empID in participantsToAdd
					var ary = toAdd;
					var index = ary.indexOf(empID);
					if (index != -1) {
						ary.splice(index, 1);
					}
					// removes row from table
					$(this).closest('tr').remove();
					// re-enables the option in select
					$('#participantSelect').find("option[value=" + empID + "]")
							.removeAttr('disabled');

				});
		
		$('#addParticipant')
		.click(
				function() {
					var select = $('#participantSelect');
					var empID = select.val();
					if (empID != "" && empID != null) {
						// Adds to the global variable
						toAdd.push(empID);
						console.log(participantsToAdd);
						// Adds employee to the table
						$
								.ajax(
										{
											url : ROOT_URL
													+ '/api/employee/view/'
													+ empID,
											type : "get",
											dataType : "json"

										})
								.done(
										function(emp) {
											$('table > tbody')
													.append(
															'<tr>'
																	+ '<td>'
																	+ emp.employeeID
																	+ '</td>'
																	+ '<td>'
																	+ emp.firstName
																	+ ' '
																	+ emp.lastName
																	+ '</td>'
																	+ '<td>'
																	+ emp.employer
																	+ '</td>'
																	+ '<td>'
																	+ emp.jobTitle
																	+ '</td>'
																	+ '<td>'
																	+ emp.businessGroup
																	+ '</td>'
																	+ "<td><a href='#' id='removeParticipant' data-empid='"
																	+ emp.employeeID
																	+ "'>Remove</a></td>"
																	+ '</tr>');
										});

						// Disables the employee in selection (for unique)
						$('#participantSelect').find(
								"option[value=" + empID + "]").attr(
								'disabled', 'disabled');

					}

				});
		
		$('#save').click(function() {
			var params = {};
			params['participantsToAdd'] = toAdd;
			console.log(params);
			$('form').find('[name]').each(function(index, value) {
				params[$(this).attr('name')] = $(this).val();

			});

			$.ajax({
				url : ROOT_URL + "/admin/training/edit",
				method : "post",
				data : params,
				dataType : "text",
			}).done(function(data) {
				$('#main-content').html(data);
			});

		});
		
		
		
	});
	
	
	
	
}
function bindTrainingDetails(){
	var t = $("#trainingID").data("id");
	
   $('#editTraining').click(function(){
	   $.ajax({
			url : ROOT_URL + '/admin/training/edit',
			data : {
				trainingID :t
			},
			method : "get",
			dataType : "text"
		}).done(function(data) {
			$('#main-content').html(data);
		});
   });
   
   updateFormRelease();
   
   $('#releaseCF').click(function(){
	   $.ajax({
		   url: ROOT_URL + '/api/training/'+t+'/form/release/participants',
		   data: {
			   typeID: 3,
			   formType: "Course Feedback"
		   },
		   method: "post",
		   dataType: "json"
	   }).done(function(status){
		   if(status.success){
			   console.log("Releasing success");
			   $('#releaseSuccessModal').modal('show');
			   updateFormRelease();
		   }else{
			   console.log("Error releasing success");
			   $('#releaseErrorModal').modal('show');
		   }
	   });
   });
   
   $('#releaseParticipantSA').click(function(){
	   $.ajax({
		   url: ROOT_URL + '/api/training/'+t+'/form/release/participants',
		   data: {
			   typeID: 1,
			   formType: "Skills Assessment"
		   },
		   method: "post",
		   dataType: "json"
	   }).done(function(status){
		   if(status.success){
			   console.log("Releasing success");
			   $('#releaseSuccessModal').modal('show');
			   updateFormRelease();
		   }else{
			   console.log("Error releasing success");
			   $('#releaseErrorModal').modal('show');
		   }
	   });
   });
   
   $('#releaseSupervisorSA').click(function(){
	   $.ajax({
		   url: ROOT_URL + '/api/training/'+t+'/form/release/supervisors',
		   data: {
			   typeID: 1,
			   formType: "Skills Assessment"
		   },
		   method: "post",
		   dataType: "json"
	   }).done(function(status){
		   if(status.success){
			   console.log("Releasing success");
			   $('#releaseSuccessModal').modal('show');
			   updateFormRelease();
		   }else{
			   console.log("Error releasing success");
			   $('#releaseErrorModal').modal('show');
		   }
	   });
   });
   
   $('#releaseTE').click(function(){
	   $.ajax({
		   url: ROOT_URL + '/api/training/'+t+'/form/release/supervisors',
		   data: {
			   typeID: 4,
			   formType: "Training Effectiveness Assessment"
		   },
		   method: "post",
		   dataType: "json"
	   }).done(function(status){
		   if(status.success){
			   console.log("Releasing success");
			   $('#releaseSuccessModal').modal('show');
			   updateFormRelease();
		   }else{
			   console.log("Error releasing success");
			   $('#releaseErrorModal').modal('show');
		   }
	   });
   });
   
   
   $('#generateCF').click(function(){
	   $.ajax({
		   url: ROOT_URL + '/admin/training/'+ t + '/form/generate',
		   method: "get",
		   data:{
			 formType: "Course Feedback",
			 typeID: 3
		   },
		   dataType: "text"
	   }).done(function(data){
		   $("#main-content").html(data);
	   });
   });
   
   $('#generateTE').click(function(){
	   $.ajax({
		   url: ROOT_URL + '/admin/training/'+ t + '/form/generate',
		   method: "get",
		   data:{
			 formType: "Training Effectiveness Assessment",
			 typeID: 4
		   },
		   dataType: "text"
	   }).done(function(data){
		   $("#main-content").html(data);
	   });
   });
   
}

function updateFormRelease(){
	var t = $("#trainingID").data("id");
	var releaseCF = $('#releaseCF');
	var generateCF = $('#generateCF');
	var releaseParticipantSA = $('#releaseParticipantSA');
	var releaseSupervisorSA = $('#releaseSupervisorSA');
	var releaseTE = $('#releaseTE');
	var generateTE = $('#generateTE');
	
	$.ajax({
		   url: ROOT_URL + '/api/training/'+t+'/form/status',
		   dataType: "json",
		   method: "get"
	   }).done(function(status){
		   if(status['Course Feedback'] == 1){
			   releaseCF.hide();
			   generateCF.show();
		   }
		   if(status['Skills Assessment'] == 1){
			   console.log('sa 1');
			   releaseParticipantSA.hide();
			   releaseSupervisorSA.show();
		   }else if(status['Skills Assessment'] == -1){
			   console.log('sa -1');
			   releaseParticipantSA.hide();
			   releaseSupervisorSA.hide();
			   $('#skills').hide();
		   }
		   
		   if(status['Training Effectiveness Assessment'] == -1){
			   releaseTE.hide();
			   generateTE.show();
		   }
	   });
}

function loadCharts(){
	var formID = $('#formID').data('id');
	$.ajax({
		url: ROOT_URL + '/api/form/'+formID+'/report',
		method: "get",
		dataType: "json"
	}).done(function(report){
		$.each(report, function(){
			console.log(this.questionNumber);
			console.log(this.data);
			console.log(this.labels);
			var canvas = $("#"+this.questionNumber);
			if(this.questionType == 'Agree' || this.questionType == 'Comprehension'){
				var chart = new Chart(canvas, {
					type: 'bar',
					data: {
						
						labels: this.labels,
						datasets: [{
							label: 'Accumulated Answers',
							data: this.data,
							backgroundColor: [
				                'rgba(255, 99, 132, 0.7)',
				                'rgba(54, 162, 235, 0.7)',
				                'rgba(255, 206, 86, 0.7)',
				                'rgba(75, 192, 192, 0.7)',
				                'rgba(153, 102, 255,0.7)'
				            ],
				            borderColor: [
				                'rgba(255,99,132,1)',
				                'rgba(54, 162, 235, 1)',
				                'rgba(255, 206, 86, 1)',
				                'rgba(75, 192, 192, 1)',
				                'rgba(153, 102, 255, 1)'
				            ],
				            borderWidth: 1
						}]
					}
				});
				
			}else if(this.questionType == 'Useful' || this.questionType == 'Objectives' || this.questionType == 'Suitable'){
				var chart = new Chart(canvas, {
					type: 'pie',
					data: {
						labels: this.labels,
						datasets: [{
							data: this.data,
							backgroundColor: [
				                'rgba(255, 99, 132, 0.7)',
				                'rgba(54, 162, 235, 0.7)',
				                'rgba(255, 206, 86, 0.7)',
				                'rgba(75, 192, 192, 0.7)',
				                'rgba(153, 102, 255, 0.7)'
				            ]
						}]
					}
				});
			}else if(this.questionType == 'Well' || this.questionType == 'Expectations'){
				var chart = new Chart(canvas, {
					type: 'doughnut',
					data: {
						labels: this.labels,
						datasets: [{
							data: this.data,
				            backgroundColor: [
				                'rgba(255, 99, 132, 0.7)',
				                'rgba(54, 162, 235, 0.7)',
				                'rgba(255, 206, 86, 0.7)',
				                'rgba(75, 192, 192, 0.7)',
				                'rgba(153, 102, 255, 0.7)',
				                'rgba(255, 159, 64, 0.7)'
				            ]
						}]
					}
				});
			}else if(this.questionType == 'Check'){
				var chart = new Chart(canvas, {
					type: 'horizontalBar',
					data: {
						
						labels: this.labels,
						datasets: [{
							label: 'Accumulated Answers',
							data: this.data,
							backgroundColor: [
				                'rgba(255, 99, 132, 0.7)',
				                'rgba(54, 162, 235, 0.7)',
				                'rgba(255, 206, 86, 0.7)',
				                'rgba(75, 192, 192, 0.7)',
				                'rgba(153, 102, 255,0.7)',
				                'rgba(255, 99, 132, 0.7)',
				                'rgba(54, 162, 235, 0.7)',
				                'rgba(255, 206, 86, 0.7)',
				                'rgba(75, 192, 192, 0.7)',
				                'rgba(153, 102, 255,0.7)'
				            ],
				            borderColor: [
				                'rgba(255, 99, 132, 1)',
				                'rgba(54, 162, 235, 1)',
				                'rgba(255, 206, 86, 1)',
				                'rgba(75, 192, 192, 1)',
				                'rgba(153, 102, 255, 1)',
				                'rgba(255, 99, 132,1 )',
				                'rgba(54, 162, 235, 1)',
				                'rgba(255, 206, 86, 1)',
				                'rgba(75, 192, 192, 1)',
				                'rgba(153, 102, 255, 1)'
				            ],
				            borderWidth: 1
						}]
					}
				});
				
			}
		});
		$('.overlay').hide();
	});
}



