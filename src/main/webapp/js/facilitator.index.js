var ROOT_URL = "http://localhost:8080/SoaBaseCode/AllianceATES";
var faciTrainingPlans = $('#faciTrainingPlans');
var faciTrainings = $('#faciTrainings');
var faciPending = $('#faciPending');
var participantsToAdd = [];

$(function() {
	bindFaciPage();
	loadTrainingPlans();

});

function loadTrainingPlans() {
	$.ajax({
		url : ROOT_URL + '/facilitator/trainingPlans',
		type : "get",
		dataType : "text"
	}).done(function(data) {
		$('#main-content').html(data);
	});
}

function loadTrainings() {
	$.ajax({
		url : ROOT_URL + '/facilitator/training/view',
		type : "get",
		dataType : "text"
	}).done(function(data) {
		$('#main-content').html(data);
	});
}

function loadAndBindTrainings(){
	$('#tableDiv').delegate('a', 'click', function(event) {
		event.preventDefault();
		var trainingID = $(this).html();
		$.ajax({
			url : ROOT_URL + '/facilitator/training/details/' + trainingID,
			type : "get",
			dataType : "text"
		}).done(function(data) {
			$('#main-content').html(data);
		});

	});
}


function bindTrainingPlansIndex() {
	var tpModal = $('#addModal');
	
	$('.moreTP').click(function(event) {
		event.preventDefault();
		var id = $(this).data('training');
		console.log('viewing training plan id:  ' + id);

		$.ajax({
			url : ROOT_URL + '/facilitator/trainingPlan/' + id,
			type : "get",
			dataType : "text"
		}).done(function(data) {
			$('#main-content').html(data);
		});

	});

	$('#newTP').click(function(event) {
		event.preventDefault();
		tpModal.modal('show');
	});
}

function bindFaciPage() {
	$('#faciTrainingPlans').click(function(event) {
		event.preventDefault();
		faciTrainingPlans.closest('li').addClass('active');
		faciTrainings.closest('li').removeClass('active');
		faciPending.closest('li').removeClass('active');
		loadTrainingPlans();

	});

	$('#faciTrainings').click(function(event) {
		event.preventDefault();
		faciTrainings.closest('li').addClass('active');
		faciTrainingPlans.closest('li').removeClass('active');
		faciPending.closest('li').removeClass('active');
		loadTrainings();
	});
	
	$('#faciPending').click(function(event){
		event.preventDefault();
		faciPending.closest('li').addClass('active');
		faciTrainingPlans.closest('li').removeClass('active');
		faciTrainings.closest('li').removeClass('active');
		loadPending();
	});
}

function loadPending(){
	var empID = $('#empID').data('id');
	$.ajax({
		url: ROOT_URL + '/facilitator/pending/'+empID,
		method: "get",
		dataType: "text"
	}).done(function(data){
		$('#main-content').html(data);
	});
}
function loadAndBindCalendar() {
	/*
	 * initialize the calendar
	 * -----------------------------------------------------------------
	 */
	// Date for the calendar events (dummy data)
	var date = new Date();
	var d = date.getDate(), m = date.getMonth(), y = date.getFullYear();
	var tpID = $('h1').data('tpid');
	$('#calendar').fullCalendar({
		header : {
			left : 'prev,next today',
			center : 'title',
			right : 'month,agendaWeek,listMonth'
		},
		buttonText : {
			today : 'today',
			month : 'month',
			week : 'week',
			list : 'list'
		},
		// JSON Feed from Server
		events : {
			url : ROOT_URL + '/api/training/viewCalendar/' + tpID,
			type : 'GET',
			color : '#e85140',
			textColor : 'white'

		},

		eventClick : function(calEvent, jsEvent, view) {

			var trainingModal = $('#trainingModal');
			var start = $.fullCalendar.moment(calEvent.start);
			var end = $.fullCalendar.moment(calEvent.end);
			trainingModal.find('.modal-title').text(calEvent.title);
			trainingModal.find('#pstartDate').text(start.format());
			trainingModal.find('#pendDate').text(end.format());

			trainingModal.modal('show');

		},

	});

	
}


function bindTrainingDetails(){
	var t = $("#trainingID").data("id");

	$('#objectiveEdit').wysihtml5({
		toolbar : {
			"link" : false,
			"image" : false
		}
	});
   $('#editObjective').click(function(){
	   console.log('edit');
	  $('.edit').toggle();
	  $('.view').toggle();
	  $('.wysihtml5-sandbox').contents().find('body').html($('#objective').html());
   });
   
   $('#cancelObjective').click(function(){
	   console.log('edit');
	  $('.edit').toggle();
	  $('.view').toggle();
   });
   
   $('#saveObjective').click(function(){
	   var newobj = $('#objectiveEdit').val();
	   $.ajax({
		   url: ROOT_URL + '/api/training/'+t+'/edit/objective',
		   data: {objective: newobj},
		   method: "post",
		   dataType: "json"
	   }).done(function(data){
		   if(data.success){
			   $('#objective').html(newobj);
			   $('#updateSuccessModal').modal('show');
		   }
	   });
	   $('.edit').toggle();
	   $('.view').toggle();
   });
}
