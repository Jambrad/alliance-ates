var ROOT_URL = "http://localhost:8080/SoaBaseCode/ates/form";

$(document).ready(function(){
	loadChoices();
});

function loadChoices(){
	$(".Level").html("<select name='result'>" +
			"<option value='0'>N/A</option>" +
			"<option value='1'>None</option>" +
			"<option value='2'>Low</option>" +
			"<option value='3'>Beginner</option>" +
			"<option value='4'>Mid/Practicing</option>" +
			"<option value='5'>High Performing</option>" +
			"</select>");
	$(".Year").html("<select name='result'>" +
			"<option value='0'>N/A</option>" +
			"<option value='1'>&lt;1 years</option>" +
			"<option value='2'>1 year</option>" +
			"<option value='3'>2 years</option>" +
			"<option value='4'>3 years</option>" +
			"<option value='5'>4 years</option>" +
			"<option value='6'>5 years</option>" +
			"</select>");
	$(".Comprehension").html("<select name='result'>" +
			"<option value='0'>3 or more levels</option>" +
			"<option value='1'>2 levels</option>" +
			"<option value='2'>1 level</option>" +
			"<option value='3'>No improvement</option>" +
			"<option value='4'>Regressed</option>" +
			"</select>");
	$(".Well").html("<select name='result'>" +
			"<option value='0'>Very well</option>" +
			"<option value='1'>Well</option>" +
			"<option value='2'>Fair</option>" +
			"<option value='3'>Little</option>" +
			"<option value='4'>Very Little</option>" +
			"<option value='5'>No response</option>" +
			"</select>");
	$(".Suitable").html("<select name='result'>" +
			"<option value='0'>Suitable</option>" +
			"<option value='1'>Difficult</option>" +
			"<option value='2'>Easy</option>" +
			"<option value='3'>No response</option>" +
			"</select>");
	$(".Objectives").html("<select name='result'>" +
			"<option value='0'>All of the objectives were met</option>" +
			"<option value='1'>Most of the objectives were met</option>" +
			"<option value='2'>Very little of the objectives were met</option>" +
			"<option value='3'>No response</option>" +
			"</select>");
	$(".Agree").html("<select name='result'>" +
			"<option value='0'>Strongly agree</option>" +
			"<option value='1'>Agree</option>" +
			"<option value='2'>Neutral</option>" +
			"<option value='3'>Disagree</option>" +
			"<option value='4'>Strongly Disagree</option>" +
			"</select>");
	$(".Good").html("<select name='result'>" +
			"<option value='0'>Very good</option>" +
			"<option value='1'>Good</option>" +
			"<option value='2'>Fair</option>" +
			"<option value='3'>Poor</option>" +
			"<option value='4'>Very poor</option>" +
			"</select>");
	$(".Clear").html("<select name='result'>" +
			"<option value='0'>Very clear</option>" +
			"<option value='1'>Clear</option>" +
			"<option value='2'>Fair</option>" +
			"<option value='3'>Not so clear</option>" +
			"<option value='4'>Not clear</option>" +
			"</select>");
	$(".Useful").html("<select name='result'>" +
			"<option value='0'>Very useful</option>" +
			"<option value='1'>Useful</option>" +
			"<option value='2'>Fair</option>" +
			"<option value='3'>Not so useful</option>" +
			"<option value='4'>Not useful</option>" +
			"</select>");
	$(".Expectations").html("<select name='result'>" +
			"<option value='0'>Exceeds expectations</option>" +
			"<option value='1'>Significant strength</option>" +
			"<option value='2'>Meets expectations</option>" +
			"<option value='3'>Development needed</option>" +
			"<option value='4'>Needs improvement</option>" +
			"</select>");
	$(".Essay").html("<textarea name='result'></textarea>");
	$.ajax({
		url: ROOT_URL + '/checkbox',
		dataType: "text",
		method: "get"
		
	}).done(function(data){
		$(".Check").html(data);
	});
	
}

function bindCheckboxes(){
	$("input[type=checkbox]").on('change', function () {
	    var key = $(this).data("key");
	    $("#bin" + key).val($(this).is(":checked")?'1':'0');
	    console.log("Called");
	});
}


