/*
SQLyog Community v9.60 
MySQL - 5.6.21-log : Database - ates_credentials
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ates_credentials` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `ates_credentials`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `adminID` int(11) NOT NULL AUTO_INCREMENT,
  `employeeID` varchar(8) NOT NULL,
  PRIMARY KEY (`adminID`),
  KEY `employeeID` (`employeeID`),
  CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`employeeID`) REFERENCES `employee` (`employeeID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `admin` */

insert  into `admin`(`adminID`,`employeeID`) values (1,'522-0000');

/*Table structure for table `attendance` */

DROP TABLE IF EXISTS `attendance`;

CREATE TABLE `attendance` (
  `participationID` int(11) NOT NULL,
  `date` date NOT NULL,
  `amAttendance` enum('Present','Tardy','Absent','N/A') DEFAULT 'N/A',
  `pmAttendance` enum('Present','Tardy','Absent','N/A') DEFAULT 'N/A',
  PRIMARY KEY (`participationID`,`date`),
  CONSTRAINT `participation_id` FOREIGN KEY (`participationID`) REFERENCES `participation` (`participantID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `attendance` */

/*Table structure for table `employee` */

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `employeeID` varchar(8) NOT NULL,
  `firstName` varchar(16) NOT NULL,
  `middleName` varchar(16) DEFAULT NULL,
  `lastName` varchar(16) NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `address` varchar(128) NOT NULL,
  `email` varchar(32) DEFAULT NULL,
  `contactNumber` varchar(16) DEFAULT NULL,
  `employer` varchar(32) NOT NULL,
  `jobTitle` varchar(32) NOT NULL,
  `businessGroup` varchar(32) NOT NULL,
  `buHead` varchar(8) NOT NULL,
  `supervisor` varchar(8) DEFAULT NULL,
  `password` varchar(32) NOT NULL DEFAULT '123456',
  `loggedIn` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`employeeID`),
  KEY `buHead` (`buHead`),
  KEY `supervisor` (`supervisor`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`buHead`) REFERENCES `employee` (`employeeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `employee_ibfk_2` FOREIGN KEY (`supervisor`) REFERENCES `employee` (`employeeID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `employee` */

insert  into `employee`(`employeeID`,`firstName`,`middleName`,`lastName`,`gender`,`address`,`email`,`contactNumber`,`employer`,`jobTitle`,`businessGroup`,`buHead`,`supervisor`,`password`,`loggedIn`) values ('522-0000','Araceli',NULL,'Adlawon','Female','Cebu','email.com','09999999999','Alliance Software Inc.','Product Owner','Human Resource','522-0000',NULL,'test',1),('522-0001','Jhon Christian',NULL,'Ambrad','Male','Cebu','email.com','09888888888','Alliance Software Inc.','Project Manager','Human Resource','522-0000','522-0000','test',1),('522-0002','Justin',NULL,'Enerio','Male','Cebu','email.com','09777777777','Alliance Software Inc.','Developer','Human Resource','522-0000','522-0000','1234',1),('522-0003','Joshua',NULL,'Dy','Male','Cebu','email.com','09666666666','Alliance Software Inc.','Developer','Human Resource','522-0000','522-0000','test',1),('522-0004','Razil','Ogario','Cansancio','Male','Cebu','chichiri.ryota@gmail.com','09567120870','Alliance Software Inc.','Developer','Human Resource','522-0000','522-0000','password',1);

/*Table structure for table `form` */

DROP TABLE IF EXISTS `form`;

CREATE TABLE `form` (
  `formID` int(11) NOT NULL AUTO_INCREMENT,
  `formType` int(11) NOT NULL,
  `trainingID` int(11) NOT NULL,
  `releasedParticipant` int(1) DEFAULT '0',
  `releasedFacilitator` int(1) DEFAULT '0',
  `releasedSupervisor` int(1) DEFAULT '0',
  `formFor` enum('N/A','Pre','Post') NOT NULL,
  PRIMARY KEY (`formID`),
  KEY `formType` (`formType`),
  KEY `trainingID` (`trainingID`),
  CONSTRAINT `form_ibfk_1` FOREIGN KEY (`formType`) REFERENCES `form_type` (`typeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `form_ibfk_2` FOREIGN KEY (`trainingID`) REFERENCES `training` (`trainingID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=403 DEFAULT CHARSET=utf8;

/*Data for the table `form` */

insert  into `form`(`formID`,`formType`,`trainingID`,`releasedParticipant`,`releasedFacilitator`,`releasedSupervisor`,`formFor`) values (401,1,1011,1,0,0,'Pre'),(402,3,1011,0,0,0,'N/A');

/*Table structure for table `form_details` */

DROP TABLE IF EXISTS `form_details`;

CREATE TABLE `form_details` (
  `detailID` int(11) NOT NULL AUTO_INCREMENT,
  `formType` int(11) NOT NULL,
  `category` varchar(32) DEFAULT NULL,
  `questionNumber` int(11) DEFAULT NULL,
  `questionString` varchar(64) NOT NULL,
  `questionType` enum('Level','Check','Year','Essay','none') NOT NULL,
  PRIMARY KEY (`detailID`),
  KEY `formType` (`formType`),
  CONSTRAINT `form_details_ibfk_1` FOREIGN KEY (`formType`) REFERENCES `form_type` (`typeID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `form_details` */

insert  into `form_details`(`detailID`,`formType`,`category`,`questionNumber`,`questionString`,`questionType`) values (1,1,'Management',1,'Scope Management','Level'),(2,1,'Management',2,'Schedule management		','Level'),(3,1,'Management',3,'Quality management		','Level'),(4,1,'Management',4,'Manpower management 		','Level'),(5,1,'Management',5,'Cost management		','Level'),(6,1,'Management',6,'Communication management','Level'),(7,1,'Management',7,'Problem management','Level'),(8,1,'Management',8,'Risk management','Level'),(9,1,'Human Skill',9,'Leadership','Level'),(10,1,'Human Skill',10,'Globally competitive human resources','Level'),(11,1,'Human Skill',11,'Communication skill','Level');

/*Table structure for table `form_result` */

DROP TABLE IF EXISTS `form_result`;

CREATE TABLE `form_result` (
  `resultID` int(11) NOT NULL AUTO_INCREMENT,
  `formID` int(11) NOT NULL,
  `employeeID` varchar(8) NOT NULL,
  `detailID` int(11) NOT NULL,
  `answerString` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`resultID`),
  KEY `detailID` (`detailID`),
  KEY `employeeID` (`employeeID`),
  KEY `form_result_ibfk_3` (`formID`),
  CONSTRAINT `form_result_ibfk_1` FOREIGN KEY (`detailID`) REFERENCES `form_details` (`detailID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `form_result_ibfk_2` FOREIGN KEY (`employeeID`) REFERENCES `employee` (`employeeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `form_result_ibfk_3` FOREIGN KEY (`formID`) REFERENCES `form` (`formID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `form_result` */

insert  into `form_result`(`resultID`,`formID`,`employeeID`,`detailID`,`answerString`) values (1,401,'522-0000',1,'5');

/*Table structure for table `form_type` */

DROP TABLE IF EXISTS `form_type`;

CREATE TABLE `form_type` (
  `typeID` int(11) NOT NULL AUTO_INCREMENT,
  `formType` varchar(32) NOT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `form_type` */

insert  into `form_type`(`typeID`,`formType`) values (1,'Skills Assessment'),(2,'Training Needs Analysis'),(3,'Course Feedback'),(4,'Training Effetiveness Assessment');

/*Table structure for table `participation` */

DROP TABLE IF EXISTS `participation`;

CREATE TABLE `participation` (
  `participantID` int(11) NOT NULL AUTO_INCREMENT,
  `employeeID` varchar(8) NOT NULL,
  `trainingID` int(11) NOT NULL,
  PRIMARY KEY (`participantID`),
  KEY `employeeID` (`employeeID`),
  KEY `trainingID` (`trainingID`),
  CONSTRAINT `participation_ibfk_1` FOREIGN KEY (`employeeID`) REFERENCES `employee` (`employeeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `participation_ibfk_2` FOREIGN KEY (`trainingID`) REFERENCES `training` (`trainingID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8;

/*Data for the table `participation` */

insert  into `participation`(`participantID`,`employeeID`,`trainingID`) values (142,'522-0001',1033),(143,'522-0003',1033);

/*Table structure for table `pending` */

DROP TABLE IF EXISTS `pending`;

CREATE TABLE `pending` (
  `pendingID` int(11) NOT NULL AUTO_INCREMENT,
  `employeeID` varchar(8) NOT NULL,
  `formID` int(11) NOT NULL,
  `participantID` int(11) DEFAULT NULL,
  `isDone` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`pendingID`),
  KEY `employeeID` (`employeeID`),
  KEY `formID` (`formID`),
  KEY `participantID` (`participantID`),
  CONSTRAINT `pending_ibfk_1` FOREIGN KEY (`employeeID`) REFERENCES `employee` (`employeeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pending_ibfk_2` FOREIGN KEY (`formID`) REFERENCES `form` (`formID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pending_ibfk_3` FOREIGN KEY (`participantID`) REFERENCES `participation` (`participantID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pending` */

/*Table structure for table `training` */

DROP TABLE IF EXISTS `training`;

CREATE TABLE `training` (
  `trainingID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `trainingPlanID` int(11) NOT NULL,
  `facilitatorID` varchar(8) NOT NULL,
  `objective` longtext NOT NULL,
  `type` enum('Int','Ext','Cert') NOT NULL,
  `remarks` longtext,
  `businessUnit` varchar(32) DEFAULT NULL,
  `noOfPax` int(3) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `startTime` time DEFAULT NULL,
  `endTime` time DEFAULT NULL,
  PRIMARY KEY (`trainingID`),
  KEY `trainingPlanID` (`trainingPlanID`),
  KEY `facilitatorID` (`facilitatorID`),
  CONSTRAINT `training_ibfk_1` FOREIGN KEY (`trainingPlanID`) REFERENCES `training_plan` (`trainingPlanID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `training_ibfk_2` FOREIGN KEY (`facilitatorID`) REFERENCES `employee` (`employeeID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1034 DEFAULT CHARSET=utf8;

/*Data for the table `training` */

insert  into `training`(`trainingID`,`title`,`trainingPlanID`,`facilitatorID`,`objective`,`type`,`remarks`,`businessUnit`,`noOfPax`,`startDate`,`endDate`,`startTime`,`endTime`) values (1033,'J2EE Summer Bridge',105,'522-0000','<p><blockquote>Test<b>setset</b>Est tsetset</blockquote><b></b></p>','Int','ASI Summer Bridge 2017','ASJ',45,'2017-06-04','2017-06-30',NULL,NULL);

/*Table structure for table `training_plan` */

DROP TABLE IF EXISTS `training_plan`;

CREATE TABLE `training_plan` (
  `trainingPlanID` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(4) NOT NULL,
  PRIMARY KEY (`trainingPlanID`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;

/*Data for the table `training_plan` */

insert  into `training_plan`(`trainingPlanID`,`year`) values (105,2017),(106,2018),(107,2019),(108,2020);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
